export const strict = false

export const state = () => ({
  userObject: null,
  userToken: null,
  onlineUser: [],
})

export const mutations = {
  setUser(state, userObject) {
    try {
      if (userObject.auth_code) {
        state.userToken = userObject.auth_code;
      }
      state.userObject = userObject;
    } catch (error) {
      state.userToken = null;
      state.userObject = null;
    }
  },
  setOnlineUser(state, onlineUser) {
    state.onlineUser = onlineUser;
  },
  logout(state, status) {
    state.userObject = null;
    state.userToken = null;
    localStorage.removeItem("user");
  },
  reloadUser(state, status) {
    try {
      const userString = localStorage.getItem('user');
      let userObject = JSON.parse(userString);
      if (userObject.auth_code) {
        let authCode = userObject.auth_code;
        var base64Url = authCode.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        let dataJsonAfter = JSON.parse(jsonPayload);
        let currentTime = Math.floor(Date.now() / 1000);
        let exp = dataJsonAfter.exp;
        if ((Number(currentTime) - Number(exp)) < 0) {
          state.userObject = userObject;
          if (userObject.auth_code) {
            state.userToken = userObject.auth_code;
          }
        } else {
          localStorage.removeItem("user");
        }
      } else {
        localStorage.removeItem("user");
      }
    } catch (error) {
      localStorage.removeItem("user");
    }
  },
  updateUser(state, userObject) {
    try {
      if (userObject.auth_code) {
        state.userToken = userObject.auth_code;
      }
      let dataUser = { ...state.userObject, ...userObject };
      state.userObject = dataUser;
      localStorage.setItem("user", JSON.stringify(dataUser));
    } catch (error) {
      state.userToken = null;
      state.userObject = null;
    }
  },
  remove(state) {
    state.userObject = null;
    state.userToken = null;
    localStorage.removeItem("user");
  },
}

export const actions = {
  nuxtClientInit({ commit }, { req }) {
    try {
      const userString = localStorage.getItem('user');
      let userObject = JSON.parse(userString);
      if (userObject.auth_code) {
        let authCode = userObject.auth_code;
        var base64Url = authCode.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        let dataJsonAfter = JSON.parse(jsonPayload);
        let currentTime = Math.floor(Date.now() / 1000);
        let exp = dataJsonAfter.exp;
        if ((Number(currentTime) - Number(exp)) < 0) {
          commit('setUser', userObject);
        } else {
          localStorage.removeItem("user");
        }
      } else {
        localStorage.removeItem("user");
      }
    } catch (error) {
      localStorage.removeItem("user");
    }
  }
}
