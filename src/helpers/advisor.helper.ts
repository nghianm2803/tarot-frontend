import { axiosGetInstanceBackend } from "./axios-config";

class searchAdvisorHelper {
  getNewAdvisor = async (page: number, limit: number) => {
    let dataUrl = process.env.mainBackendUrl + `/users?user_role=advisor&page=${page}&limit=${limit}`;
    return await axiosGetInstanceBackend(dataUrl, {});
  };
}

export const searchAdvisor = new searchAdvisorHelper();
