import { axiosGetInstanceBackend } from "./axios-config";

class articleSearch {
  getPopularArticle = async (page: number, limit: number) => {
    let dataUrl = process.env.mainBackendUrl + `/posts?post_type=news&page=${page}&limit=${limit}`;
    return await axiosGetInstanceBackend(dataUrl, {});
  };

  getArticleList = async (type: string, page: number, limit: number) => {
    let dataUrl = process.env.mainBackendUrl + `/posts?post_type=${type}&page=${page}&limit=${limit}`;
    return await axiosGetInstanceBackend(dataUrl, {});
  };
}

export const articleHelper = new articleSearch();
