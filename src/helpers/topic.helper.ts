import { axiosGetInstanceBackend } from "./axios-config";

class searchTopic {
  getTopics = async (page: number, limit: number) => {
    let dataUrl = process.env.mainBackendUrl + `/taxonomy?taxonomy_type=category&page=${page}&limit=${limit}`;
    return await axiosGetInstanceBackend(dataUrl, {});
  };
}

export const topicHelper = new searchTopic();
