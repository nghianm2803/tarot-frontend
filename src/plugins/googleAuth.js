import Vue from 'vue'
import GoogleAuth from '~/modules/auth/googleAuth.js';
const gauthOption = {
  clientId: '159636316610-uugutdip41qo88tovnthlomn10m58n2b.apps.googleusercontent.com',
  scope: 'https://www.googleapis.com/auth/drive.metadata.readonly',
  discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/drive/v3/rest'],
  prompt: 'select_account'
}
Vue.use(GoogleAuth, gauthOption);
