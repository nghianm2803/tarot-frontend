import Vue from 'vue';
import VueSocketIO from 'vue-socket.io';
import io from "socket.io-client";


let authCode = '';
let userObjectBefore = null;
try {
  let userString = localStorage.getItem('user');
  if (userString) {
    let userObject = JSON.parse(userString);
    if (userObject && userObject.auth_code) {
      userObjectBefore = userObject;
      authCode = userObject.auth_code;
    }
  }
} catch (error) {
}
if (userObjectBefore) {
  const SocketInstance = io(process.env.socketUrl, {
    extraHeaders: {
      "X-Authorization": authCode,
    },
  });
  Vue.use(new VueSocketIO({
    debug: true,
    connection: SocketInstance
  }));
}

