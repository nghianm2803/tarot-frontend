import Vue from "vue";
import 'vue-cool-lightbox/dist/vue-cool-lightbox.min.css'
import CoolLightBox from 'vue-cool-lightbox';

Vue.use(CoolLightBox);
