import https from 'https';

export default function ({$axios, store, redirect}) {
    const agent = new https.Agent({
        rejectUnauthorized: false
    });
    $axios.onRequest(config => {
        config.httpsAgent = agent;
    })
    $axios.onError(error => {
        const code = parseInt(error.response && error.response.status)
        if (code === 400) {
            //redirect('/400')
        }
        if (code === 500) {
            //redirect('/500')
        }
        if (code === 404) {
            //redirect('/404')
        }
    })
}
