export default function (date, timeZone = 0) {

    let currentDate = new Date().getTime();
    let currentDateWithTimeZone = currentDate + timeZone*60*60*1000;
    let dateTimeZone = new Date(currentDateWithTimeZone);

    let remaining;
    let remainingSec = Math.floor((dateTimeZone / 1000) - (new Date(date).getTime() / 1000));
    if( remainingSec < 5 ){
        remaining = 'Vừa xong';
    } else if( remainingSec < 60 ){
        remaining = remainingSec + ' giây trước';
    } else if ( remainingSec < 3600 ){
        remaining = Math.floor(remainingSec/60) + ' phút trước';
    } else if ( remainingSec < 86400 ){
        remaining = Math.floor(remainingSec/3600) + ' giờ trước';
    } else if ( remainingSec < 2592000 ){
        remaining = Math.floor(remainingSec/86400) + ' ngày trước';
    } else if ( remainingSec < 31556926 ){
        remaining = Math.floor(remainingSec/2592000) + ' tháng trước';
    } else {
        remaining = Math.floor(remainingSec/31556926) + ' năm trước';
    }
    return remaining;
}