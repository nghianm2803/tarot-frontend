export default function (number) {
	number = parseInt(number);
    if (number >= 1000) {
        number = number/1000;
        number = Math.round(number * 10)/10;
    	return number + 'K';
    } else {
    	return number;
    }
}