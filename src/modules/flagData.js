const dataFlag =
{
  "AC": {
    "name": "Ascension Island",
    "emoji": "🇦🇨",
    "unicode": "U+1F1E6 U+1F1E8",
    "image": "/flags/images/AC.svg"
  },
  "AD": {
    "name": "Andorra",
    "emoji": "🇦🇩",
    "unicode": "U+1F1E6 U+1F1E9",
    "image": "/flags/images/AD.svg"
  },
  "AE": {
    "name": "United Arab Emirates",
    "emoji": "🇦🇪",
    "unicode": "U+1F1E6 U+1F1EA",
    "image": "/flags/images/AE.svg"
  },
  "AF": {
    "name": "Afghanistan",
    "emoji": "🇦🇫",
    "unicode": "U+1F1E6 U+1F1EB",
    "image": "/flags/images/AF.svg"
  },
  "AG": {
    "name": "Antigua & Barbuda",
    "emoji": "🇦🇬",
    "unicode": "U+1F1E6 U+1F1EC",
    "image": "/flags/images/AG.svg"
  },
  "AI": {
    "name": "Anguilla",
    "emoji": "🇦🇮",
    "unicode": "U+1F1E6 U+1F1EE",
    "image": "/flags/images/AI.svg"
  },
  "AL": {
    "name": "Albania",
    "emoji": "🇦🇱",
    "unicode": "U+1F1E6 U+1F1F1",
    "image": "/flags/images/AL.svg"
  },
  "AM": {
    "name": "Armenia",
    "emoji": "🇦🇲",
    "unicode": "U+1F1E6 U+1F1F2",
    "image": "/flags/images/AM.svg"
  },
  "AO": {
    "name": "Angola",
    "emoji": "🇦🇴",
    "unicode": "U+1F1E6 U+1F1F4",
    "image": "/flags/images/AO.svg"
  },
  "AQ": {
    "name": "Antarctica",
    "emoji": "🇦🇶",
    "unicode": "U+1F1E6 U+1F1F6",
    "image": "/flags/images/AQ.svg"
  },
  "AR": {
    "name": "Argentina",
    "emoji": "🇦🇷",
    "unicode": "U+1F1E6 U+1F1F7",
    "image": "/flags/images/AR.svg"
  },
  "AS": {
    "name": "American Samoa",
    "emoji": "🇦🇸",
    "unicode": "U+1F1E6 U+1F1F8",
    "image": "/flags/images/AS.svg"
  },
  "AT": {
    "name": "Austria",
    "emoji": "🇦🇹",
    "unicode": "U+1F1E6 U+1F1F9",
    "image": "/flags/images/AT.svg"
  },
  "AU": {
    "name": "Australia",
    "emoji": "🇦🇺",
    "unicode": "U+1F1E6 U+1F1FA",
    "image": "/flags/images/AU.svg"
  },
  "AW": {
    "name": "Aruba",
    "emoji": "🇦🇼",
    "unicode": "U+1F1E6 U+1F1FC",
    "image": "/flags/images/AW.svg"
  },
  "AX": {
    "name": "Åland Islands",
    "emoji": "🇦🇽",
    "unicode": "U+1F1E6 U+1F1FD",
    "image": "/flags/images/AX.svg"
  },
  "AZ": {
    "name": "Azerbaijan",
    "emoji": "🇦🇿",
    "unicode": "U+1F1E6 U+1F1FF",
    "image": "/flags/images/AZ.svg"
  },
  "BA": {
    "name": "Bosnia & Herzegovina",
    "emoji": "🇧🇦",
    "unicode": "U+1F1E7 U+1F1E6",
    "image": "/flags/images/BA.svg"
  },
  "BB": {
    "name": "Barbados",
    "emoji": "🇧🇧",
    "unicode": "U+1F1E7 U+1F1E7",
    "image": "/flags/images/BB.svg"
  },
  "BD": {
    "name": "Bangladesh",
    "emoji": "🇧🇩",
    "unicode": "U+1F1E7 U+1F1E9",
    "image": "/flags/images/BD.svg"
  },
  "BE": {
    "name": "Belgium",
    "emoji": "🇧🇪",
    "unicode": "U+1F1E7 U+1F1EA",
    "image": "/flags/images/BE.svg"
  },
  "BF": {
    "name": "Burkina Faso",
    "emoji": "🇧🇫",
    "unicode": "U+1F1E7 U+1F1EB",
    "image": "/flags/images/BF.svg"
  },
  "BG": {
    "name": "Bulgaria",
    "emoji": "🇧🇬",
    "unicode": "U+1F1E7 U+1F1EC",
    "image": "/flags/images/BG.svg"
  },
  "BH": {
    "name": "Bahrain",
    "emoji": "🇧🇭",
    "unicode": "U+1F1E7 U+1F1ED",
    "image": "/flags/images/BH.svg"
  },
  "BI": {
    "name": "Burundi",
    "emoji": "🇧🇮",
    "unicode": "U+1F1E7 U+1F1EE",
    "image": "/flags/images/BI.svg"
  },
  "BJ": {
    "name": "Benin",
    "emoji": "🇧🇯",
    "unicode": "U+1F1E7 U+1F1EF",
    "image": "/flags/images/BJ.svg"
  },
  "BL": {
    "name": "St. Barthélemy",
    "emoji": "🇧🇱",
    "unicode": "U+1F1E7 U+1F1F1",
    "image": "/flags/images/BL.svg"
  },
  "BM": {
    "name": "Bermuda",
    "emoji": "🇧🇲",
    "unicode": "U+1F1E7 U+1F1F2",
    "image": "/flags/images/BM.svg"
  },
  "BN": {
    "name": "Brunei",
    "emoji": "🇧🇳",
    "unicode": "U+1F1E7 U+1F1F3",
    "image": "/flags/images/BN.svg"
  },
  "BO": {
    "name": "Bolivia",
    "emoji": "🇧🇴",
    "unicode": "U+1F1E7 U+1F1F4",
    "image": "/flags/images/BO.svg"
  },
  "BQ": {
    "name": "Caribbean Netherlands",
    "emoji": "🇧🇶",
    "unicode": "U+1F1E7 U+1F1F6",
    "image": "/flags/images/BQ.svg"
  },
  "BR": {
    "name": "Brazil",
    "emoji": "🇧🇷",
    "unicode": "U+1F1E7 U+1F1F7",
    "image": "/flags/images/BR.svg"
  },
  "BS": {
    "name": "Bahamas",
    "emoji": "🇧🇸",
    "unicode": "U+1F1E7 U+1F1F8",
    "image": "/flags/images/BS.svg"
  },
  "BT": {
    "name": "Bhutan",
    "emoji": "🇧🇹",
    "unicode": "U+1F1E7 U+1F1F9",
    "image": "/flags/images/BT.svg"
  },
  "BV": {
    "name": "Bouvet Island",
    "emoji": "🇧🇻",
    "unicode": "U+1F1E7 U+1F1FB",
    "image": "/flags/images/BV.svg"
  },
  "BW": {
    "name": "Botswana",
    "emoji": "🇧🇼",
    "unicode": "U+1F1E7 U+1F1FC",
    "image": "/flags/images/BW.svg"
  },
  "BY": {
    "name": "Belarus",
    "emoji": "🇧🇾",
    "unicode": "U+1F1E7 U+1F1FE",
    "image": "/flags/images/BY.svg"
  },
  "BZ": {
    "name": "Belize",
    "emoji": "🇧🇿",
    "unicode": "U+1F1E7 U+1F1FF",
    "image": "/flags/images/BZ.svg"
  },
  "CA": {
    "name": "Canada",
    "emoji": "🇨🇦",
    "unicode": "U+1F1E8 U+1F1E6",
    "image": "/flags/images/CA.svg"
  },
  "CC": {
    "name": "Cocos (Keeling) Islands",
    "emoji": "🇨🇨",
    "unicode": "U+1F1E8 U+1F1E8",
    "image": "/flags/images/CC.svg"
  },
  "CD": {
    "name": "Congo - Kinshasa",
    "emoji": "🇨🇩",
    "unicode": "U+1F1E8 U+1F1E9",
    "image": "/flags/images/CD.svg"
  },
  "CF": {
    "name": "Central African Republic",
    "emoji": "🇨🇫",
    "unicode": "U+1F1E8 U+1F1EB",
    "image": "/flags/images/CF.svg"
  },
  "CG": {
    "name": "Congo - Brazzaville",
    "emoji": "🇨🇬",
    "unicode": "U+1F1E8 U+1F1EC",
    "image": "/flags/images/CG.svg"
  },
  "CH": {
    "name": "Switzerland",
    "emoji": "🇨🇭",
    "unicode": "U+1F1E8 U+1F1ED",
    "image": "/flags/images/CH.svg"
  },
  "CI": {
    "name": "Côte d’Ivoire",
    "emoji": "🇨🇮",
    "unicode": "U+1F1E8 U+1F1EE",
    "image": "/flags/images/CI.svg"
  },
  "CK": {
    "name": "Cook Islands",
    "emoji": "🇨🇰",
    "unicode": "U+1F1E8 U+1F1F0",
    "image": "/flags/images/CK.svg"
  },
  "CL": {
    "name": "Chile",
    "emoji": "🇨🇱",
    "unicode": "U+1F1E8 U+1F1F1",
    "image": "/flags/images/CL.svg"
  },
  "CM": {
    "name": "Cameroon",
    "emoji": "🇨🇲",
    "unicode": "U+1F1E8 U+1F1F2",
    "image": "/flags/images/CM.svg"
  },
  "CN": {
    "name": "China",
    "emoji": "🇨🇳",
    "unicode": "U+1F1E8 U+1F1F3",
    "image": "/flags/images/CN.svg"
  },
  "CO": {
    "name": "Colombia",
    "emoji": "🇨🇴",
    "unicode": "U+1F1E8 U+1F1F4",
    "image": "/flags/images/CO.svg"
  },
  "CP": {
    "name": "Clipperton Island",
    "emoji": "🇨🇵",
    "unicode": "U+1F1E8 U+1F1F5",
    "image": "/flags/images/CP.svg"
  },
  "CR": {
    "name": "Costa Rica",
    "emoji": "🇨🇷",
    "unicode": "U+1F1E8 U+1F1F7",
    "image": "/flags/images/CR.svg"
  },
  "CU": {
    "name": "Cuba",
    "emoji": "🇨🇺",
    "unicode": "U+1F1E8 U+1F1FA",
    "image": "/flags/images/CU.svg"
  },
  "CV": {
    "name": "Cape Verde",
    "emoji": "🇨🇻",
    "unicode": "U+1F1E8 U+1F1FB",
    "image": "/flags/images/CV.svg"
  },
  "CW": {
    "name": "Curaçao",
    "emoji": "🇨🇼",
    "unicode": "U+1F1E8 U+1F1FC",
    "image": "/flags/images/CW.svg"
  },
  "CX": {
    "name": "Christmas Island",
    "emoji": "🇨🇽",
    "unicode": "U+1F1E8 U+1F1FD",
    "image": "/flags/images/CX.svg"
  },
  "CY": {
    "name": "Cyprus",
    "emoji": "🇨🇾",
    "unicode": "U+1F1E8 U+1F1FE",
    "image": "/flags/images/CY.svg"
  },
  "CZ": {
    "name": "Czechia",
    "emoji": "🇨🇿",
    "unicode": "U+1F1E8 U+1F1FF",
    "image": "/flags/images/CZ.svg"
  },
  "DE": {
    "name": "Germany",
    "emoji": "🇩🇪",
    "unicode": "U+1F1E9 U+1F1EA",
    "image": "/flags/images/DE.svg"
  },
  "DG": {
    "name": "Diego Garcia",
    "emoji": "🇩🇬",
    "unicode": "U+1F1E9 U+1F1EC",
    "image": "/flags/images/DG.svg"
  },
  "DJ": {
    "name": "Djibouti",
    "emoji": "🇩🇯",
    "unicode": "U+1F1E9 U+1F1EF",
    "image": "/flags/images/DJ.svg"
  },
  "DK": {
    "name": "Denmark",
    "emoji": "🇩🇰",
    "unicode": "U+1F1E9 U+1F1F0",
    "image": "/flags/images/DK.svg"
  },
  "DM": {
    "name": "Dominica",
    "emoji": "🇩🇲",
    "unicode": "U+1F1E9 U+1F1F2",
    "image": "/flags/images/DM.svg"
  },
  "DO": {
    "name": "Dominican Republic",
    "emoji": "🇩🇴",
    "unicode": "U+1F1E9 U+1F1F4",
    "image": "/flags/images/DO.svg"
  },
  "DZ": {
    "name": "Algeria",
    "emoji": "🇩🇿",
    "unicode": "U+1F1E9 U+1F1FF",
    "image": "/flags/images/DZ.svg"
  },
  "EA": {
    "name": "Ceuta & Melilla",
    "emoji": "🇪🇦",
    "unicode": "U+1F1EA U+1F1E6",
    "image": "/flags/images/EA.svg"
  },
  "EC": {
    "name": "Ecuador",
    "emoji": "🇪🇨",
    "unicode": "U+1F1EA U+1F1E8",
    "image": "/flags/images/EC.svg"
  },
  "EE": {
    "name": "Estonia",
    "emoji": "🇪🇪",
    "unicode": "U+1F1EA U+1F1EA",
    "image": "/flags/images/EE.svg"
  },
  "EG": {
    "name": "Egypt",
    "emoji": "🇪🇬",
    "unicode": "U+1F1EA U+1F1EC",
    "image": "/flags/images/EG.svg"
  },
  "EH": {
    "name": "Western Sahara",
    "emoji": "🇪🇭",
    "unicode": "U+1F1EA U+1F1ED",
    "image": "/flags/images/EH.svg"
  },
  "ER": {
    "name": "Eritrea",
    "emoji": "🇪🇷",
    "unicode": "U+1F1EA U+1F1F7",
    "image": "/flags/images/ER.svg"
  },
  "ES": {
    "name": "Spain",
    "emoji": "🇪🇸",
    "unicode": "U+1F1EA U+1F1F8",
    "image": "/flags/images/ES.svg"
  },
  "ET": {
    "name": "Ethiopia",
    "emoji": "🇪🇹",
    "unicode": "U+1F1EA U+1F1F9",
    "image": "/flags/images/ET.svg"
  },
  "EU": {
    "name": "European Union",
    "emoji": "🇪🇺",
    "unicode": "U+1F1EA U+1F1FA",
    "image": "/flags/images/EU.svg"
  },
  "FI": {
    "name": "Finland",
    "emoji": "🇫🇮",
    "unicode": "U+1F1EB U+1F1EE",
    "image": "/flags/images/FI.svg"
  },
  "FJ": {
    "name": "Fiji",
    "emoji": "🇫🇯",
    "unicode": "U+1F1EB U+1F1EF",
    "image": "/flags/images/FJ.svg"
  },
  "FK": {
    "name": "Falkland Islands",
    "emoji": "🇫🇰",
    "unicode": "U+1F1EB U+1F1F0",
    "image": "/flags/images/FK.svg"
  },
  "FM": {
    "name": "Micronesia",
    "emoji": "🇫🇲",
    "unicode": "U+1F1EB U+1F1F2",
    "image": "/flags/images/FM.svg"
  },
  "FO": {
    "name": "Faroe Islands",
    "emoji": "🇫🇴",
    "unicode": "U+1F1EB U+1F1F4",
    "image": "/flags/images/FO.svg"
  },
  "FR": {
    "name": "France",
    "emoji": "🇫🇷",
    "unicode": "U+1F1EB U+1F1F7",
    "image": "/flags/images/FR.svg"
  },
  "GA": {
    "name": "Gabon",
    "emoji": "🇬🇦",
    "unicode": "U+1F1EC U+1F1E6",
    "image": "/flags/images/GA.svg"
  },
  "GB": {
    "name": "United Kingdom",
    "emoji": "🇬🇧",
    "unicode": "U+1F1EC U+1F1E7",
    "image": "/flags/images/GB.svg"
  },
  "GD": {
    "name": "Grenada",
    "emoji": "🇬🇩",
    "unicode": "U+1F1EC U+1F1E9",
    "image": "/flags/images/GD.svg"
  },
  "GE": {
    "name": "Georgia",
    "emoji": "🇬🇪",
    "unicode": "U+1F1EC U+1F1EA",
    "image": "/flags/images/GE.svg"
  },
  "GF": {
    "name": "French Guiana",
    "emoji": "🇬🇫",
    "unicode": "U+1F1EC U+1F1EB",
    "image": "/flags/images/GF.svg"
  },
  "GG": {
    "name": "Guernsey",
    "emoji": "🇬🇬",
    "unicode": "U+1F1EC U+1F1EC",
    "image": "/flags/images/GG.svg"
  },
  "GH": {
    "name": "Ghana",
    "emoji": "🇬🇭",
    "unicode": "U+1F1EC U+1F1ED",
    "image": "/flags/images/GH.svg"
  },
  "GI": {
    "name": "Gibraltar",
    "emoji": "🇬🇮",
    "unicode": "U+1F1EC U+1F1EE",
    "image": "/flags/images/GI.svg"
  },
  "GL": {
    "name": "Greenland",
    "emoji": "🇬🇱",
    "unicode": "U+1F1EC U+1F1F1",
    "image": "/flags/images/GL.svg"
  },
  "GM": {
    "name": "Gambia",
    "emoji": "🇬🇲",
    "unicode": "U+1F1EC U+1F1F2",
    "image": "/flags/images/GM.svg"
  },
  "GN": {
    "name": "Guinea",
    "emoji": "🇬🇳",
    "unicode": "U+1F1EC U+1F1F3",
    "image": "/flags/images/GN.svg"
  },
  "GP": {
    "name": "Guadeloupe",
    "emoji": "🇬🇵",
    "unicode": "U+1F1EC U+1F1F5",
    "image": "/flags/images/GP.svg"
  },
  "GQ": {
    "name": "Equatorial Guinea",
    "emoji": "🇬🇶",
    "unicode": "U+1F1EC U+1F1F6",
    "image": "/flags/images/GQ.svg"
  },
  "GR": {
    "name": "Greece",
    "emoji": "🇬🇷",
    "unicode": "U+1F1EC U+1F1F7",
    "image": "/flags/images/GR.svg"
  },
  "GS": {
    "name": "South Georgia & South Sandwich Islands",
    "emoji": "🇬🇸",
    "unicode": "U+1F1EC U+1F1F8",
    "image": "/flags/images/GS.svg"
  },
  "GT": {
    "name": "Guatemala",
    "emoji": "🇬🇹",
    "unicode": "U+1F1EC U+1F1F9",
    "image": "/flags/images/GT.svg"
  },
  "GU": {
    "name": "Guam",
    "emoji": "🇬🇺",
    "unicode": "U+1F1EC U+1F1FA",
    "image": "/flags/images/GU.svg"
  },
  "GW": {
    "name": "Guinea-Bissau",
    "emoji": "🇬🇼",
    "unicode": "U+1F1EC U+1F1FC",
    "image": "/flags/images/GW.svg"
  },
  "GY": {
    "name": "Guyana",
    "emoji": "🇬🇾",
    "unicode": "U+1F1EC U+1F1FE",
    "image": "/flags/images/GY.svg"
  },
  "HK": {
    "name": "Hong Kong SAR China",
    "emoji": "🇭🇰",
    "unicode": "U+1F1ED U+1F1F0",
    "image": "/flags/images/HK.svg"
  },
  "HM": {
    "name": "Heard & McDonald Islands",
    "emoji": "🇭🇲",
    "unicode": "U+1F1ED U+1F1F2",
    "image": "/flags/images/HM.svg"
  },
  "HN": {
    "name": "Honduras",
    "emoji": "🇭🇳",
    "unicode": "U+1F1ED U+1F1F3",
    "image": "/flags/images/HN.svg"
  },
  "HR": {
    "name": "Croatia",
    "emoji": "🇭🇷",
    "unicode": "U+1F1ED U+1F1F7",
    "image": "/flags/images/HR.svg"
  },
  "HT": {
    "name": "Haiti",
    "emoji": "🇭🇹",
    "unicode": "U+1F1ED U+1F1F9",
    "image": "/flags/images/HT.svg"
  },
  "HU": {
    "name": "Hungary",
    "emoji": "🇭🇺",
    "unicode": "U+1F1ED U+1F1FA",
    "image": "/flags/images/HU.svg"
  },
  "IC": {
    "name": "Canary Islands",
    "emoji": "🇮🇨",
    "unicode": "U+1F1EE U+1F1E8",
    "image": "/flags/images/IC.svg"
  },
  "ID": {
    "name": "Indonesia",
    "emoji": "🇮🇩",
    "unicode": "U+1F1EE U+1F1E9",
    "image": "/flags/images/ID.svg"
  },
  "IE": {
    "name": "Ireland",
    "emoji": "🇮🇪",
    "unicode": "U+1F1EE U+1F1EA",
    "image": "/flags/images/IE.svg"
  },
  "IL": {
    "name": "Israel",
    "emoji": "🇮🇱",
    "unicode": "U+1F1EE U+1F1F1",
    "image": "/flags/images/IL.svg"
  },
  "IM": {
    "name": "Isle of Man",
    "emoji": "🇮🇲",
    "unicode": "U+1F1EE U+1F1F2",
    "image": "/flags/images/IM.svg"
  },
  "IN": {
    "name": "India",
    "emoji": "🇮🇳",
    "unicode": "U+1F1EE U+1F1F3",
    "image": "/flags/images/IN.svg"
  },
  "IO": {
    "name": "British Indian Ocean Territory",
    "emoji": "🇮🇴",
    "unicode": "U+1F1EE U+1F1F4",
    "image": "/flags/images/IO.svg"
  },
  "IQ": {
    "name": "Iraq",
    "emoji": "🇮🇶",
    "unicode": "U+1F1EE U+1F1F6",
    "image": "/flags/images/IQ.svg"
  },
  "IR": {
    "name": "Iran",
    "emoji": "🇮🇷",
    "unicode": "U+1F1EE U+1F1F7",
    "image": "/flags/images/IR.svg"
  },
  "IS": {
    "name": "Iceland",
    "emoji": "🇮🇸",
    "unicode": "U+1F1EE U+1F1F8",
    "image": "/flags/images/IS.svg"
  },
  "IT": {
    "name": "Italy",
    "emoji": "🇮🇹",
    "unicode": "U+1F1EE U+1F1F9",
    "image": "/flags/images/IT.svg"
  },
  "JE": {
    "name": "Jersey",
    "emoji": "🇯🇪",
    "unicode": "U+1F1EF U+1F1EA",
    "image": "/flags/images/JE.svg"
  },
  "JM": {
    "name": "Jamaica",
    "emoji": "🇯🇲",
    "unicode": "U+1F1EF U+1F1F2",
    "image": "/flags/images/JM.svg"
  },
  "JO": {
    "name": "Jordan",
    "emoji": "🇯🇴",
    "unicode": "U+1F1EF U+1F1F4",
    "image": "/flags/images/JO.svg"
  },
  "JP": {
    "name": "Japan",
    "emoji": "🇯🇵",
    "unicode": "U+1F1EF U+1F1F5",
    "image": "/flags/images/JP.svg"
  },
  "KE": {
    "name": "Kenya",
    "emoji": "🇰🇪",
    "unicode": "U+1F1F0 U+1F1EA",
    "image": "/flags/images/KE.svg"
  },
  "KG": {
    "name": "Kyrgyzstan",
    "emoji": "🇰🇬",
    "unicode": "U+1F1F0 U+1F1EC",
    "image": "/flags/images/KG.svg"
  },
  "KH": {
    "name": "Cambodia",
    "emoji": "🇰🇭",
    "unicode": "U+1F1F0 U+1F1ED",
    "image": "/flags/images/KH.svg"
  },
  "KI": {
    "name": "Kiribati",
    "emoji": "🇰🇮",
    "unicode": "U+1F1F0 U+1F1EE",
    "image": "/flags/images/KI.svg"
  },
  "KM": {
    "name": "Comoros",
    "emoji": "🇰🇲",
    "unicode": "U+1F1F0 U+1F1F2",
    "image": "/flags/images/KM.svg"
  },
  "KN": {
    "name": "St. Kitts & Nevis",
    "emoji": "🇰🇳",
    "unicode": "U+1F1F0 U+1F1F3",
    "image": "/flags/images/KN.svg"
  },
  "KP": {
    "name": "North Korea",
    "emoji": "🇰🇵",
    "unicode": "U+1F1F0 U+1F1F5",
    "image": "/flags/images/KP.svg"
  },
  "KR": {
    "name": "South Korea",
    "emoji": "🇰🇷",
    "unicode": "U+1F1F0 U+1F1F7",
    "image": "/flags/images/KR.svg"
  },
  "KW": {
    "name": "Kuwait",
    "emoji": "🇰🇼",
    "unicode": "U+1F1F0 U+1F1FC",
    "image": "/flags/images/KW.svg"
  },
  "KY": {
    "name": "Cayman Islands",
    "emoji": "🇰🇾",
    "unicode": "U+1F1F0 U+1F1FE",
    "image": "/flags/images/KY.svg"
  },
  "KZ": {
    "name": "Kazakhstan",
    "emoji": "🇰🇿",
    "unicode": "U+1F1F0 U+1F1FF",
    "image": "/flags/images/KZ.svg"
  },
  "LA": {
    "name": "Laos",
    "emoji": "🇱🇦",
    "unicode": "U+1F1F1 U+1F1E6",
    "image": "/flags/images/LA.svg"
  },
  "LB": {
    "name": "Lebanon",
    "emoji": "🇱🇧",
    "unicode": "U+1F1F1 U+1F1E7",
    "image": "/flags/images/LB.svg"
  },
  "LC": {
    "name": "St. Lucia",
    "emoji": "🇱🇨",
    "unicode": "U+1F1F1 U+1F1E8",
    "image": "/flags/images/LC.svg"
  },
  "LI": {
    "name": "Liechtenstein",
    "emoji": "🇱🇮",
    "unicode": "U+1F1F1 U+1F1EE",
    "image": "/flags/images/LI.svg"
  },
  "LK": {
    "name": "Sri Lanka",
    "emoji": "🇱🇰",
    "unicode": "U+1F1F1 U+1F1F0",
    "image": "/flags/images/LK.svg"
  },
  "LR": {
    "name": "Liberia",
    "emoji": "🇱🇷",
    "unicode": "U+1F1F1 U+1F1F7",
    "image": "/flags/images/LR.svg"
  },
  "LS": {
    "name": "Lesotho",
    "emoji": "🇱🇸",
    "unicode": "U+1F1F1 U+1F1F8",
    "image": "/flags/images/LS.svg"
  },
  "LT": {
    "name": "Lithuania",
    "emoji": "🇱🇹",
    "unicode": "U+1F1F1 U+1F1F9",
    "image": "/flags/images/LT.svg"
  },
  "LU": {
    "name": "Luxembourg",
    "emoji": "🇱🇺",
    "unicode": "U+1F1F1 U+1F1FA",
    "image": "/flags/images/LU.svg"
  },
  "LV": {
    "name": "Latvia",
    "emoji": "🇱🇻",
    "unicode": "U+1F1F1 U+1F1FB",
    "image": "/flags/images/LV.svg"
  },
  "LY": {
    "name": "Libya",
    "emoji": "🇱🇾",
    "unicode": "U+1F1F1 U+1F1FE",
    "image": "/flags/images/LY.svg"
  },
  "MA": {
    "name": "Morocco",
    "emoji": "🇲🇦",
    "unicode": "U+1F1F2 U+1F1E6",
    "image": "/flags/images/MA.svg"
  },
  "MC": {
    "name": "Monaco",
    "emoji": "🇲🇨",
    "unicode": "U+1F1F2 U+1F1E8",
    "image": "/flags/images/MC.svg"
  },
  "MD": {
    "name": "Moldova",
    "emoji": "🇲🇩",
    "unicode": "U+1F1F2 U+1F1E9",
    "image": "/flags/images/MD.svg"
  },
  "ME": {
    "name": "Montenegro",
    "emoji": "🇲🇪",
    "unicode": "U+1F1F2 U+1F1EA",
    "image": "/flags/images/ME.svg"
  },
  "MF": {
    "name": "St. Martin",
    "emoji": "🇲🇫",
    "unicode": "U+1F1F2 U+1F1EB",
    "image": "/flags/images/MF.svg"
  },
  "MG": {
    "name": "Madagascar",
    "emoji": "🇲🇬",
    "unicode": "U+1F1F2 U+1F1EC",
    "image": "/flags/images/MG.svg"
  },
  "MH": {
    "name": "Marshall Islands",
    "emoji": "🇲🇭",
    "unicode": "U+1F1F2 U+1F1ED",
    "image": "/flags/images/MH.svg"
  },
  "MK": {
    "name": "North Macedonia",
    "emoji": "🇲🇰",
    "unicode": "U+1F1F2 U+1F1F0",
    "image": "/flags/images/MK.svg"
  },
  "ML": {
    "name": "Mali",
    "emoji": "🇲🇱",
    "unicode": "U+1F1F2 U+1F1F1",
    "image": "/flags/images/ML.svg"
  },
  "MM": {
    "name": "Myanmar (Burma)",
    "emoji": "🇲🇲",
    "unicode": "U+1F1F2 U+1F1F2",
    "image": "/flags/images/MM.svg"
  },
  "MN": {
    "name": "Mongolia",
    "emoji": "🇲🇳",
    "unicode": "U+1F1F2 U+1F1F3",
    "image": "/flags/images/MN.svg"
  },
  "MO": {
    "name": "Macao SAR China",
    "emoji": "🇲🇴",
    "unicode": "U+1F1F2 U+1F1F4",
    "image": "/flags/images/MO.svg"
  },
  "MP": {
    "name": "Northern Mariana Islands",
    "emoji": "🇲🇵",
    "unicode": "U+1F1F2 U+1F1F5",
    "image": "/flags/images/MP.svg"
  },
  "MQ": {
    "name": "Martinique",
    "emoji": "🇲🇶",
    "unicode": "U+1F1F2 U+1F1F6",
    "image": "/flags/images/MQ.svg"
  },
  "MR": {
    "name": "Mauritania",
    "emoji": "🇲🇷",
    "unicode": "U+1F1F2 U+1F1F7",
    "image": "/flags/images/MR.svg"
  },
  "MS": {
    "name": "Montserrat",
    "emoji": "🇲🇸",
    "unicode": "U+1F1F2 U+1F1F8",
    "image": "/flags/images/MS.svg"
  },
  "MT": {
    "name": "Malta",
    "emoji": "🇲🇹",
    "unicode": "U+1F1F2 U+1F1F9",
    "image": "/flags/images/MT.svg"
  },
  "MU": {
    "name": "Mauritius",
    "emoji": "🇲🇺",
    "unicode": "U+1F1F2 U+1F1FA",
    "image": "/flags/images/MU.svg"
  },
  "MV": {
    "name": "Maldives",
    "emoji": "🇲🇻",
    "unicode": "U+1F1F2 U+1F1FB",
    "image": "/flags/images/MV.svg"
  },
  "MW": {
    "name": "Malawi",
    "emoji": "🇲🇼",
    "unicode": "U+1F1F2 U+1F1FC",
    "image": "/flags/images/MW.svg"
  },
  "MX": {
    "name": "Mexico",
    "emoji": "🇲🇽",
    "unicode": "U+1F1F2 U+1F1FD",
    "image": "/flags/images/MX.svg"
  },
  "MY": {
    "name": "Malaysia",
    "emoji": "🇲🇾",
    "unicode": "U+1F1F2 U+1F1FE",
    "image": "/flags/images/MY.svg"
  },
  "MZ": {
    "name": "Mozambique",
    "emoji": "🇲🇿",
    "unicode": "U+1F1F2 U+1F1FF",
    "image": "/flags/images/MZ.svg"
  },
  "NA": {
    "name": "Namibia",
    "emoji": "🇳🇦",
    "unicode": "U+1F1F3 U+1F1E6",
    "image": "/flags/images/NA.svg"
  },
  "NC": {
    "name": "New Caledonia",
    "emoji": "🇳🇨",
    "unicode": "U+1F1F3 U+1F1E8",
    "image": "/flags/images/NC.svg"
  },
  "NE": {
    "name": "Niger",
    "emoji": "🇳🇪",
    "unicode": "U+1F1F3 U+1F1EA",
    "image": "/flags/images/NE.svg"
  },
  "NF": {
    "name": "Norfolk Island",
    "emoji": "🇳🇫",
    "unicode": "U+1F1F3 U+1F1EB",
    "image": "/flags/images/NF.svg"
  },
  "NG": {
    "name": "Nigeria",
    "emoji": "🇳🇬",
    "unicode": "U+1F1F3 U+1F1EC",
    "image": "/flags/images/NG.svg"
  },
  "NI": {
    "name": "Nicaragua",
    "emoji": "🇳🇮",
    "unicode": "U+1F1F3 U+1F1EE",
    "image": "/flags/images/NI.svg"
  },
  "NL": {
    "name": "Netherlands",
    "emoji": "🇳🇱",
    "unicode": "U+1F1F3 U+1F1F1",
    "image": "/flags/images/NL.svg"
  },
  "NO": {
    "name": "Norway",
    "emoji": "🇳🇴",
    "unicode": "U+1F1F3 U+1F1F4",
    "image": "/flags/images/NO.svg"
  },
  "NP": {
    "name": "Nepal",
    "emoji": "🇳🇵",
    "unicode": "U+1F1F3 U+1F1F5",
    "image": "/flags/images/NP.svg"
  },
  "NR": {
    "name": "Nauru",
    "emoji": "🇳🇷",
    "unicode": "U+1F1F3 U+1F1F7",
    "image": "/flags/images/NR.svg"
  },
  "NU": {
    "name": "Niue",
    "emoji": "🇳🇺",
    "unicode": "U+1F1F3 U+1F1FA",
    "image": "/flags/images/NU.svg"
  },
  "NZ": {
    "name": "New Zealand",
    "emoji": "🇳🇿",
    "unicode": "U+1F1F3 U+1F1FF",
    "image": "/flags/images/NZ.svg"
  },
  "OM": {
    "name": "Oman",
    "emoji": "🇴🇲",
    "unicode": "U+1F1F4 U+1F1F2",
    "image": "/flags/images/OM.svg"
  },
  "PA": {
    "name": "Panama",
    "emoji": "🇵🇦",
    "unicode": "U+1F1F5 U+1F1E6",
    "image": "/flags/images/PA.svg"
  },
  "PE": {
    "name": "Peru",
    "emoji": "🇵🇪",
    "unicode": "U+1F1F5 U+1F1EA",
    "image": "/flags/images/PE.svg"
  },
  "PF": {
    "name": "French Polynesia",
    "emoji": "🇵🇫",
    "unicode": "U+1F1F5 U+1F1EB",
    "image": "/flags/images/PF.svg"
  },
  "PG": {
    "name": "Papua New Guinea",
    "emoji": "🇵🇬",
    "unicode": "U+1F1F5 U+1F1EC",
    "image": "/flags/images/PG.svg"
  },
  "PH": {
    "name": "Philippines",
    "emoji": "🇵🇭",
    "unicode": "U+1F1F5 U+1F1ED",
    "image": "/flags/images/PH.svg"
  },
  "PK": {
    "name": "Pakistan",
    "emoji": "🇵🇰",
    "unicode": "U+1F1F5 U+1F1F0",
    "image": "/flags/images/PK.svg"
  },
  "PL": {
    "name": "Poland",
    "emoji": "🇵🇱",
    "unicode": "U+1F1F5 U+1F1F1",
    "image": "/flags/images/PL.svg"
  },
  "PM": {
    "name": "St. Pierre & Miquelon",
    "emoji": "🇵🇲",
    "unicode": "U+1F1F5 U+1F1F2",
    "image": "/flags/images/PM.svg"
  },
  "PN": {
    "name": "Pitcairn Islands",
    "emoji": "🇵🇳",
    "unicode": "U+1F1F5 U+1F1F3",
    "image": "/flags/images/PN.svg"
  },
  "PR": {
    "name": "Puerto Rico",
    "emoji": "🇵🇷",
    "unicode": "U+1F1F5 U+1F1F7",
    "image": "/flags/images/PR.svg"
  },
  "PS": {
    "name": "Palestinian Territories",
    "emoji": "🇵🇸",
    "unicode": "U+1F1F5 U+1F1F8",
    "image": "/flags/images/PS.svg"
  },
  "PT": {
    "name": "Portugal",
    "emoji": "🇵🇹",
    "unicode": "U+1F1F5 U+1F1F9",
    "image": "/flags/images/PT.svg"
  },
  "PW": {
    "name": "Palau",
    "emoji": "🇵🇼",
    "unicode": "U+1F1F5 U+1F1FC",
    "image": "/flags/images/PW.svg"
  },
  "PY": {
    "name": "Paraguay",
    "emoji": "🇵🇾",
    "unicode": "U+1F1F5 U+1F1FE",
    "image": "/flags/images/PY.svg"
  },
  "QA": {
    "name": "Qatar",
    "emoji": "🇶🇦",
    "unicode": "U+1F1F6 U+1F1E6",
    "image": "/flags/images/QA.svg"
  },
  "RE": {
    "name": "Réunion",
    "emoji": "🇷🇪",
    "unicode": "U+1F1F7 U+1F1EA",
    "image": "/flags/images/RE.svg"
  },
  "RO": {
    "name": "Romania",
    "emoji": "🇷🇴",
    "unicode": "U+1F1F7 U+1F1F4",
    "image": "/flags/images/RO.svg"
  },
  "RS": {
    "name": "Serbia",
    "emoji": "🇷🇸",
    "unicode": "U+1F1F7 U+1F1F8",
    "image": "/flags/images/RS.svg"
  },
  "RU": {
    "name": "Russia",
    "emoji": "🇷🇺",
    "unicode": "U+1F1F7 U+1F1FA",
    "image": "/flags/images/RU.svg"
  },
  "RW": {
    "name": "Rwanda",
    "emoji": "🇷🇼",
    "unicode": "U+1F1F7 U+1F1FC",
    "image": "/flags/images/RW.svg"
  },
  "SA": {
    "name": "Saudi Arabia",
    "emoji": "🇸🇦",
    "unicode": "U+1F1F8 U+1F1E6",
    "image": "/flags/images/SA.svg"
  },
  "SB": {
    "name": "Solomon Islands",
    "emoji": "🇸🇧",
    "unicode": "U+1F1F8 U+1F1E7",
    "image": "/flags/images/SB.svg"
  },
  "SC": {
    "name": "Seychelles",
    "emoji": "🇸🇨",
    "unicode": "U+1F1F8 U+1F1E8",
    "image": "/flags/images/SC.svg"
  },
  "SD": {
    "name": "Sudan",
    "emoji": "🇸🇩",
    "unicode": "U+1F1F8 U+1F1E9",
    "image": "/flags/images/SD.svg"
  },
  "SE": {
    "name": "Sweden",
    "emoji": "🇸🇪",
    "unicode": "U+1F1F8 U+1F1EA",
    "image": "/flags/images/SE.svg"
  },
  "SG": {
    "name": "Singapore",
    "emoji": "🇸🇬",
    "unicode": "U+1F1F8 U+1F1EC",
    "image": "/flags/images/SG.svg"
  },
  "SH": {
    "name": "St. Helena",
    "emoji": "🇸🇭",
    "unicode": "U+1F1F8 U+1F1ED",
    "image": "/flags/images/SH.svg"
  },
  "SI": {
    "name": "Slovenia",
    "emoji": "🇸🇮",
    "unicode": "U+1F1F8 U+1F1EE",
    "image": "/flags/images/SI.svg"
  },
  "SJ": {
    "name": "Svalbard & Jan Mayen",
    "emoji": "🇸🇯",
    "unicode": "U+1F1F8 U+1F1EF",
    "image": "/flags/images/SJ.svg"
  },
  "SK": {
    "name": "Slovakia",
    "emoji": "🇸🇰",
    "unicode": "U+1F1F8 U+1F1F0",
    "image": "/flags/images/SK.svg"
  },
  "SL": {
    "name": "Sierra Leone",
    "emoji": "🇸🇱",
    "unicode": "U+1F1F8 U+1F1F1",
    "image": "/flags/images/SL.svg"
  },
  "SM": {
    "name": "San Marino",
    "emoji": "🇸🇲",
    "unicode": "U+1F1F8 U+1F1F2",
    "image": "/flags/images/SM.svg"
  },
  "SN": {
    "name": "Senegal",
    "emoji": "🇸🇳",
    "unicode": "U+1F1F8 U+1F1F3",
    "image": "/flags/images/SN.svg"
  },
  "SO": {
    "name": "Somalia",
    "emoji": "🇸🇴",
    "unicode": "U+1F1F8 U+1F1F4",
    "image": "/flags/images/SO.svg"
  },
  "SR": {
    "name": "Suriname",
    "emoji": "🇸🇷",
    "unicode": "U+1F1F8 U+1F1F7",
    "image": "/flags/images/SR.svg"
  },
  "SS": {
    "name": "South Sudan",
    "emoji": "🇸🇸",
    "unicode": "U+1F1F8 U+1F1F8",
    "image": "/flags/images/SS.svg"
  },
  "ST": {
    "name": "São Tomé & Príncipe",
    "emoji": "🇸🇹",
    "unicode": "U+1F1F8 U+1F1F9",
    "image": "/flags/images/ST.svg"
  },
  "SV": {
    "name": "El Salvador",
    "emoji": "🇸🇻",
    "unicode": "U+1F1F8 U+1F1FB",
    "image": "/flags/images/SV.svg"
  },
  "SX": {
    "name": "Sint Maarten",
    "emoji": "🇸🇽",
    "unicode": "U+1F1F8 U+1F1FD",
    "image": "/flags/images/SX.svg"
  },
  "SY": {
    "name": "Syria",
    "emoji": "🇸🇾",
    "unicode": "U+1F1F8 U+1F1FE",
    "image": "/flags/images/SY.svg"
  },
  "SZ": {
    "name": "Eswatini",
    "emoji": "🇸🇿",
    "unicode": "U+1F1F8 U+1F1FF",
    "image": "/flags/images/SZ.svg"
  },
  "TA": {
    "name": "Tristan da Cunha",
    "emoji": "🇹🇦",
    "unicode": "U+1F1F9 U+1F1E6",
    "image": "/flags/images/TA.svg"
  },
  "TC": {
    "name": "Turks & Caicos Islands",
    "emoji": "🇹🇨",
    "unicode": "U+1F1F9 U+1F1E8",
    "image": "/flags/images/TC.svg"
  },
  "TD": {
    "name": "Chad",
    "emoji": "🇹🇩",
    "unicode": "U+1F1F9 U+1F1E9",
    "image": "/flags/images/TD.svg"
  },
  "TF": {
    "name": "French Southern Territories",
    "emoji": "🇹🇫",
    "unicode": "U+1F1F9 U+1F1EB",
    "image": "/flags/images/TF.svg"
  },
  "TG": {
    "name": "Togo",
    "emoji": "🇹🇬",
    "unicode": "U+1F1F9 U+1F1EC",
    "image": "/flags/images/TG.svg"
  },
  "TH": {
    "name": "Thailand",
    "emoji": "🇹🇭",
    "unicode": "U+1F1F9 U+1F1ED",
    "image": "/flags/images/TH.svg"
  },
  "TJ": {
    "name": "Tajikistan",
    "emoji": "🇹🇯",
    "unicode": "U+1F1F9 U+1F1EF",
    "image": "/flags/images/TJ.svg"
  },
  "TK": {
    "name": "Tokelau",
    "emoji": "🇹🇰",
    "unicode": "U+1F1F9 U+1F1F0",
    "image": "/flags/images/TK.svg"
  },
  "TL": {
    "name": "Timor-Leste",
    "emoji": "🇹🇱",
    "unicode": "U+1F1F9 U+1F1F1",
    "image": "/flags/images/TL.svg"
  },
  "TM": {
    "name": "Turkmenistan",
    "emoji": "🇹🇲",
    "unicode": "U+1F1F9 U+1F1F2",
    "image": "/flags/images/TM.svg"
  },
  "TN": {
    "name": "Tunisia",
    "emoji": "🇹🇳",
    "unicode": "U+1F1F9 U+1F1F3",
    "image": "/flags/images/TN.svg"
  },
  "TO": {
    "name": "Tonga",
    "emoji": "🇹🇴",
    "unicode": "U+1F1F9 U+1F1F4",
    "image": "/flags/images/TO.svg"
  },
  "TR": {
    "name": "Turkey",
    "emoji": "🇹🇷",
    "unicode": "U+1F1F9 U+1F1F7",
    "image": "/flags/images/TR.svg"
  },
  "TT": {
    "name": "Trinidad & Tobago",
    "emoji": "🇹🇹",
    "unicode": "U+1F1F9 U+1F1F9",
    "image": "/flags/images/TT.svg"
  },
  "TV": {
    "name": "Tuvalu",
    "emoji": "🇹🇻",
    "unicode": "U+1F1F9 U+1F1FB",
    "image": "/flags/images/TV.svg"
  },
  "TW": {
    "name": "Taiwan",
    "emoji": "🇹🇼",
    "unicode": "U+1F1F9 U+1F1FC",
    "image": "/flags/images/TW.svg"
  },
  "TZ": {
    "name": "Tanzania",
    "emoji": "🇹🇿",
    "unicode": "U+1F1F9 U+1F1FF",
    "image": "/flags/images/TZ.svg"
  },
  "UA": {
    "name": "Ukraine",
    "emoji": "🇺🇦",
    "unicode": "U+1F1FA U+1F1E6",
    "image": "/flags/images/UA.svg"
  },
  "UG": {
    "name": "Uganda",
    "emoji": "🇺🇬",
    "unicode": "U+1F1FA U+1F1EC",
    "image": "/flags/images/UG.svg"
  },
  "UM": {
    "name": "U.S. Outlying Islands",
    "emoji": "🇺🇲",
    "unicode": "U+1F1FA U+1F1F2",
    "image": "/flags/images/UM.svg"
  },
  "UN": {
    "name": "United Nations",
    "emoji": "🇺🇳",
    "unicode": "U+1F1FA U+1F1F3",
    "image": "/flags/images/UN.svg"
  },
  "US": {
    "name": "United States",
    "emoji": "🇺🇸",
    "unicode": "U+1F1FA U+1F1F8",
    "image": "/flags/images/US.svg"
  },
  "UY": {
    "name": "Uruguay",
    "emoji": "🇺🇾",
    "unicode": "U+1F1FA U+1F1FE",
    "image": "/flags/images/UY.svg"
  },
  "UZ": {
    "name": "Uzbekistan",
    "emoji": "🇺🇿",
    "unicode": "U+1F1FA U+1F1FF",
    "image": "/flags/images/UZ.svg"
  },
  "VA": {
    "name": "Vatican City",
    "emoji": "🇻🇦",
    "unicode": "U+1F1FB U+1F1E6",
    "image": "/flags/images/VA.svg"
  },
  "VC": {
    "name": "St. Vincent & Grenadines",
    "emoji": "🇻🇨",
    "unicode": "U+1F1FB U+1F1E8",
    "image": "/flags/images/VC.svg"
  },
  "VE": {
    "name": "Venezuela",
    "emoji": "🇻🇪",
    "unicode": "U+1F1FB U+1F1EA",
    "image": "/flags/images/VE.svg"
  },
  "VG": {
    "name": "British Virgin Islands",
    "emoji": "🇻🇬",
    "unicode": "U+1F1FB U+1F1EC",
    "image": "/flags/images/VG.svg"
  },
  "VI": {
    "name": "U.S. Virgin Islands",
    "emoji": "🇻🇮",
    "unicode": "U+1F1FB U+1F1EE",
    "image": "/flags/images/VI.svg"
  },
  "VN": {
    "name": "Vietnam",
    "emoji": "🇻🇳",
    "unicode": "U+1F1FB U+1F1F3",
    "image": "/flags/images/VN.svg"
  },
  "VU": {
    "name": "Vanuatu",
    "emoji": "🇻🇺",
    "unicode": "U+1F1FB U+1F1FA",
    "image": "/flags/images/VU.svg"
  },
  "WF": {
    "name": "Wallis & Futuna",
    "emoji": "🇼🇫",
    "unicode": "U+1F1FC U+1F1EB",
    "image": "/flags/images/WF.svg"
  },
  "WS": {
    "name": "Samoa",
    "emoji": "🇼🇸",
    "unicode": "U+1F1FC U+1F1F8",
    "image": "/flags/images/WS.svg"
  },
  "XK": {
    "name": "Kosovo",
    "emoji": "🇽🇰",
    "unicode": "U+1F1FD U+1F1F0",
    "image": "/flags/images/XK.svg"
  },
  "YE": {
    "name": "Yemen",
    "emoji": "🇾🇪",
    "unicode": "U+1F1FE U+1F1EA",
    "image": "/flags/images/YE.svg"
  },
  "YT": {
    "name": "Mayotte",
    "emoji": "🇾🇹",
    "unicode": "U+1F1FE U+1F1F9",
    "image": "/flags/images/YT.svg"
  },
  "ZA": {
    "name": "South Africa",
    "emoji": "🇿🇦",
    "unicode": "U+1F1FF U+1F1E6",
    "image": "/flags/images/ZA.svg"
  },
  "ZM": {
    "name": "Zambia",
    "emoji": "🇿🇲",
    "unicode": "U+1F1FF U+1F1F2",
    "image": "/flags/images/ZM.svg"
  },
  "ZW": {
    "name": "Zimbabwe",
    "emoji": "🇿🇼",
    "unicode": "U+1F1FF U+1F1FC",
    "image": "/flags/images/ZW.svg"
  },
  "ENGLAND": {
    "name": "England",
    "emoji": "🏴󠁧󠁢󠁥󠁮󠁧󠁿",
    "unicode": "U+1F3F4 U+E0067 U+E0062 U+E0065 U+E006E U+E0067 U+E007F",
    "image": "/flags/images/ENGLAND.svg"
  },
  "SCOTLAND": {
    "name": "Scotland",
    "emoji": "🏴󠁧󠁢󠁳󠁣󠁴󠁿",
    "unicode": "U+1F3F4 U+E0067 U+E0062 U+E0073 U+E0063 U+E0074 U+E007F",
    "image": "/flags/images/SCOTLAND.svg"
  },
  "WALES": {
    "name": "Wales",
    "emoji": "🏴󠁧󠁢󠁷󠁬󠁳󠁿",
    "unicode": "U+1F3F4 U+E0067 U+E0062 U+E0077 U+E006C U+E0073 U+E007F",
    "image": "/flags/images/WALES.svg"
  }
}

export default dataFlag;
