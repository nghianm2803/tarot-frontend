module.exports = {
    getLanguages: function(){
        return {
            'vi': 'Tiếng Việt'
        };
    },
    getLanguageFull: function(){
        return {
            'vi': 'Vietnamese',
            'en': 'English'
        };
    },
}
