import dataFlag from './flagData.js';

class flagClass {
  findFlagByCode = (code: string) => {
    if (!code) {
      return "";
    }
    code = code.toUpperCase();
    //@ts-ignore
    if (dataFlag && dataFlag[code]) {
      //@ts-ignore
      return dataFlag[code];
    } else {
      return '';
    }
  };
}

export const flagModule = new flagClass();
