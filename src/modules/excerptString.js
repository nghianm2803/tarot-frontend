export default function (string, length) {
    if (!string) {
        return string;
    }
    string = string.replace(/<[^>]*>/g, "");
    string = string.replace(/(^\s*)|(\s*$)/gi, '');
    string = string.replace(/\s\s+/g, ' ');
    string = string.replace(/,/g, ' ');
    string = string.replace(/;/g, ' ');
    string = string.replace(/\//g, ' ');
    string = string.replace(/\\/g, ' ');
    string = string.replace(/{/g, ' ');
    string = string.replace(/}/g, ' ');
    string = string.replace(/\n/g, ' ');
    string = string.replace(/\./g, ' ');
    string = string.replace(/[\{\}]/g, ' ');
    string = string.replace(/[\(\)]/g, ' ');
    string = string.replace(/[[\]]/g, ' ');
    string = string.replace(/[ ]{2,}/gi, ' ');
    let count = string.split(' ').length;
    let dem = 0;
    let i = 0;
    if (count <= length) {
        return string;
    } else {
        while (i <= string.length) {
            if (string.substr(i, 1) == ' ') {
                dem++;
            }
            if (dem == parseInt(length)) {
                return string.substr(0, i);
            }
            i++;
        }
    }
}