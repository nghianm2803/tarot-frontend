module.exports = {

  // Common 
  "Save data": "Lưu dữ liệu",
  "General Setting": "Cài đặt chung",
  "General Information": "Thông tin chung",
  "Notification Setting": "Cài đặt thông báo",
  "Language Setting": "Cài đặt ngôn ngữ",
  "Order": "Đơn hàng",
  "Orders": "Đơn hàng",
  "Setting": "Cài đặt",
  "Advisors": "Chuyên gia",
  "On": "Bật",
  "Off": "Tắt",
  "Save data Successfully!": "Lưu dữ liệu thành công",
  "Your service update Successfully!": "Dịch vụ của bạn đã được cập nhật thành công",
  "This function only for Advisor.": "Chức năng này chỉ dành cho chuyên gia",
  "Do you want to become an Advisor?": "Bạn có muốn trở thành chuyên gia?",

  // Page Banner
  "How to find yourself": "Hiểu về bản thân",
  "Order advisor now!": "Đặt hàng chuyên gia ngay bây giờ",
  "ENJOY YOUR READING": "TRẢI NGHIỆM",
  "WITH OUR ADVISOR": "VỚI CÁC CHUYÊN GIA CỦA CHUNG TÔI",
  "100% Satisfaction Guaranteed": "Đảm bảo hài lòng 100%",

  // Page NewsAdvisors
  "New Advisors": "Các chuyên gia mới",

  // Page Header & profile-layout
  "Donwload App" : "Tải ứng dụng",
  "Connect Facebook" : "Kết nối facebook",
  "Contact us": "Liên hệ",
  "Find an Advisor": "Tìm một chuyên gia",
  "Horoscopes": "Tử vi",
  "Need Helps": "Cần giúp đỡ",
  "Order History": "Lịch sử đơn hàng",
  "Balance & Transactions": "Số dư và giao dịch",
  "Service Setting": "Cài đặt dịch vụ",
  "Chat Manager": "Quản lý chat",
  "Advisor Setting": "Cài đặt của chuyên gia",
  "Invoices": "Hoá đơn",
  "Withdrawal": "Rút tiền",
  "Payment Setting": "Cài đặt thanh toán",
  "LOGIN": "ĐĂNG NHẬP",
  "SIGN IN": "ĐĂNG KÝ",
  "Login": "Đăng nhập",
  "SIGN UP": "ĐĂNG KÝ",
  "Sign Up": "Đăng ký",
  "LOGOUT": "ĐĂNG XUẤT",
  "Logout": "Đăng xuất",
  "Not have Data": "Chưa có dữ liệu",
  "Balance:": "Số dư:",
  "User Menu":"Menu người dùng",

  // Page NotificationSetting
  "Enable Notification":"Bật thông báo",
  "Your notification update Successfully!": "Thông báo được cập nhật thành công!",

  // Page LanguageSetting
  "Choose language": "Chọn ngôn ngữ",
  "Your language update Successfully!": "Ngôn ngữ được cập nhật thành công!",

  // Page OrderComponent
  "Paid": "Đã thanh toán",
  "In Process": "Đang xử lý",
  "Processed": "Đã xử lý",
  "Cancelled": "Đã huỷ",
  "Refunded": "Đã hoàn tiền",
  "Draft": "Nháp",

  // Page UserBalance
  "TOPUP": "NẠP TIỀN",
  "My balance": "Số dư của tôi",
  "Balance:": "Số dư:",
  "LOAD MORE": "TẢI THÊM",

  // Page AdvisorSetting
  "Year Experience": "Năm kinh nghiệm",
  "Cover image": "Ảnh bìa",
  "Cover video": "Video bìa",
  "If you have specific instructions for your clients, please enter them below. Don't ask for personally identifiable information, such as full name, social media accounts, etc": 
  "Nếu bạn có yêu cầu cụ thể cho khách hàng của mình vui lòng nhập xuống dưới. Ví dụ: hình ảnh lòng bàn tay, hình ảnh mặt mộc, không hỏi về sức khỏe và âm phần,... Không yêu cầu thông tin nhận dạng cá nhân như tên đầy đủ, tài khoản mạng xã hội, v.v.",
  "Your profile update Successfully!": "Thông tin cá nhân cập nhật thành công!",

  // Page ServiceSetting
  "Service name": "Tên dịch vụ",
  "Description": "Mô tả",
  "Price": "Giá",
  
  // Page InvoiceComponent
  "Invoices is empty": "Không có hoá đơn nào",
  
  // Page WithdrawalComponent
  "Submit withdrawal request": "Gửi yêu cầu rút tiền",
  "Payment Method": "Phương thức rút tiền",
  "Total Balance:": "Tổng số dư:",
  "Total coin in process:": "Tổng số coin đang xử lý:",
  "Enter coin to withdrawal": "Nhập số coin để rút",
  "Request History": "Lịch sử",
  "Withdrawal coin:": "Rút coin:",
  "Total Receive: ": "Tổng số nhận được:",
  "Fee:": "Phí:",
  "Payment method:": "Phương thức chuyển tiền:",
  "History request is empty!": "Lịch sử yêu cầu trống",
  "Please add Payment method before Withdrawal!": "Vui lòng thêm phương thức thanh toán trước khi rút tiền",
  "ADD PAYMENT METHOD": "THÊM PHƯƠNG THỨC THANH TOÁN",
  "Submit a Withdrawal request successfully!": "Gửi xuất rút tiền thành công!",

  // Page PaymentSetting
  "Select payment method": "Chọn phương thức thanh toán",
  "Payment method": "Phương thức thanh toán",
  "Payment fullname": "Họ và tên",
  "Payment address": "Địa chỉ",
  "Tax identification number": "Mã số thuế",
  "Your payment update Successfully!": "Phương thức thanh toán cập nhật thành công!",
  
  // Page LoginForm
  "Hello Again!": "Xin chào!",
  "Login with Facebook": "Đăng nhập với Facebook",
  "Don't have an account?": "Chưa có tài khoản?",
  "Sign up now": "Đăng ký ngay",
  ". If you have an account by e-mail and password, click": ". Nếu bạn có tài khoản, nhấn vào",
  "here": "đây",
  "to login.": "để đăng nhập.",
  "Forgot password": "Quên mật khẩu",
  "Remember for 30 days": "Nhớ mật khẩu trong 30 ngày",
  "Login...": "Đang đăng nhập...",

  // Page RecoveryPassword
  "Reset password": "Đặt lại mật khẩu",
  "Enter the email associated with your account and we'll send an email with instructions to reset your password": 
  "Nhập email được liên kết với tài khoản của bạn và chúng tôi sẽ gửi email kèm theo hướng dẫn để đặt lại mật khẩu của bạn",
  "Submit Successfully!": "Gửi yêu cầu thành công",
  "Submit your information successfully!": "Gửi thông tin của bạn thành công!",

  // Page RegisterAdvisorForm
  "Go to homepage": "Đi tới trang chủ",
  "Become an Advisor!": "Trở thành chuyên gia!",
  "Sign up with Fuda first!": "Đăng ký với Fuda",
  "Thank you for your interest in Fudary World, we recruit only the best advisors, to ensure the highest lovel of satisfaction for our user. Please carefully review the requirements and proceed with the application only if you feel you are qualified.": 
  "Cảm ơn bạn đã quan tâm đến Fuda, chúng tôi tuyển dụng những chuyên gia để giúp đỡ, giải đáp thắc mắc, chỉ dẫn con đường cho khách hàng vì vậy vui lòng xem xét cẩn thận các yêu cầu.",
  "Display name": "Tên hiển thị",
  "Social media": "Mạng xã hội Facebook hoặc Zalo",
  "Please write down your Facebook or Zalo account you use often use": 
  "Đây là thông tin mà chúng tôi sẽ liên lạc với bạn, chú ý điền chính xác đường link Facebook, Zalo",
  "Describe experience": "Mô tả kinh nghiệm",
  "Describe yourself": "Mô tả ngắn về bạn",
  "Video about you": "Video về bạn",
  "Post a video 45 seconds to 1 minute to introduce yourself": 
  "Đăng một video khoảng 45 giây đến 1 phút để giới thiệu về bản thân",
  "Topic": "Chủ đề",
  "Choose topic you can serve for your service. You can select multiple topic.": "Chọn chủ đề bạn có thể cung cấp. Bạn có thể chọn nhiều chủ đề.",
  "Choose category you can serve for your service. You can select multiple category.": "Chọn thể loại bạn có thể cung cấp. Bạn có thể chọn nhiều thể loại.",
  "Your target": "Mục tiêu của bạn",
  "Time slot": "Khung thời gian",
  "Enter working time slot you expect.": "Nhập khung thời gian bạn muốn làm việc",
  "Image of your tool": "Ảnh dụng cụ của bạn",
  "Post an image of your tool if you have": "Đăng một tấm ảnh về dụng cụ của bạn nếu có",
  "Order Instruction": "Yêu cầu đối với khách hàng",
  "Test reading": "Bài kiểm tra",
  "Thanks for applying to become an advisor at Fudary World!": "Cảm ơn bạn đã đăng ký trở thành chuyên gia tại Fudary World!",
  "This is a test order so we know how you'll respond to clients. Please provide your professinal respond, insights a client. Consider your presentation, speal clearly and use the tools essential to perform your reading.":  
  "Đây là bài kiểm tra thử để chúng tôi biết bạn sẽ phản hồi như thế nào với khách hàng. Vui lòng đưa ra câu trả lời của bạn.", 
  "Where did you here about us?": "Bạn biết tới chúng tôi qua đâu?",
  "From a friend": "Từ một người bạn",
  "Become an Advisor...": "Trở thành chuyên gia...",
  "Sign up with Facebook": "Đăng ký với Facebook",
  "Sign up with E-mail": "Đăng ký với E-mail",
  "Email address": "Địa chỉ email",
  "Create new a Password": "Tạo mật khẩu mới",
  "Sign up": "Đăng ký",
  "Signing up...": "Đăng ký...",
  "Choose country": "Chọn quốc gia",
  "Summit successfully!": "Gửi yêu cầu thành công!",

  // Page RegisterForm
  "Sign up!": "Đăng ký!",
  "Create account to start using Fuda.": "Tạo một tài khoản để sử dụng Fuda",
  "Already an account?": "Đã có tài khoản?",
  "Login now": "Đăng nhập ngay",
  
  // Page SubmitContactUs
  "How can we help you?": "Làm sao để chúng tôi giúp bạn?",
  "Please use this online form and provide your email or other contact details so we can better help you and get back to you as soon as possible. All fields are required.": 
  "Vui lòng sử dụng biểu mẫu này để cung cấp email của bạn hoặc thông tin liên hệ khác để chúng tôi có thể trợ giúp bạn tốt hơn và liên hệ lại với bạn sớm nhất có thể. Tất cả các trường thông tin là bắt buộc.",
  "Question": "Câu hỏi",
  "Detail": "Chi tiết",

  // Page SubmitRemoveUser
  "Submit a data request": "Gửi yêu cầu dữ liệu",
  "Erase my account": "Xoá tài khoản",
  "Permanently erase the personal data associated with your account or email address.": 
  "Xóa vĩnh viễn dữ liệu cá nhân được liên kết với tài khoản hoặc địa chỉ email của bạn.",
  "This action cannot be undone": "Hành động này không thể hoàn tác",
  ". Your questions and answers will remain on the site, but will be disassociated and will not indicate your authorship even if you later return to the site.":
  ". Các câu hỏi và câu trả lời của bạn sẽ vẫn còn trên trang web, nhưng sẽ bị tách ra và sẽ không cho biết quyền tác giả của bạn ngay cả khi bạn quay lại trang web sau đó.",
  "In case you decide to remove your account, do the following:": "Trong trường hợp bạn quyết định xóa tài khoản của mình, hãy làm như sau:",
  "Step 1:": "Bước 1:",
  "Step 2:": "Bước 2:",
  "Step 3:": "Bước 3:",
  "Enter your email address in the form below.": "Nhập địa chỉ email của bạn vào biểu mẫu bên dưới.",
  "Describe reason you want to remove your account.": "Mô tả lý do bạn muốn xoá tài khoản.",
  "Click on “Submit” button.": "Click vào nút “Submit”.",
  "We'll review your request and remove your data within": "Chúng tôi sẽ xem xét yêu cầu của bạn và xoá dữ liệu của bạn trong",
  "48 hours": "48 tiếng",
  ". All the following data will be remove from the server include: Your cookie session, your user data, chat history, order history, your balance, media file and all your in-app purchases.": 
  ". Tất cả dữ liệu sau sẽ bị xóa khỏi máy chủ bao gồm: Phiên cookie, dữ liệu người dùng, lịch sử trò chuyện, lịch sử đặt hàng, số dư của bạn, tệp phương tiện và tất cả các giao dịch mua hàng trong ứng dụng của bạn.",
  "Request Details": "Chi tiết yêu cầu",

  // Page VideoComponent
  "Video order is empty": "Đơn hàng xem Video trống",

  // Page GeneralService
  "Edit service": "Sửa dịch vụ", 
  
  // Page TopAdvisors
  "See all": "Xem thêm",
  "No reviews": "Không có đánh giá",
  "coin/min": "coin/phút",
  "See more Advisors": "Xem thêm các chuyên gia khác",
  
  // Page PopularArticles
  "Popular Articles": "Bài viết phổ biến",
  "See more Articles": "Xem thêm các bài viết phổ biến",


  // Page topic
  "What topic or method are you interested in?": "Bạn quan tâm đến chủ đề hoặc thể loại nào?",
  "Connect with real professionals who have the real skills to help you":
    "Kết nối với những chuyên gia có kỹ năng thực sự để giúp bạn",

  //Page NeedHelp
  "Experts are ready to help you": "Những chuyên gia sẵn sàng giúp bạn",
  "Life is challenging enough. We make getting online psychic advice easy. Connect with experts in divination, life coaching, and more.":
    "Cuộc sống là thử thách. Chúng tôi kết nối các khuyên tâm linh trực tuyến dễ dàng. Kết nối với các chuyên gia về bói toán, lời khuyên về cuộc sống và hơn thế nữa.",
  "Get in touch in a couple of minutes": "Liên hệ sau vài phút",
  "Real ratings and reviews": "Xếp hạng và đánh giá thực tế",
  "Always ready to answer": "Luôn sẵn sàng để trả lời",
  "Get in touch in a couple of minutes": "Liên hệ sau vài phút",
  "Let's explore!": "Cùng khám phá nào!",

  // Page About-us
  "About Fuda": "Về Fuda",
  "Our platform instantly connects you to spiritual advisors and life coaches from all around the world. These are real people with real talents who use real esoteric methods to help you. Browse genuine professionals who combine real-world experience in esoteric practices like tarot reading, palmistry, clairvoyance, and mediumship with expertise in topics like love, family, career and finance. No matter what you're curious about or currently dealing with, there's an advisor who can help.": 
  "Fuda là nền tảng trung gian kết nối người dùng với các nhà tâm linh thông qua tương tác trực tiếp qua tin nhắn hoặc video. Mạng lưới các nhà tâm linh hội tụ tại Fuda của chúng tôi là người thật, được đánh giá kỹ về khả năng, những cố vấn chuyên nghiệp với chứng chỉ quốc tế trong các loại hình tâm linh khác nhau, bao gồm chiêm tinh, thần số học, xem chỉ tay, giải mã giấc mơ, tử vi, ...",
  "Your safety & security matters": "VỀ BẢO MẬT VÀ QUYỀN RIÊNG TƯ",
  "When you use our platform, you can rest easy knowing your information is handled with the utmost respect for your confidentiality and privacy. We use an extremely secure payment system and have safely processed more than 2 million transactions.": 
  "Chúng tôi rất quan tâm tới chính sách quyền riêng tư và bảo mật của người dùng, bạn hoàn toàn có thể an tâm khi sử dụng dịch vụ của chúng tôi. Mọi thông tin và giao dịch đều nằm trong sự kiểm soát từ hệ thống.",
  "Experiencing a technical issue? Need help choosing an advisor? Get in touch with our Customer Support team. The easier way to reach them is by tapping the Customer Support button in the Settings menu (where you found this page). Leave a message there or email support@appuni.io and someone will respond within 24-48 hours.": 
  "Bạn gặp vấn đề về kỹ thuật? Cần sự giúp đỡ khi lựa chọn Nhà tâm linh? Liên hệ với đội ngũ hỗ trợ của chúng tôi. Cách đơn giản và nhanh nhất là tra cứu thư mục “Hỗ trợ khách hàng” trong phần “Cài Đặt” trên cả App và Web. Hoặc để lại tin nhắn cho chúng tôi qua các phương thức liên hệ sau:",

  // Page Error
  "Opps. Have an error, please try again or go Home.": "Opps. Có lỗi xảy ra, vui lòng thử lại.",
  "GO HOME": "QUAY LẠI",

  // Page index
  "All Advisors": "Tất cả chuyên gia",

  // “”
  "Unhappy with your reading? It's important to remember that spiritual matters are not an exact science and experiences may vary. Our Customer Support Agents are happy to help you find an advisor but we are unable to provide refunds due to lack of spiritual connection or dissatisfaction with an advisor's answers. To read our full Refund Policy, go": 
  "Không hài lòng với trải nghiệm này? Điều quan trọng cần nhớ là các vấn đề tâm linh không phải là một môn khoa học chính xác và các trải nghiệm có thể khác nhau. Đội ngũ hỗ trợ khách hàng của chúng tôi rất vui khi giúp bạn tìm được cố vấn nhưng chúng tôi không thể hoàn lại tiền do thiếu kết nối tinh thần hoặc không hài lòng với câu trả lời của chuyên gia. Để đọc Chính sách hoàn lại tiền đầy đủ của chúng tôi, hãy truy cập vào",

  // Page customer-support
  "Fuda Customer Support": "Hỗ trợ khách hàng Fuda",

  // Page how-it-work
  "How Fuda work": "Fuda hoạt động như thế nào",
  "The advisors on our platform are real, professional entrepreneurs who use real abilities and methods to help you. Each advisor is a unique person with their own special talents, gifts and approach. Sometimes it takes a little while to find one that you have an amazing connection with.": 
  "Mạng lưới các nhà tâm linh hội tụ tại Fuda của chúng tôi là người thật, được đánh giá kỹ về khả năng, những cố vấn chuyên nghiệp với chứng chỉ quốc tế trong các loại hình tâm linh khác nhau, bao gồm chiêm tinh, thần số học, xem chỉ tay, giải mã giấc mơ, tử vi, ...",
  "You know that gut feeling that tells you when something is wrong? That's your intuition. Put it to work when it comes to finding an advisor. Browse our network by specialty, language or price or search by keyword. If you're curious about someone, click on their name or photo to read their full profile and see customer reviews. Listen to your intuition and choose an advisor you feel drawn to.": 
  "Chỉ cần vài bước đơn giản, từ đăng nhập, cuộn xem danh sách nhà Tâm linh, xem hồ sơ, và đánh giá, và chọn loại hình dịch vụ, là có thể trò chuyện không giới hạn với nhà Tâm linh yêu thích.",
  "What to ask": "Nên hỏi gì",
  "Got a question about your crush? Need career inspiration? Seeking out a spirit guide? Every reading centers on what you want. If you know what that is, it'll be easier to select the right advisor and ask the right questions.":
  "Những thắc mắc về các mối quan hệ, người thân, người yêu, bạn bè? Cần cảm hứng nghề nghiệp? Tìm kiếm một hướng dẫn trong bước đường tâm linh? Mỗi bài đọc đều tập trung vào những điều bạn đang băn khoăn, phân vân. Hãy xác định rõ vấn đề của bản thân, đặt câu hỏi và từ đó, dễ dàng lựa chọn loại hình và nhà Tâm linh phù hợp.",
  "Of course, not everyone goes into a reading knowing exactly what they want to discuss. Maybe you're more interested in a particular method or type of reading. You can seek out general life advice and guidance without a specific question in mind.": 
  "Hãy lưu ý rằng, các nhà Tâm linh, nhà Cố vấn sẽ đưa ra cho bạn sự chỉ dẫn, những thông điệp phù hợp mà từng lá bài, loại hình tâm linh chỉ ra cho bạn. Hãy đón nhận chúng một cách cởi mở và khách quan nhất, thay vì quá quả quyết hay áp đặt chúng vào thực tế.",
  "Your first few minutes": "Lần đầu tiên sử dụng",
  "Once you choose the advisor who appeals to you most, start a call or chat and use the first few minutes to help you decide if you want to continue with this particular person. Be clear with them about what you want. Tell them how much time you can invest in the reading. Let them know if there is a particular method you're interested in or if you have a specific question.": 
  "Sau khi chọn được cố vấn mà bạn cảm thấy có sự kết nối nhất, hãy bắt đầu cuộc trò chuyện trực tiếp hoặc đặt vấn đề thông qua video, và sử dụng những đồng xu được tặng đầu tiên để giúp bạn trải nghiệm, sau đó quyết định xem bạn có muốn tiếp tục hay không.",
  "If you don't get a good vibe from the advisor you chose, it's OK to end the conversation and find a different advisor to connect with.": 
  "Nếu bạn nhận được câu trả lời hài lòng, như mong muốn từ nhà tâm linh mà bạn đã chọn, bạn có thể kết thúc cuộc trò chuyện và tìm một cố vấn, nhà tâm linh khác để kết nối.",
  "Make sure you read the refund policy on our website. It's important to know that spiritual matters are not an exact science and experiences may vary.": 
  "Hãy đảm bảo rằng bạn đã đọc chính sách hoàn lại tiền trên trang web của chúng tôi. Hãy lưu ý rằng các vấn đề tâm linh không phải là một môn khoa học chính xác và các trải nghiệm có thể khác nhau.",
  "Every advisor on our platform is a unique individual with different methods, tools, and style. We encourage customers to take time exploring our network, reading profiles and reviews, and finding someone who truly appeals to you. To improve your experience,": 
  "Mỗi cố vấn, nhà tâm linh trên nền tảng của chúng tôi là một cá nhân độc nhất với các phương pháp, công cụ và phong cách khác nhau. Chúng tôi khuyến khích khách hàng dành thời gian , đọc hồ sơ và bài đánh giá, đồng thời tìm người thực sự thu hút bạn. Để cải thiện trải nghiệm của bạn, ",
  "please take a look at our tips for getting the best online reading possible": "vui lòng xem các mẹo của chúng tôi để đọc trực tuyến tốt nhất có thể",
  "If you're unhappy with your selected advisor, end the chat immediately and find someone new. We are unable to provide refunds due to lack of spiritual connection or dissatisfaction with an advisor's answers.": 
  "Nếu bạn không hài lòng với cố vấn đã chọn của mình, hãy kết thúc cuộc trò chuyện ngay lập tức và tìm một người mới. Chúng tôi không thể hoàn lại tiền do thiếu kết nối tinh thần hoặc không hài lòng với câu trả lời của cố vấn.",
  "As a platform, we strive to foster a safe and respectful environment for all users. Every advisor undergoes an evaluation process and we monitor all customer complaints that arise. Your feedback is an integral part of this. If you have a concern about a specific advisor, please let us know.": 
  "Là một nền tảng, chúng tôi cố gắng thúc đẩy một môi trường an toàn và tôn trọng cho tất cả người dùng. Mọi cố vấn, nhà tâm linh đều trải qua một quá trình đánh giá và chúng tôi theo dõi tất cả các khiếu nại, phản hồi của khách hàng phát sinh. Phản hồi của bạn là một phần không thể thiếu của điều này. Nếu bạn có mối quan tâm về một cố vấn cụ thể, vui lòng cho chúng tôi biết.",

  // Page BecomeAdvisorComponent
  "We are looking for the best psychics": "Tìm kiếm nhà tâm linh tài năng nhất trên thế giới",
  "Work from home or even on the go!": "Làm việc linh động bất kể thời gian hay địa điểm nào.",
  "Fuda is seeking world class professional psychics with the highest moral and ethical standards. If you are a gifted psychic, this could be the opportunity that you have been looking for. Earn money providing online readings from the comfort of your home or even on the go (using Fuda mobile app). Qualified candidates have experience using their psychic abilities and must be capable of delivering accurate and insightful readings to our customers using computer, iPad or smartphone.": 
  "Fuda luôn tìm kiếm các nhà tâm linh chuyên nghiệp đẳng cấp thế giới với các tiêu năng lực và đạo đức cao nhất. Nếu bạn là một nhà tâm linh có tài năng, đây có thể là cơ hội mà bạn đang tìm kiếm. Kiếm tiền khi cung cấp các bài đọc trực tuyến tại nhà, thời gian và địa điểm linh hoạt. Các nhà tâm linh tài năng đủ điều kiện có kinh nghiệm sử dụng khả năng ngoại cảm của họ và phải có khả năng cung cấp các bài đọc chính xác và sâu sắc cho khách hàng của chúng tôi bằng máy tính, iPad hoặc điện thoại thông minh.",
  "Our user-friendly platform will enable you to work using your PC, laptop, iPad or Smartphone": 
  "Nền tảng thân thiện với người dùng cho phép bạn làm việc trên các thiết bị khác nhau máy tính bàn, laptop, iPad hoặc điện thoại thông minh của mình.",
  "You can build your customer base and earn income when and where you wish": 
  "Xây dựng cơ sở khách hàng của mình và kiếm thu nhập bất kể khi nào và bất kì đâu bạn muốn",
  "You can make more than $2000/wk": "Thu nhập không giới hạn",
  "We pay on time and offer various payment methods": "Các hình thức thanh toán đa dạng, tích hợp trên nhiều thiết bị",
  "We provide a range of promotional opportunities": "Chương trình khuyến mãi liên tục diễn ra trên nền tảng của chúng tôi",
  "We offer reliable 24/7 support": "Dịch vụ hỗ trợ khách hàng luôn trực tuyến 24/7",
  "Apply now": "Đăng ký ngay",

  // Page disclaimer
  "Disclaimer": "Khiếu nại",
  "Fuda does not guarantee the accuracy or success of any answers, advice or services given through the Site. The site and the services provided by Fuda are provided “as is” with no warranty. Fuda expressly disclaims any warranty, regarding the site and all services, including any implied warranty of merchant fitness for a particular purpose or for failure of performance. Fuda does not warrant that the services provided by Fuda or the site of the verification that services will be free from bias, defects, errors, eavesdropping or listening. Fuda shall not be held responsible for the quality of information or the authentication of the services or details given by psychics on the site. By using this site you accept the terms and conditions of this Disclaimer. You agree that any use you make of such answers, advice or services is at your own risk and that Fuda is not responsible for any damages or losses resulting from your reliance on such answers or advice. Fuda and its services are for entertainment purposes only. By submitting a question here, you understand and expressly agree that Fuda is not responsible for any reply or information that you do or do not receive, and you expressly agree to hold Fuda harmless from any loss, harm, injury, or damage whatsoever resulting from or arising out of your submission of the question or your use of or reliance on any response thereto.": 
  "Fuda không đảm bảo sự chính xác và những câu trả lời đáp ứng sự hài lòng của bạn, những nhà Cố vấn/ nhà Tâm linh cố gắng đưa cho bạn những chỉ dẫn, định hướng theo hình thức tâm linh tham vấn. Chúng tôi tiếp nhận khiếu nại thông qua ứng dụng và trang web của chúng tôi. Chúng tôi không trịu trách nhiệm cho những thông tin được đưa ra bởi Nhà tâm linh theo ý kiến chủ quan của họ. Bằng cách sử dụng trang web này, bạn chấp nhận các điều khoản và điều kiện của Tuyên bố từ chối trách nhiệm này. Hãy lưu ý rằng bất kỳ việc hành động nào bạn thực hiện được áp dụng từ các câu trả lời, lời khuyên hoặc dịch vụ bạn tham vấn có thể là rủi ro của riêng bạn và Fuda không chịu trách nhiệm về bất kỳ thiệt hại hoặc mất mát nào do bạn dựa vào các câu trả lời hoặc lời khuyên đó gây ra.",
  "All rates shown are USD $ per minute. Valid Visa/MasterCard/AMEX Credit Card or PayPal account is required. Pre-Paid, Gift and some Debit Cards are also accepted. If using the Fuda app, additional charges may incur due to iStore or Google PLAY charges. 3 (Three) Free minutes on all Chat/call readings apply only to the first three minutes of the first reading per New Unique Customer. Normal rates apply thereafter. By initiating a reading you confirm and accept the associated charges. Please make sure you understand the charges involved before starting a reading. Readings are charged in minute intervals.Additional mobile phone fees may apply if you use a mobile phone.": 
  "Để sử dụng được dịch vụ, bạn cần phải mua xu, các gói xu được quy ra tiền thật theo USD/ VNĐ. Các hình thức thẻ thanh toán được chấp nhận bao gồm: Visa/MasterCard/AMEX Credit Card or PayPal account, momo,... Sẽ có 100 coins miễn phí dành tặng cho bạn mới khi lần đầu đăng nhập vào ứng dụng của chúng tôi. Khoản tiền đó sẽ được sử dụng dịch vụ bình thường. Vui lòng đảm bảo rằng bạn hiểu các khoản phí liên quan trước khi bắt đầu đọc. Các bài đọc được tính theo từng phút và các gói dịch vụ cố định theo từng video.",

  // Page terms
  "Customer Terms and Conditions": "Điều khoản và điều kiện của khách hàng",
  "Introduction": "Giới thiệu",
  "Fuda is a platform operated by ICEO INC., on which Users can ask any questions to Advisors and receive recommendations from them.": 
  "Fuda là một nền tảng được điều hành bởi ICEO INC., nền tảng trung gian kết nối người dùng với các nhà tâm linh thông qua tương tác trực tiếp qua tin nhắn hoặc video.",
  "IF YOU ARE THINKING ABOUT SUICIDE OR IF YOU ARE CONSIDERING TAKING ACTIONS THAT MAY CAUSE HARM TO YOU OR TO OTHERS OR IF YOU FEEL THAT YOU OR ANY OTHER PERSON MAY BE IN ANY DANGER OR IF YOU HAVE ANY MEDICAL EMERGENCY, YOU MUST IMMEDIATELY CALL THE EMERGENCY SERVICE NUMBER AND NOTIFY THE RELEVANT AUTHORITIES.": 
  "TRONG BẤT CỨ TRƯỜNG HỢP TỰ Ý GÂY HẠI CHO BẢN THÂN HOẶC NGƯỜI KHÁC DỰA THEO SỰ CHỈ DẪN, BẠN HOÀN TOÀN TỰ CHỊU TRÁCH NHIỆM VỀ TẤT CẢ HÀNH VI CỦA BẢN THÂN VÀ ĐƯA RA HÀNH ĐỘNG XỬ LÝ PHÙ HỢP.",
  "Please read the Terms of Use carefully. These Terms apply to your access and use of the Fuda website and mobile applications (collectively referred to as “Service”) and any information, text, graphics, photographs, or other materials that are downloaded or appear in the Service. By accessing or using the Service, you agree to abide by all terms and conditions described in this Agreement. If you do not agree with all these terms and conditions, you may not use the Service. The terms “you” and “your” refer to an individual user of the Service.": 
  "Hãy đảm bảo rằng bạn đã đọc kỹ phần Điều khoản sử dụng. Điều khoản này được áp dụng khi người dùng sử dụng ứng dụng và trang web của Fuda (gọi tắt là “Dịch vụ”) và tất cả nội dung bên trong bao gồm: thông tin, chữ viết, ảnh, và các tài nguyên khác được tải xuống và xuất hiện trên dịch vụ. Bằng cách truy cập hoặc sử dụng Dịch vụ, bạn đồng ý tuân theo tất cả các điều khoản và điều kiện được mô tả trong Thỏa thuận này. Nếu bạn không đồng ý với tất cả các điều khoản và điều kiện này, bạn có thể không sử dụng Dịch vụ.",
  "Acceptable Use": "Chấp thuận sử dụng",
  "By agreeing to the Terms of Use, you acknowledge and warrant the following:": "Bằng cách đồng ý với Điều khoản sử dụng, bạn xác nhận và đảm bảo những điều sau:",
  "You are at least 18 years old.": "Bạn từ 18 tuổi trở lên.",
  "The information provided during registration is true, accurate and complete, and you agree to keep it up to date for the duration of the Agreement.": 
  "Thông tin được cung cấp trong quá trình đăng ký là trung thực, chính xác và đầy đủ và chấp nhận cập nhật thông tin trong suốt thời gian của Thỏa thuận.",
  "You are ready to provide the necessary minimum information for the provision of the Services.": 
  "Bạn đã sẵn sàng cung cấp thông tin tối thiểu cần thiết cho việc cung cấp Dịch vụ.",
  "You agree not to use the Service for illegal purposes.": "Chấp nhận không sử dụng Dịch vụ cho các mục đích bất hợp pháp.",
  "You undertake not to obstruct the operation of the Service using malicious software or in any other way that could damage the Service.": 
  "Cam kết không cản trở hoạt động của Dịch vụ bằng phần mềm độc hại hoặc theo bất kỳ cách nào khác có thể làm ảnh hưởng tới Dịch vụ.",
  "You agree not to try obtaining information about other Users of the Service.": "Không cố lấy thông tin về những Người dùng Dịch vụ khác.",
  "While exchanging files within the Service, you agree to independently check them for viruses.": "Trong khi trao đổi tệp trong Dịch vụ, bạn đồng ý kiểm tra vi-rút một cách độc lập.",
  "Fuda has the right to stop the exchange of any information between the participants of the Service and/or at any time remove any content uploaded by the User.": 
  "Fuda có quyền ngừng trao đổi bất kỳ thông tin nào giữa những người tham gia Dịch vụ và / hoặc xóa bất kỳ nội dung, tài khoản nào do Người dùng tải lên, bất kỳ lúc nào.",
  "User rights": "Quyền hạn của người dùng",
  "The user has the right to forbid Fuda processing his personal information for marketing purposes.": "Người dùng có quyền hạn chế Fuda tiếp cận thông tin cá nhân của mình cho các mục đích tiếp thị.",
  "The user can request an update of his personal information, and also has the right to request all information about him stored on the servers of Fuda to be deleted. In order to do this, he needs to contact Fuda via email:": 
  "Người dùng có thể yêu cầu cập nhật thông tin cá nhân của mình và cũng có quyền yêu cầu xóa tất cả thông tin về mình được lưu trữ trên máy chủ của Fuda. Hãy liên hệ email chúng tôi để được hỗ trợ trực tiếp nhất:",
  "Fees and Payments": "Phí và thanh toán",
  "All interactions between Users and Advisors are paid through online channels. You agree to abide by the agreed pricing terms and pay all Fuda fees for the Services, provided by Advisors.": 
  "Tất cả các tương tác giữa Người dùng và Nhà Tâm Linh đều được áp dụng các hình thức thanh toán nội địa và quốc tế như nhau. Bạn đồng ý tuân theo các điều khoản định giá đã thỏa thuận và thanh toán tất cả các khoản phí của Fuda cho Dịch vụ, và Nhà Tâm Linh/ nhà cố vấn cung cấp.",
  "You directly authorize the payment of all commissions for each transaction that occurs under your account.": 
  "Bạn trực tiếp cho phép thanh toán tất cả các khoản cho mỗi giao dịch xảy ra trong tài khoản của bạn.",
  "Any credit purchased by a user that hasn't been used will be nullified after six months.": "Bất kỳ khoản tín dụng nào được mua bởi người dùng chưa được sử dụng sẽ bị vô hiệu hóa sau sáu tháng.",
  "Fuda has the right to refuse to provide the service to the User, returning the paid amount in any convenient way.": 
  "Fuda có quyền từ chối cung cấp dịch vụ cho Người sử dụng, trả lại số tiền đã thanh toán bằng mọi hình thức thuận tiện. Fuda có quyền từ chối cung cấp dịch vụ cho Người sử dụng, hoàn trả số tiền đã thanh toán bằng mọi hình thức thuận tiện.",
  "Privacy": "Quyền riêng tư",
  "For information about how we protect your privacy, please refer to our Privacy Policy which is not part of the Customer Terms and Conditions": 
  "Để biết thông tin về cách chúng tôi bảo vệ quyền riêng tư của bạn, vui lòng tham khảo Chính sách quyền riêng tư của chúng tôi, không nằm trong Điều khoản và Điều kiện của Khách hàng",
  "Fuda Privacy Policy": "Chính sách quyền riêng tư của Fuda",
  "Fuda Consulting Limited develops cutting-edge digital solutions that enable you to instantly connect with life coaches and advisors from around the world.": 
  "Fuda phát triển các giải pháp kỹ thuật số tiên tiến cho phép bạn kết nối tức thì với các cố vấn và huấn luyện viên cuộc sống từ khắp nơi trên thế giới.",
  "Spiritual matters are not an exact science and experiences may vary.": 
  "Các vấn đề tâm linh không phải là một môn khoa học chính xác và các trải nghiệm có thể khác nhau.",
  "Every advisor on our platform is a unique individual with different methods, tools, and style. We encourage customers to take time exploring our network, reading profiles and reviews, and finding someone who truly appeals to you. To improve your experience, please": 
  "Mỗi cố vấn trên nền tảng của chúng tôi là một cá nhân độc lập với các phương pháp, công cụ và phong cách khác nhau. Chúng tôi khuyến khích người dùng dành thời gian tham khảo hồ sơ Nhà Tâm Linh, đọc hồ sơ và đánh giá, đồng thời tìm người thực sự có sự liên kết bạn. Để cải thiện trải nghiệm của người dùng, vui lòng",
  "take a look at our tips for getting the best online reading possible.": "xem các mẹo của chúng tôi để có trải nghiệm tốt nhất có thể.",
  "On our platform, advisors have the right to choose their own pricing for the services they offer. By initiating a reading, you are agreeing to pay the listed price for the service you request.": 
  "Chúng tôi luôn cố gắng thúc đẩy một môi trường an toàn và tôn trọng cho tất cả người dùng. Mọi cố vấn đều trải qua một quá trình đánh giá và chúng tôi theo dõi tất cả các khiếu nại của khách hàng phát sinh. Phản hồi của bạn là một phần quan trọng trong quá trình cải tiến của chúng tôi.",
  "If the service is not delivered due to technical reasons, we will issue a credit to your Fuda account for the full amount. If the service was delivered but you feel the quality warrants a refund, please get in touch with": 
  "Nếu dịch vụ không được cung cấp vì lý do kỹ thuật, chúng tôi sẽ cấp toàn bộ số tiền cho tài khoản Fuda của bạn. Nếu dịch vụ đã được giao nhưng bạn cảm thấy chất lượng cần được hoàn lại tiền, vui lòng liên hệ với",
  "All refund requests are investigated by our Customer Support team on a case-by-case basis. Refunds will only be issued as credits on your Fuda account. Transaction fees may be deducted. If you encounter an error during the refund process, please reach out to us at": 
  "Tất cả các yêu cầu hoàn tiền đều được nhóm Hỗ trợ khách hàng của chúng tôi kiểm tra theo từng trường hợp cụ thể. Tiền hoàn lại sẽ chỉ được phát hành dưới dạng tín dụng trên tài khoản Fuda của bạn. Phí giao dịch có thể được khấu trừ. Nếu bạn gặp lỗi trong quá trình hoàn lại tiền, vui lòng liên hệ với chúng tôi theo địa chỉ",
  "Modification and Termination of Services": "Thay đổi và chấm dứt dịch vụ",
  "Fuda is allowed to make any temporary and/or permanent changes to the work of the Service with or without notice to its Members and is not liable to Users, Advisors or any other third party.": 
  "Fuda được phép thực hiện bất kỳ thay đổi tạm thời và / hoặc vĩnh viễn nào đối với công việc của Dịch vụ có hoặc không có thông báo cho các Thành viên và không chịu trách nhiệm với Người dùng, Cố vấn hoặc bất kỳ bên thứ ba nào khác.",
  "Fuda has the right to stop providing Services to the User at any time without giving reasons.": "Fuda có quyền ngừng cung cấp Dịch vụ cho Người dùng bất cứ lúc nào mà không cần nêu lý do.",
  "Third-party websites": "Chính sách bên thứ ba",
  "If necessary, Fuda can use third-party resources, however does not have the ability to control their work.": 
  "Dịch vụ có thể chứa các liên kết đến tài nguyên của bên thứ ba, quảng cáo, ưu đãi đặc biệt hoặc bất kỳ sự kiện/hành động nào khác không thuộc sở hữu hoặc kiểm soát của Fuda.",
  "The service may contain links to third-party resources, advertising, special offers or any other events / actions that are not owned or controlled by Fuda. Fuda is not responsible for the information contained on third-party resources and the services they offer. Moving to a third-party resource and/or deciding to use the Services of a third-party resource, the User assumes all responsibility for possible risks and clearly releases Fuda from any liability associated with the use of third-party resources and any services they provide. In addition, the User releases Fuda from responsibility for participation in promotions or any other events organized by third-party services, as well as for payment and delivery of any goods. Using third-party services, the User agrees that Fuda is not responsible for any loss or damage of any kind related to his interaction with third-party resources.": 
  "Fuda không chịu trách nhiệm về thông tin có trên các tài nguyên của bên thứ ba và các dịch vụ mà họ cung cấp. Chuyển sang tài nguyên của bên thứ ba và / hoặc quyết định sử dụng Dịch vụ của tài nguyên bên thứ ba, Người dùng chịu mọi trách nhiệm về những rủi ro có thể xảy ra, Fuda không chịu trách nhiệm trách nhiệm liên quan đến việc sử dụng tài nguyên của bên thứ ba và bất kỳ dịch vụ nào họ cung cấp. Ngoài ra, Fuda không chịu trách nhiệm về việc người dùng tham gia các chương trình khuyến mãi hoặc bất kỳ sự kiện nào khác do các dịch vụ của bên thứ ba tổ chức, cũng như thanh toán và giao hàng hóa. Sử dụng dịch vụ của bên thứ ba, Người dùng đồng ý rằng Fuda không chịu trách nhiệm về bất kỳ tổn thất hoặc thiệt hại nào dưới bất kỳ hình thức nào liên quan đến các tài nguyên của bên thứ ba.",
  "Fuda may provide the User with notifications or other messages about changes in this Agreement and/or changes in any aspect of the Service, by email, regular mail, or by posting information on the Fuda website. The date of receipt shall be the date of receiving such notification.": 
  "Fuda có thể cung cấp cho Người dùng các thông báo hoặc tin nhắn khác về các thay đổi trong Thỏa thuận và / hoặc các thay đổi trong bất kỳ khía cạnh nào của Dịch vụ, qua email, hoặc bằng cách đăng thông tin trên trang web/ mạng xã hội/ kênh thông tin chính thức của Fuda.",
  "The User may not re-assign his rights or obligations in accordance with this Agreement without the prior written consent of Fuda.": 
  "Người dùng không được chuyển nhượng lại các quyền hoặc nghĩa vụ của mình theo Thỏa thuận này mà không có sự đồng ý trước bằng văn bản của Fuda.",
  "This Agreement constitutes the entire agreement between Fuda and the User.": "Thỏa thuận này cấu thành toàn bộ thỏa thuận giữa Fuda và Người dùng.",
  "No amendments to this Agreement will enter into force unless they are made in writing. If any provision of this Agreement is recognized by a court of competent jurisdiction as illegal, invalid, unenforceable or otherwise contrary to law, the remaining provisions of this Agreement will remain in full force and effect.": 
  "Không có sửa đổi nào đối với Thỏa thuận này sẽ có hiệu lực trừ khi chúng được thực hiện bằng văn bản.",
  "All services provided and all rights to them are the property of Fuda.": "Tất cả các dịch vụ được cung cấp và tất cả các quyền đối với chúng là tài sản của Fuda.",
  "By using the Fuda service you agree to the terms and conditions of this Agreement.": "Bằng cách sử dụng dịch vụ của Fuda, bạn đồng ý với các điều khoản và điều kiện của Điều khoản.",
  "All promos/coupons have a 7-day expiration since issuance date unless explicitly stated.": "Tất cả các khuyến mãi / chương trình ưu đãi được áp dụng trong thời hạn quy định của từng chương trình đặc biệt.",
  "Any concerns relating to the Site or this Agreement, should be notified to Fuda at": "Tất cả mọi thắc mắc, quan tâm tới Điều Khoản và Dịch vụ của chúng tôi, hãy liên hệ với chúng tôi qua email:",

  // Page Question
  "Ask Any Questions": "Câu hỏi",
  "Instead of worrying about yourself and your loved ones, consult an expert who can advise you on what to do next.":
    "Thay vì lo lắng cho bản thân và những người thân yêu của bạn, hãy tham khảo ý kiến của một chuyên gia, người có thể tư vấn cho bạn về những gì cần làm tiếp theo.",
  "Horoscopes & Astrology": "Tử vi & Chiêm tinh",
  "Would you like to find true love? Need a career change? Fuda' Astrologers will help you discover your path to love and good fortune":
    "Bạn có muốn thấy tình yêu đích thực? Cần thay đổi nghề nghiệp? Các nhà chiêm tinh của Fuda sẽ giúp bạn khám phá con đường dẫn đến tình yêu và vận may",
  "Clairvoyance" : "Thấu thị",
    "Stop feeling confused and lost - start a one-on-one session with our Clairvoyant Mediums now and get spiritual direction and answers": "Ngừng cảm thấy bối rối và mất mát - bắt đầu cuộc nói chuyện một - một với Thấu thị của chúng tôi ngay bây giờ và nhận được hướng dẫn và câu trả lời về tâm linh",
  "Love & Relationships": "Tình yêu & Mối quan hệ",
  "Receive Psychic Advice on Love and Relationships. Find out if Your Partner is Cheating? Are You Soul Mates? Learn How to Find a True Love": "Nhận lời khuyên của nhà ngoại cảm về tình yêu và các mối quan hệ. Tìm hiểu xem đối phương có đang giấu diếm điều gì không? Có phải là bạn tâm giao? Tìm hiểu cách tìm một tình yêu đích thực",
  "Psychic Reading": "Đọc ngoại cảm",
  "Get a new perspective and clear insights about events happening in your life. Our Psychic Readings help people every day. Sign up with Fuda now!": "Có cái nhìn mới và hiểu biết rõ ràng về các sự kiện xảy ra trong cuộc sống của bạn. Các bài đọc về ngoại cảm của chúng tôi giúp ích cho mọi người mỗi ngày. Đăng ký với Fuda ngay bây giờ!",
  "Tarot Readers": "Người xem tarot",
  "Your FREE Tarot Reading NOW - first 3 minutes free. If it's Latin, love, New Age or anything else - find what the future has in store for you": "Xen Tarot MIỄN PHÍ NGAY BÂY GIỜ - miễn phí 3 phút đầu tiên. Nếu đó là tiếng Latinh, tình yêu, thời đại mới hoặc bất cứ thứ gì khác - hãy tìm những gì tương lai có sẵn cho bạn",
  "Life Coaching": "Lời khuyên cuộc sống",
  "Find a life coach Advisor match for you!": "Tìm một chuyên gia cố vấn cuộc sống phù hợp với bạn!",
  "Try it now": "Thử ngay",
  "Popular questions": "Các câu hỏi phổ biến",
  "How do I register and start a reading on Fuda": "Làm cách nào để đăng ký và bắt đầu một dịch vụ trên Fuda",
  "To get started, press the Sign Up button on the top right corner of this website. After you have registered, simply Sign In and choose your favorite psychic, then click on their Chat or Call button.": "Để bắt đầu, hãy nhấn nút Đăng ký ở góc trên cùng bên phải của trang web. Sau khi bạn đã đăng ký, chỉ cần Đăng nhập và chọn nhà ngoại cảm yêu thích của bạn, sau đó nhấp vào nút Chat hoặc Gọi họ.",
  "How do I login to my account?": "Làm sao tôi có để đăng nhập vào tài khoản của tôi?",
  "If you have already registered, just click Sign In, located on the top right of this website.": "Nếu bạn đã đăng ký, chỉ cần nhấp vào Đăng nhập, nằm ở trên cùng bên phải của trang web.",
  "Choosing a username and password": "Chọn tên đăng nhập và mật khẩu",
  "When you register on Fuda, you'll need to choose a username and password. You'll need these to login each time you come back for more psychic readings. We recommend that you choose a username and password that are unique, yet easy to remember. In order to keep your privacy, we suggest you pick something different from your real first and last name.": 
  "Khi đăng ký trên Fuda, bạn cần chọn tên đăng nhập và mật khẩu. Bạn sẽ tên đăng nhập và mật khẩu để đăng nhập mỗi khi quay lại để có thêm các bài đọc về tâm linh. Chúng tôi khuyên bạn nên chọn tên đăng nhập và mật khẩu duy nhất nhưng dễ nhớ. Để giữ sự riêng tư của bạn, chúng tôi khuyên bạn nên chọn một cái gì đó khác với họ và tên thật của bạn.",
  
  //   Page AppStore
  "Our experts are always in touch": "Các chuyên gia của chúng tôi luôn phản hồi",
  "Enjoy psychic reading on your mobile. Our Psychic Readers are compassionate and will amaze you with a personal live reading unlike any you've had before.":
  "Tận hưởng trải nghiệm tâm linh trên điện thoại di động của bạn. Các nhà ngoại cảm của chúng tôi có lòng trắc ẩn và sẽ khiến bạn ngạc nhiên với cách đọc trực tiếp cá nhân không giống như bất kỳ bài đọc nào bạn đã từng có trước đây.",

  // Page Footer
  "ABOUT FUDA": "VỀ Fuda",
  "About Us": "Về chúng tôi",
  "Your safety & security matters": "An toàn & bảo mật của bạn",
  "Customer Support": "Hỗ trợ khác hàng",
  "Refund policy": "Chính sách hoàn tiền",
  "HOW IT WORKS": "CÁCH THỨC HOẠT ĐỘNG",
  "Getting Started": "Bắt đầu",
  "How to find an advisor": "Tìm một chuyên gia",
  "Advisor Terms and Conditions": "Điều khoản và Điều kiện của chuyên gia",
  "Articles": "Bài viết",
  "HOW WE CAN HELP": "CHÚNG TÔI CÓ THỂ GIÚP GÌ",
  "Find a Psychic": "Tìm một nhà ngoại cảm",
  "Become an Advisor": "Trở thành chuyên gia",
  "Contact Us": "Liên hệ",
  "Remove User": "Xoá tài khoản",
  "© 2022 fuda.appuni.io is operated by ICEO inc.": "© 2022 fuda.appuni.io được điều hành bởi Công ty cổ phần công nghệ ICEO.",
  "* All prices are listed in coin credits. One coin credit is equivalent to $0.1 USD. Psychics are not employees or representatives of Fuda.": 
  "* Tất cả giá được liệt kê bằng coin credits. 1 coin credit tương đương với 80vnđ. Các chuyên gia không phải là nhân viên hoặc đại diện của Fuda.",
  "Use of this site is subject to the": "Việc sử dụng trang web này phải tuân theo",
  "WE ACCEPT": "CHÚNG TÔI CHẤP NHẬN",

  // Page Info
  "Fuda connects customers with genuine advice from some of the world's best psychics. Through our safe and simple online platform, discover the perfect psychic reading to help in love, work, life and beyond. Our network of psychics, clairvoyants and other spiritual experts is carefully selected for their proven abilities in a diverse range of divination methods, including astrology, numerology, channeling, dream analysis, automatic writing, card reading, and so much more. Some advisors specialize in certain subjects, like past lives, soulmates, love and relationships, career, and family. Browse the listings, check out advisor profiles, and read user reviews to find the ideal psychic who can offer the right kind of reading. After signing up and selecting an advisor, a psychic reading is as easy as choosing 'call' or 'chat'. New customers begin with 3 Free minutes and 60% off their first reading. All psychic readings are private and confidential. Expect insightful advice in a kind, honest and non-judgmental environment. After every reading, we encourage you to leave feedback about your experience. If any problems arise, customer support is available online 24/7. There's more to Fuda than readings. Learn about mystical and spiritual topics, as well as subjects like self-care and healing, in our blog. Gain insight into what lies ahead in your day, week and month from our free horoscopes." : 
  "Fuda là nền tảng trung gian kết nối người dùng với các nhà tâm linh thông qua tương tác trực tiếp qua tin nhắn hoặc video. Mạng lưới các nhà tâm linh hội tụ tại Fuda của chúng tôi là người thật, được đánh giá kỹ về khả năng, những cố vấn chuyên nghiệp với chứng chỉ quốc tế trong các loại hình tâm linh khác nhau, bao gồm chiêm tinh, thần số học, xem chỉ tay, giải mã giấc mơ, tử vi, ... Chỉ vài bước lựa chọn cố vấn yêu thích, xem xét hồ sơ và nhận xét về họ, người dùng có thể dễ dàng kết nối với nhà tâm linh thông qua “trò chuyện trực tiếp” hoặc “video” tùy theo từng mong muốn. Đừng ngần ngại liên hệ với bộ phận hỗ trợ của chúng tôi nếu có bất cứ thắc mắc nào.",

  // Page Policies
  "Fuda Policies" :"Chính sách Fuda",
  "MOBILE APP PRIVACY POLICY" :"CHÍNH SÁCH BẢO MẬT CỦA ỨNG DỤNG DI ĐỘNG",
  "Last updated [June 16th, 2022]" :"Cập nhật lần cuối [16 tháng 6, 2021]",
  "We respect the privacy of our users. This Privacy Policy explains how we collect, use, disclose, and safeguard your information after you visit our mobile application (the “Fuda”). Please read this Privacy Policy carefully. IF you are doing NOT believe THE TERMS OF THIS PRIVACY POLICY, PLEASE don't ACCESS the appliance." :
  "Chúng tôi tôn trọng quyền riêng tư của người dùng. Chính sách Bảo mật này giải thích cách chúng tôi thu thập, sử dụng, tiết lộ và bảo vệ thông tin của bạn sau khi bạn truy cập ứng dụng di động của chúng tôi (“Fuda”). Vui lòng đọc kĩ chính sách bảo mật này. NẾU bạn KHÔNG tin vào CÁC ĐIỀU KHOẢN CỦA CHÍNH SÁCH BẢO MẬT NÀY, VUI LÒNG KHÔNG TRUY CẬP thiết bị.",
  "We reserve the correct to create changes to the present Privacy Policy at any time and for any reason. we'll warn you about any changes by updating the “Last Updated” date of this Privacy Policy. you're encouraged to periodically review this Privacy Policy to remain informed of updates. you'll be deemed to possess been made tuned in to, are going to be subject to, and can be deemed to possess accepted the changes in any revised Privacy Policy by your continued use of the applying after the date such revised Privacy Policy is posted." : 
  "Chúng tôi có quyền tạo ra các thay đổi đối với Chính sách Bảo mật hiện tại bất kỳ lúc nào và vì bất kỳ lý do gì. Chúng tôi sẽ cảnh báo bạn về bất kỳ thay đổi nào bằng cách cập nhật ngày “Cập nhật lần cuối” của Chính sách quyền riêng tư này. Chúng tôi khuyến khích bạn xem xét định kỳ Chính sách quyền riêng tư này để được cập nhật thông tin. bạn sẽ được coi là sở hữu đã được điều chỉnh, sẽ tuân theo và có thể được coi là sở hữu những thay đổi được chấp nhận trong bất kỳ Chính sách quyền riêng tư sửa đổi nào bằng cách bạn tiếp tục sử dụng ứng dụng sau ngày Chính sách quyền riêng tư sửa đổi đó được đăng.",
  "This Privacy Policy doesn't apply to the third-party online/mobile store from which you put in the appliance or make payments, including any in-game virtual items, which can also collect and use data about you. We don't seem to be accountable for any of the info collected by any such third party." :
  "Chính sách Bảo mật này không áp dụng cho cửa hàng trực tuyến / di động của bên thứ ba mà ứng dụng được đặt tại hoặc thanh toán, bao gồm bất kỳ vật phẩm ảo nào trong trò chơi, cũng có thể thu thập và sử dụng dữ liệu về bạn. Chúng tôi dường như không chịu trách nhiệm về bất kỳ thông tin nào được thu thập bởi bất kỳ bên thứ ba nào như vậy.",
  "COLLECTION OF YOUR INFORMATION": "THU THẬP THÔNG TIN CỦA BẠN",
  "We may collect information about you in a variety of ways. The information we may collect through the Application depends on the content and materials you use and includes:":
  "Chúng tôi có thể thu thập thông tin về bạn theo nhiều cách khác nhau. Thông tin chúng tôi có thể thu thập thông qua Ứng dụng phụ thuộc vào nội dung và tài liệu bạn sử dụng và bao gồm:",
  "Personal Data": "Thông tin cá nhân",
  "Demographic and other personally identifiable information (such as your name and email address) that you voluntarily give to us when choosing to participate in various activities related to the Application, such as chat, posting messages in comment sections or our forums, liking posts, sending feedback, and responding to surveys. If you choose to share data about yourself via your profile, online chat, or other interactive areas of the Application, please be advised that all data you disclose in these areas is public and your data will be accessible to anyone who accesses the Application.":
  "Thông tin nhân khẩu học và các thông tin nhận dạng cá nhân khác (chẳng hạn như tên và địa chỉ email của bạn) mà bạn tự nguyện cung cấp cho chúng tôi khi chọn tham gia vào các hoạt động khác nhau liên quan đến Ứng dụng, chẳng hạn như trò chuyện, đăng tin nhắn trong phần bình luận hoặc diễn đàn của chúng tôi, thích bài đăng, gửi phản hồi và trả lời các cuộc khảo sát. Nếu bạn chọn chia sẻ dữ liệu về bản thân qua hồ sơ, trò chuyện trực tuyến hoặc các khu vực tương tác khác của Ứng dụng, xin lưu ý rằng tất cả dữ liệu bạn tiết lộ trong các khu vực này đều công khai và dữ liệu của bạn sẽ có thể truy cập được cho bất kỳ ai truy cập Ứng dụng.",
  "Derivative Data": "Dữ liệu phát sinh",
  "The information our servers automatically collect when you access the Application, such as your native actions that are integral to the Fudalyworld, including liking, re-blogging, or replying to a post, as well as other interactions with the Application and other users via server log files.":
  "Thông tin mà máy chủ của chúng tôi tự động thu thập khi bạn truy cập Ứng dụng, chẳng hạn như các hành động gốc của bạn không thể tách rời với Fudalyworld, bao gồm thích, viết blog hoặc trả lời một bài đăng, cũng như các tương tác khác với Ứng dụng và những người dùng khác thông qua máy chủ các tệp nhật ký.",
  "Financial Data": "Dữ liệu tài chính",
  "Financial information, like data associated with your payment method (e.g. valid mastercard number, card brand, expiration date) that we may collect once you purchase, order, return, exchange, or request information about our services from the Fuda. [We store only very limited, if any, financial information that we collect. Otherwise, all financial information is stored by our payment processor," : 
  "Thông tin tài chính như dữ liệu liên quan đến phương thức thanh toán của bạn (ví dụ: số MasterCard hợp lệ, thương hiệu thẻ và ngày hết hạn) mà chúng tôi có thể thu thập khi bạn mua, đặt hàng, trả lại, trao đổi hoặc yêu cầu thông tin về dịch vụ của chúng tôi từ Fuda. [Chúng tôi chỉ lưu trữ rất hạn chế, nếu có, thông tin tài chính mà chúng tôi thu thập được. Nếu không, tất cả thông tin tài chính được lưu trữ bởi bộ xử lý thanh toán của chúng tôi,",
  ", and you're encouraged to review their privacy policy and speak to them directly for responses to your questions." : 
  ", và bạn được khuyến khích xem lại chính sách quyền riêng tư của họ và trao đổi trực tiếp với họ để có câu trả lời cho các câu hỏi của bạn.",
  "Facebook Permissions" : "Quyền Facebook",
  "The Application may by default access your Facebook basic account information, including your name, email, gender, birthday, current city, and profile picture URL, moreover as other information that you simply like better to make public. We can also request access to other permissions associated with your accounts, like friends, check-ins, and likes, and you will favor to grant or deny us access to every permission. For more information regarding Facebook permissions, see the Facebook Permissions Reference page." :
  "Theo mặc định đăng nhập, Ứng dụng có thể truy cập thông tin tài khoản Facebook cơ bản của bạn, bao gồm tên, email, giới tính, ngày sinh, thành phố hiện tại và URL ảnh hồ sơ của bạn, ngoài ra còn có thông tin mà bạn công khai. Chúng tôi cũng có thể yêu cầu quyền truy cập vào các quyền khác được liên kết với tài khoản của bạn, như bạn bè, đăng ký và lượt thích, và bạn sẽ ủng hộ việc cấp hoặc từ chối cho chúng tôi quyền truy cập vào mọi quyền. Để biết thêm thông tin liên quan đến quyền của Facebook, hãy xem trang Tham khảo quyền của Facebook.",
  "Data from Social Networks": "Dữ liệu từ mạng xã hội",
  "User information from social networking sites, like Facebook, Google+ Instagram, Pinterest, or Twitter, including your name, your social network username, location, gender, birth date, email address, profile picture, and public data for contacts, if you connect your account to such social networks. This information might also include the contact information of anyone you invite to use and/or join the Fuda." :
  "Thông tin người dùng từ các trang mạng xã hội, như Facebook, Google+ Instagram, Pinterest hoặc Twitter, bao gồm tên của bạn, tên người dùng mạng xã hội, vị trí, giới tính, ngày sinh, địa chỉ email, ảnh hồ sơ và dữ liệu công khai cho các liên hệ, nếu bạn kết nối tài khoản cho các mạng xã hội đó. Thông tin này cũng có thể bao gồm thông tin liên hệ của bất kỳ ai mà bạn mời sử dụng và / hoặc tham gia Fuda.",
  "Geo-Location Information": "Thông tin định vị - địa lý",
  "We may request access or permission to track location-based information from your mobile device, either continuously or while you are using the Application, to provide location-based services. If you wish to change our access or permissions, you may do so in your device's settings.": 
  "Chúng tôi có thể yêu cầu quyền truy cập hoặc quyền theo dõi thông tin dựa trên vị trí từ thiết bị di động của bạn, liên tục hoặc trong khi bạn đang sử dụng Ứng dụng, để cung cấp các dịch vụ dựa trên vị trí. Nếu bạn muốn thay đổi quyền truy cập hoặc quyền của chúng tôi, bạn có thể thay đổi trong cài đặt thiết bị của mình.",
  "Mobile Device Access": "Truy cập thiết bị di động",
  "We may request access or permission to certain features from your mobile device, including your mobile device's [bluetooth, calendar, camera, contacts, microphone, reminders, sensors, SMS messages, social media accounts, storage,] and other features. If you would like to change our access or permissions, you can do so in your device's settings." :
  "Chúng tôi có thể yêu cầu quyền truy cập hoặc quyền đối với một số tính năng nhất định từ thiết bị di động của bạn, bao gồm [bluetooth, lịch, máy ảnh, danh bạ, micrô, lời nhắc, cảm biến, tin nhắn SMS, tài khoản mạng xã hội, bộ nhớ] và các tính năng khác của thiết bị di động. Nếu bạn muốn thay đổi quyền truy cập hoặc quyền của chúng tôi, bạn có thể thay đổi trong cài đặt thiết bị của mình.",
  "Mobile Device Data": "Dữ liệu thiết bị di động",
  "Device information such as your mobile device ID number, model, and manufacturer, version of your operating system, phone number, country, location, and any other data you choose to provide.":
  "Thông tin thiết bị như số ID thiết bị di động, kiểu máy và nhà sản xuất, phiên bản hệ điều hành, số điện thoại, quốc gia, vị trí và bất kỳ dữ liệu nào khác mà bạn chọn cung cấp.",
  "Push Notifications": "Cho phép thông báo",
  "We may request to send you to push notifications regarding your account or the Application. If you wish to opt-out of receiving these types of communications, you may turn them off in your device's settings.":
  "Chúng tôi có thể yêu cầu gửi thông báo liên quan đến tài khoản của bạn hoặc Ứng dụng. Nếu bạn muốn chọn không nhận các loại liên lạc này, bạn có thể tắt chúng trong cài đặt của thiết bị.",
  "Third-Party Data" : "Dữ liệu từ bên thứ ba",
  "Information from third parties, such as personal information or network friends, if you connect your account to the third party and grant the Fuda permission to access this information.":
  "Thông tin từ bên thứ ba, chẳng hạn như thông tin cá nhân hoặc bạn bè trong mạng, nếu bạn kết nối tài khoản của mình với bên thứ ba và cấp cho Fuda quyền truy cập thông tin này.",
  "Data From Contests, Giveaways, and Surveys": "Dữ liệu từ cuộc thi, trò chơi, và khảo sát",
  "Personal and other information may be provided when entering contests or giveaways and/or responding to surveys.": 
  "Thông tin cá nhân và thông tin khác có thể được cung cấp khi tham gia các cuộc thi hoặc chương trình tặng quà và/hoặc trả lời các cuộc khảo sát.",
  "USE OF YOUR INFORMATION": "SỬ DỤNG THÔNG TIN CÁ NHÂN",
  "Having accurate information about you permits us to provide you with a smooth, efficient, and customized experience. Specifically, we may use information collected about you through the Fuda to:" :
  "Có thông tin chính xác về bạn cho phép chúng tôi cung cấp cho bạn trải nghiệm mượt mà, hiệu quả và tùy chỉnh. Cụ thể, chúng tôi có thể sử dụng thông tin thu thập được về bạn thông qua Fuda để:",
  "1. Administer sweepstakes, promotions, and contests.": "1. Quản lý khuyến mại và các cuộc thi.",
  "2. Assist enforcement and reply to subpoenas.": "2. Hỗ trợ việc thực thi pháp lý.",
  "3. Compile anonymous statistical data and analysis to be used internally or with third parties.": "3. Biên dịch dữ liệu thống kê ẩn danh và phân tích để sử dụng trong nội bộ hoặc với các bên thứ ba.",
  "4. Create and manage your account.": "4. Tạo và quản lý tài khoản của bạn.",
  "5. Deliver targeted advertising, coupons, newsletters, and other information regarding promotions and therefore the Fuda to you.":
  "5. Cung cấp quảng cáo được nhắm mục tiêu, phiếu giảm giá, bản tin và các thông tin khác liên quan đến các chương trình khuyến mãi và do Fuda dành cho bạn.",
  "6. Email you regarding your account or order." : "6. Gửi email cho bạn về tài khoản hoặc đơn đặt hàng của bạn.",
  "7. Enable user-to-user communications." : "7. Kích hoạt giao tiếp giữa người dùng với người dùng.",
  "8. Fulfill and manage purchases, orders, payments, and other transactions associated with the Fuda." : "8. Thực hiện và quản lý các giao dịch mua, đơn đặt hàng, thanh toán và các giao dịch khác liên quan đến Fuda.",
  "9. Generate a private profile about you to form future visits to the Fuda more personalized." : 
  "9. Tạo hồ sơ cá nhân về bạn để tạo sư cá nhân hóa trải nghiệm trong các lần tiếp theo.",
  "10. Increase the efficiency and operation of the Fuda." : "10. Tăng hiệu quả và hoạt động của Fuda.",
  "11. Monitor and analyze usage and trends to boost your experience with the Fuda." : "11. Theo dõi và phân tích việc sử dụng cũng như xu hướng để nâng cao trải nghiệm của bạn với Fuda.",
  "12. Notify you of updates to the appliance." : "12. Thông báo cho bạn về các bản cập nhật cho thiết bị.",
  "13. Offer new products, services, mobile applications, and/or recommendations to you." : "13. Cung cấp các sản phẩm, dịch vụ, ứng dụng di động và / hoặc các đề xuất mới cho bạn.",
  "14. Perform other business activities as required." : "14. Thực hiện các hoạt động kinh doanh khác theo yêu cầu.",
  "15. Prevent fraudulent transactions, monitor against theft, and protect against criminal activity." : "15. Ngăn chặn các giao dịch gian lận, giám sát chống trộm cắp và bảo vệ chống lại hoạt động tội phạm.",
  "16. Process payments and refunds." : "16. Xử lý các khoản thanh toán và hoàn lại tiền.",
  "17. Request feedback and contact you about your use of this application." : "17. Yêu cầu phản hồi và liên hệ với bạn về việc bạn sử dụng ứng dụng này.",
  "18. Resolve disputes and troubleshoot problems." : "18. Giải quyết tranh chấp và khắc phục sự cố.",
  "19. Respond to product and customer service requests." : "19. Đáp ứng các yêu cầu về sản phẩm và dịch vụ của khách hàng.",
  "20. Send you a newsletter." : "20. Gửi bản tin cho bạn.",
  "21. Solicit support for the application." : "21. Xin hỗ trợ cho ứng dụng.",
  "DISCLOSURE OF YOUR INFORMATION" : "TIẾT LỘ THÔNG TIN CÁ NHÂN",
  "We may share information we have collected about you in certain situations. Your information may be disclosed as follows:" : "Chúng tôi có thể chia sẻ thông tin chúng tôi đã thu thập được về bạn trong một số tình huống nhất định. Thông tin của bạn có thể được tiết lộ như sau:",
  "By Law or to Protect Rights" : "Theo luật hoặc để bảo vệ quyền",
  "If we believe the release of information about you is necessary to respond to legal process, to investigate or remedy potential violations of our policies, or to protect the rights, property, and safety of others, we may share your information as permitted or required by any applicable law, rule, or regulation. This includes exchanging information with other entities for fraud protection and credit risk reduction."
  : "Nếu chúng tôi cho rằng việc tiết lộ thông tin về bạn là cần thiết để đáp ứng quy trình pháp lý, để điều tra hoặc khắc phục các vi phạm tiềm ẩn đối với chính sách của chúng tôi hoặc để bảo vệ quyền, tài sản và sự an toàn của người khác, chúng tôi có thể chia sẻ thông tin của bạn khi được cho phép hoặc yêu cầu bởi bất kỳ luật, quy tắc hoặc quy định hiện hành nào. Điều này bao gồm việc trao đổi thông tin với các đơn vị khác để bảo vệ chống gian lận và giảm thiểu rủi ro tín dụng.",
  "Third-Party Service Providers" : "Bên cung cấp dịch vụ thứ ba",
  "We may share your information with third parties that perform services for us or on our behalf, including payment processing, data analysis, email delivery, hosting services, customer service, and marketing assistance." :
  "Chúng tôi có thể chia sẻ thông tin của bạn với các bên thứ ba thực hiện các dịch vụ cho chúng tôi hoặc thay mặt chúng tôi, bao gồm xử lý thanh toán, phân tích dữ liệu, gửi email, dịch vụ lưu trữ, dịch vụ khách hàng và hỗ trợ tiếp thị.",
  "Marketing Communications" : "Truyền thông tiếp thị",
  "With your consent, or with an opportunity for you to withdraw consent, we may share your information with third parties for marketing purposes, as permitted by law." :
  "Với sự đồng ý của bạn hoặc với cơ hội để bạn rút lại sự đồng ý, chúng tôi có thể chia sẻ thông tin của bạn với các bên thứ ba cho các mục đích tiếp thị, khi được pháp luật cho phép.",
  "Interactions with Other Users" : "Tương tác với những người dùng khác",
  "If you interact with other users of Fuda, those users may see your name, profile photo, and descriptions of your activity, including sending invitations to other users, chatting with other users, liking posts, and following blogs." : 
  "Nếu bạn tương tác với những người dùng khác của Fuda, những người dùng đó có thể thấy tên, ảnh hồ sơ và mô tả về hoạt động của bạn, bao gồm cả việc gửi lời mời đến những người dùng khác, trò chuyện với những người dùng khác, thích bài đăng và theo dõi blog.",
  "Online Postings" : "Bài đăng trực tuyến",
  "When you post comments, contributions, or other content to Fuda, your posts may be viewed by all users and may be publicly distributed outside Fuda in perpetuity." : 
  "Khi bạn đăng nhận xét, đóng góp hoặc nội dung khác lên Fuda, tất cả người dùng có thể xem bài đăng của bạn và có thể được phân phối công khai bên ngoài Fuda vĩnh viễn.",
  "Third-Party Advertisers" : "Quảng cáo bên thứ ba",
  "We may use third-party advertising companies to serve ads when you visit Fuda. These companies may use information about your visits to Fuda and other websites that are contained in web cookies to provide advertisements about goods and services of interest to you." : 
  "Chúng tôi có thể sử dụng các công ty quảng cáo của bên thứ ba để phân phát quảng cáo khi bạn sử dung Fuda. Các công ty này có thể sử dụng thông tin về những lần bạn ghé thăm Fuda và các trang web khác có trong cookie web để cung cấp quảng cáo về hàng hóa và dịch vụ mà bạn quan tâm.",
  "Affiliates" : "Tiếp thị liên kết",
  "We may share your information with our affiliates, in which case we will require those affiliates to honor this Privacy Policy. Affiliates include our parent company and any subsidiaries, joint venture partners, or other companies that we control or that are under common control with us." : 
  "Chúng tôi có thể chia sẻ thông tin của bạn với các chi nhánh của chúng tôi, trong trường hợp đó, chúng tôi sẽ yêu cầu các chi nhánh đó tuân theo Chính sách Bảo mật này. Các chi nhánh bao gồm công ty mẹ của chúng tôi và bất kỳ công ty con nào, đối tác liên doanh hoặc các công ty khác mà chúng tôi kiểm soát hoặc dưới sự kiểm soát chung của chúng tôi.",
  "Business Partners" : "Đối tác kinh doanh",
  "We may share your information with our business partners to offer you certain products, services, or promotions." : 
  "Chúng tôi có thể chia sẻ thông tin của bạn với các đối tác kinh doanh của chúng tôi để cung cấp cho bạn một số sản phẩm, dịch vụ hoặc chương trình khuyến mãi.",
  "Social Media Contacts" : "Liên kết với mạng xã hội ",
  "If you connect to Fuda through a social network, your contacts on the social network will see your name, profile photo, and descriptions of your activity." : 
  "Nếu bạn kết nối với Fuda thông qua mạng xã hội, các liên hệ của bạn trên mạng xã hội sẽ thấy tên, ảnh hồ sơ và mô tả về hoạt động của bạn.",
  "Other Third Parties" : "Bên thứ ba khác",
  "We may share your information with advertisers and investors to conduct general business analysis. We may also share your information with such third parties for marketing purposes, as permitted by law." : 
  "Chúng tôi có thể chia sẻ thông tin của bạn với các nhà quảng cáo và nhà đầu tư để tiến hành phân tích kinh doanh chung. Chúng tôi cũng có thể chia sẻ thông tin của bạn với các bên thứ ba như vậy cho các mục đích tiếp thị, khi được pháp luật cho phép.",
  "Sale or Bankruptcy" : "Kinh doanh hoặc Phá sản",
  "If we reorganize or sell all or some of our assets, undergo a merger, or are acquired by another entity, we may transfer your information to the successor entity. If we venture out of business or enter bankruptcy, your information would be an asset transferred or acquired by a 3rd party. You acknowledge that such transfers may occur which the transferee may decline to honor commitments we made during this Privacy Policy." : 
  "Nếu chúng tôi tổ chức lại hoặc bán tất cả hoặc một số tài sản của mình, trải qua một cuộc sáp nhập hoặc được một pháp nhân khác mua lại, chúng tôi có thể chuyển thông tin của bạn cho pháp nhân kế thừa. Nếu chúng tôi ngừng kinh doanh hoặc phá sản, thông tin của bạn sẽ là tài sản được chuyển nhượng hoặc mua lại bởi bên thứ ba. Bạn thừa nhận rằng việc chuyển giao như vậy có thể xảy ra mà bên nhận có thể từ chối thực hiện các cam kết mà chúng tôi đã thực hiện trong Chính sách bảo mật này.",
  "We don't seem to be answerable for the actions of third parties with whom you share personal or sensitive data, and that we haven't any authority to manage or control third-party solicitations. If you now not wish to receive correspondence, emails, or other communications from third parties, you're to blame for contacting the third party directly." : 
  "Chúng tôi không thể chịu trách nhiệm về hành động của các bên thứ ba mà bạn chia sẻ dữ liệu cá nhân hoặc dữ liệu nhạy cảm và chúng tôi không có bất kỳ quyền hạn nào để quản lý hoặc kiểm soát các yêu cầu của bên thứ ba. Nếu bây giờ bạn không muốn nhận thư từ, email hoặc thông tin liên lạc khác từ bên thứ ba, bạn có trách nhiệm liên hệ trực tiếp với bên thứ ba.",
  "TRACKING TECHNOLOGIES" : "CÔNG NGHỆ THEO DÕI",
  "Cookies and Web Beacons" : "Cookie và Web Beacons",
  "We may use cookies, web beacons, tracking pixels, and other tracking technologies on Fuda to help customize the application and improve your experience. When you access our application, your personal information is not collected through the use of tracking technology. Most browsers are set to accept cookies by default. You can remove or reject cookies, but be aware that such action could affect the availability and functionality of Fuda. You may not decline web beacons. However, they can be rendered ineffective by declining all cookies or by modifying your web browser's settings to notify you each time a cookie is tendered, permitting you to accept or decline cookies on an individual basis." : 
  "Chúng tôi có thể sử dụng cookie, dữ liệu web, pixel theo dõi và các công nghệ theo dõi khác trên Fuda để giúp tùy chỉnh ứng dụng và cải thiện trải nghiệm của bạn. Khi bạn truy cập ứng dụng của chúng tôi, thông tin cá nhân của bạn không được thu thập thông qua việc sử dụng công nghệ theo dõi. Hầu hết các trình duyệt được đặt để chấp nhận cookie theo mặc định. Bạn có thể xóa hoặc từ chối cookie, nhưng lưu ý rằng hành động đó có thể ảnh hưởng đến tính khả dụng và chức năng của Fuda. Bạn không thể từ chối báo hiệu web. Tuy nhiên, chúng có thể không hiệu quả bằng cách từ chối tất cả cookie hoặc bằng cách sửa đổi cài đặt của trình duyệt web của bạn để thông báo cho bạn mỗi khi cookie được đấu thầu, cho phép bạn chấp nhận hoặc từ chối cookie trên cơ sở cá nhân.",
  "Internet-Based Advertising" : "Quảng cáo dựa trên Internet",
  "Additionally, we may use third-party software to serve ads on our application, implement email marketing campaigns, and manage other interactive marketing initiatives. This third-party software may use cookies or similar tracking technology to help manage and optimize your online experience with us. For more information about opting out of interest-based ads, visit the" : 
  "Ngoài ra, chúng tôi có thể sử dụng phần mềm của bên thứ ba để phân phát quảng cáo trên ứng dụng của mình, thực hiện các chiến dịch tiếp thị qua email và quản lý các sáng kiến tiếp thị tương tác khác. Phần mềm của bên thứ ba này có thể sử dụng cookie hoặc công nghệ theo dõi tương tự để giúp quản lý và tối ưu hóa trải nghiệm trực tuyến của bạn với chúng tôi. Để biết thêm thông tin về việc chọn không tham gia quảng cáo dựa trên sở thích, hãy truy cập vào",
  "or" : "hoặc",
  "Website Analytics" : "Phân tích dữ liệu Web",
  "We may also partner with selected third-party vendors[, such as Cloudfare, Google Analytics..., to allow tracking technologies and remarketing services on the app through the use of first-party cookies and third-party cookies, to, among other things, analyze and track users' use of the application, determine the popularity of certain content, and better understand online activity. By accessing our application, you consent to the collection and use of your information by these third-party vendors. You are encouraged to review their privacy policy and contact them directly for responses to your questions. We do not transfer personal information to these third-party vendors. However, if you do not want any information to be collected and used by tracking technologies, you can install and/or update your settings. You should be aware that getting a new computer, installing a new browser, upgrading an existing browser, or erasing or otherwise altering your browser's cookies files may also clear certain opt-out cookies, plug-ins, or settings." : 
  "Chúng tôi cũng có thể hợp tác với các nhà cung cấp bên thứ ba được chọn [, chẳng hạn như [Adobe Analytics,] [Clicktale,] [Clicky,] [Cloudfare,] [Crazy Egg,] [Flurry Analytics,] [Google Analytics,] [Heap Analytics,] [Inspectlet,] [Kissmetrics,] [Mixpanel,] [Piwik,] và những thứ khác], để cho phép các công nghệ theo dõi và dịch vụ tiếp thị lại trên ứng dụng thông qua việc sử dụng cookie của bên thứ nhất và cookie của bên thứ ba, để, trong số những thứ khác, phân tích và theo dõi việc sử dụng ứng dụng của người dùng, xác định mức độ phổ biến của một số nội dung nhất định , và hiểu rõ hơn về hoạt động trực tuyến. Bằng cách truy cập ứng dụng của chúng tôi, bạn đồng ý với việc thu thập và sử dụng thông tin của bạn bởi các nhà cung cấp bên thứ ba này. Bạn được khuyến khích xem lại chính sách bảo mật của họ và liên hệ trực tiếp với họ để có câu trả lời cho các câu hỏi của bạn. Chúng tôi không chuyển thông tin cá nhân cho các nhà cung cấp bên thứ ba này. Tuy nhiên, nếu bạn không muốn công nghệ theo dõi thu thập và sử dụng bất kỳ thông tin nào, bạn có thể cài đặt và / hoặc cập nhật cài đặt của mình. Bạn nên biết rằng việc nhận một máy tính mới, cài đặt trình duyệt mới, nâng cấp trình duyệt hiện có hoặc xóa hoặc thay đổi các tệp cookie của trình duyệt cũng có thể xóa một số cookie, plugin hoặc cài đặt chọn không tham gia nhất định.",
  "THIRD-PARTY WEBSITES" : "TRANG WEB THỨ BA",
  "The Application may contain links to third-party websites and applications of interest, including advertisements and external services, that don't seem to be affiliated with us. Once you have got used these links to depart the appliance, any information you provide to those third parties isn't covered by this Privacy Policy, and that we cannot guarantee the protection and privacy of your information. Before visiting and providing any information to any third-party websites, you must inform yourself of the privacy policies and practices (if any) of the third-party answerable for that website, and may take those steps necessary to, in your discretion, protect the privacy of your information. We don't seem to be answerable for the content or privacy and security practices and policies of any third parties, including other sites, services, or applications which will be linked to or from the applying." : 
  "Ứng dụng có thể chứa các liên kết đến các trang web của bên thứ ba và các ứng dụng quan tâm, bao gồm cả quảng cáo và các dịch vụ bên ngoài, dường như không được liên kết với chúng tôi. Khi bạn đã sử dụng các liên kết này để khởi động thiết bị, bất kỳ thông tin nào bạn cung cấp cho các bên thứ ba đó đều không được Chính sách bảo mật này đề cập đến và chúng tôi không thể đảm bảo việc bảo vệ và quyền riêng tư đối với thông tin của bạn. Trước khi truy cập và cung cấp bất kỳ thông tin nào cho bất kỳ trang web của bên thứ ba nào, bạn phải chú ý về các chính sách và thông lệ bảo mật (nếu có) của bên thứ ba có thể trả lời cho trang web đó và có thể thực hiện các bước cần thiết để bảo vệ, theo quyết định của bạn. tính riêng tư của thông tin của bạn. Chúng tôi dường như không chịu trách nhiệm về nội dung hoặc các thông lệ và chính sách về quyền riêng tư và bảo mật của bất kỳ bên thứ ba nào, bao gồm các trang web, dịch vụ hoặc ứng dụng khác sẽ được liên kết đến hoặc từ ứng dụng.",
  "SECURITY OF YOUR INFORMATION" : "BẢO MẬT THÔNG TIN CỦA BẠN",
  "We use administrative, technical, and physical security measures to assist protect your personal information. While we've taken reasonable steps to secure the non-public information you provide to us, please bear in mind that despite our efforts, no security measures are perfect or impenetrable, and no method of information transmission is guaranteed against any interception or other sort of misuse. Any information disclosed online is susceptible to interception and misuse by unauthorized parties. Therefore, we cannot guarantee complete security if you provide personal information." : 
  "Chúng tôi sử dụng các biện pháp bảo mật hành chính, kỹ thuật và vật lý để hỗ trợ bảo vệ thông tin cá nhân của bạn. Mặc dù chúng tôi đã thực hiện các bước hợp lý để bảo mật thông tin không công khai mà bạn cung cấp cho chúng tôi, xin lưu ý rằng bất chấp nỗ lực của chúng tôi, không có biện pháp bảo mật nào là hoàn hảo hoặc không thể xuyên thủng và không có phương pháp truyền thông tin nào được đảm bảo chống lại bất kỳ hành vi đánh chặn hoặc hình thức nào khác sử dụng sai. Bất kỳ thông tin nào được tiết lộ trực tuyến đều dễ bị các bên không có thẩm quyền đánh chặn và sử dụng sai mục đích. Do đó, chúng tôi không thể đảm bảo bảo mật hoàn toàn nếu bạn cung cấp thông tin cá nhân.",
  "POLICY FOR CHILDREN" : "CHÍNH SÁCH DÀNH CHO TRẺ EM",
  "We do not knowingly solicit information from or market to children under the age of 13. If you become aware of any data we have collected from children under age 13, please contact us using the contact information provided below." : 
  "Chúng tôi không cố ý thu thập thông tin từ hoặc tiếp thị cho trẻ em dưới 13 tuổi. Nếu bạn biết bất kỳ dữ liệu nào chúng tôi đã thu thập từ trẻ em dưới 13 tuổi, vui lòng liên hệ với chúng tôi bằng cách sử dụng thông tin liên hệ được cung cấp bên dưới.",
  "CONTROLS FOR DO-NOT-TRACK FEATURES" : "ĐIỀU KHIỂN CÁC TÍNH NĂNG KHÔNG THEO DÕI",
  "Most web browsers and some mobile operating systems (and our mobile applications) include a Do-Not-Track (“DNT”) feature or setting you'll activate to signal your privacy preference to not have data about your online browsing activities monitored and picked up. No uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we don't currently reply to DNT browser signals or the other mechanism that automatically communicates your choice to not be tracked online. If a regular for online tracking is adopted that we must follow within the future, we'll inform you that practice during a Good Book of this Privacy Policy." : 
  "Hầu hết các trình duyệt web và một số hệ điều hành di động (và các ứng dụng di động của chúng tôi) bao gồm tính năng Không theo dõi (“KTD”) hoặc cài đặt mà bạn sẽ kích hoạt để báo hiệu tùy chọn bảo mật của bạn để không có dữ liệu về các hoạt động duyệt web trực tuyến của bạn được giám sát và chọn lên. Không có tiêu chuẩn công nghệ thống nhất để nhận biết và triển khai tín hiệu KTD đã được hoàn thiện. Do đó, chúng tôi hiện không trả lời các tín hiệu trình duyệt KTD hoặc cơ chế khác tự động thông báo lựa chọn của bạn để không bị theo dõi trực tuyến. Nếu thông lệ theo dõi trực tuyến được áp dụng mà chúng tôi phải tuân theo trong tương lai, chúng tôi sẽ thông báo cho bạn thông lệ đó trong Sách Hay về Chính sách Bảo mật này.",
  "OPTIONS REGARDING YOUR INFORMATION" : "CÁC LỰA CHỌN LIÊN QUAN ĐẾN THÔNG TIN CỦA BẠN",
  "Account Information" : "Thông tin tài khoản",
  "You may at any time review or change the information in your account or terminate your account by:" : 
  "Bạn có thể xem lại hoặc thay đổi thông tin trong tài khoản của mình bất cứ lúc nào hoặc chấm dứt tài khoản của mình bằng cách:",
  "Logging into your account settings and updating your account" : "Đăng nhập vào cài đặt tài khoản của bạn và cập nhật tài khoản của bạn",
  "Contacting us using the contact information provided below" : "Liên hệ với chúng tôi sử dụng những thông tin mà bạn đã cung cấp",
  "Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, some information may be retained in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our Terms of Use, and/or comply with legal requirements." : 
  "Theo yêu cầu chấm dứt tài khoản của bạn, chúng tôi sẽ hủy kích hoạt hoặc xóa tài khoản và thông tin của bạn khỏi cơ sở dữ liệu đang hoạt động của chúng tôi. Tuy nhiên, một số thông tin có thể được giữ lại trong tệp của chúng tôi để ngăn chặn gian lận, khắc phục sự cố, hỗ trợ bất kỳ cuộc điều tra nào, thực thi Điều khoản sử dụng của chúng tôi và / hoặc tuân thủ các yêu cầu pháp lý.",
  "Emails and Communications" : "Emails và liên hệ",
  "If you no longer wish to receive correspondence, emails, or other communications from us, you may opt out by:" : 
  "Nếu bạn không còn muốn nhận thư từ, email hoặc các thông tin liên lạc khác từ chúng tôi, bạn có thể chọn không nhận bằng cách:",
  "Noting your preferences at the time you register your account with Fuda" : "Ghi nhận các tùy chọn của bạn tại thời điểm bạn đăng ký tài khoản với Fuda",
  "Logging into your account settings and updating your preferences." : "Đăng nhập vào cài đặt tài khoản của bạn và cập nhật tùy chọn của bạn.",
  "Contacting us using the contact information provided below." : "Liên hệ với chúng tôi bằng cách sử dụng thông tin liên hệ được cung cấp bên dưới.",
  "If you no longer wish to receive correspondence, emails, or other communications from third parties, you are responsible for contacting the third party directly." : 
  "Nếu bạn không còn muốn nhận thư từ, email hoặc thông tin liên lạc khác từ bên thứ ba, bạn có trách nhiệm liên hệ trực tiếp với bên thứ ba.",
  "CALIFORNIA PRIVACY RIGHTS" : " ",
  "California Civil Code Section 1798.83, also called the “Shine The Light” law, permits our users who are California residents to request and acquire from us, once a year and freed from charge, information about categories of non-public information (if any) we disclosed to 3rd parties for marketing purposes and also the names and addresses of all third parties with which we shared personal information within the immediately preceding year. If you're a California resident and would really like to create such missive of invitation, please submit your request in writing to us using the contact information provided below." :  " ",
  "If you're under 18 years old, reside in California, and have a registered account with the applying, you have got the correct to request the removal of unwanted data that you just publicly post on the applying. To request the removal of such data, please contact us using the contact information provided below, and include the e-mail address related to your account and an announcement that you just reside in California. we'll confirm the info isn't publicly displayed on the applying, but please bear in mind that the info might not be completely or comprehensively aloof from our systems." : " ",
  "CONTACT US" : "LIÊN HỆ VỚI CHÚNG TÔI",
  "If you have questions or comments about this Privacy Policy, please contact us at:" : "Nếu bạn có câu hỏi hoặc nhận xét về Chính sách bảo mật này, vui lòng liên hệ với chúng tôi theo địa chỉ:",

  // Page OrderAdvisor
  "Select one suitable package": "Chọn một gói phù hợp",
  "Checkout":  "Thanh toán",
  "Order successfully":  "Đặt đơn thành công",
  "Buy coin to continue":  "Mua coin để tiếp tục",
  "Video Reading": "Xem video",
  "Your name": "Tên của bạn",
  "Date of birth": "Ngày sinh",
  "Gender": "Giới tính",
  "Female": "Nữ",
  "Male": "Nam",
  "Other": "Khác",
  "General situation": "Vấn đề chung",
  "Questions": "Câu hỏi",
  "Please record a video": "Vui lòng gửi một video",
  "Your browser does not support the video tag.": "Trình duyệt của bạn không hỗ trợ video này",
  "Quick advice": "Lời khuyên",
  "Wholeheartedly": "Hết lòng",
  "Chat Duration": "Thời gian chat",
  "minutes": "phút",
  "Price per minute:": "Giá mỗi phút",
  "Payment method:": "Phương thức thanh toán:",
  "Wallet balance": "Số dư trên ví",
  "Total payment:": "Tổng tiền thanh toán:",
  "Wallet balance is not enough.": "Số dư trên ví không đủ.",
  "Current balance:": "Số dư hiện tại",
  "Arrears:": "Truy thu",
  "Your balance": "Số dư của bạn",
  "Payment successfully!": "Thanh toán thành công!",
  "Quick Reading": "Xem video nhanh",
  "Video Reading": "Xem video",
  "Order Now": "Đặt hàng ngày",
  "Order Now...": "Đặt hàng ngày...",
  "Submitting...": "Đang gửi yêu cầu...",
  "Submit": "Gửi yêu cầu",
  "Send your first Message": "Gửi tin nhắn đầu tiên",
  "Go to your Order": "Xem đơn hàng",
  "Buy coin to continue": "Mua coin để tiếp tục",

  // Page Advisor-term-and-conditions
  "Please read these terms and conditions carefully before using the Fuda web site and/or mobile application (the “Service”).": "Vui lòng đọc kỹ các điều khoản và điều kiện này trước khi sử dụng trang web hoặc ứng dụng di động của Fuda (được gọi tắt là “Dịch vụ”).",
  "THIS SERVICE IS FOR ENTERTAINMENT PURPOSES ONLY. YOU UNDERSTAND THAT BY ACCESSING, DOWNLOADING OR USING THE SERVICE, OPERATED BY BARGES TECHNOLOGIES, INC": 
  "DỊCH VỤ NÀY CHỈ DÀNH CHO MỤC ĐÍCH GIẢI TRÍ. BẠN HIỂU RẰNG BẰNG CÁCH TRUY CẬP, TẢI XUỐNG HOẶC SỬ DỤNG DỊCH VỤ DO CÔNG NGHỆ BARGES ĐIỀU HÀNH, gọi tắt các khái niệm:",
  "US": "CHÚNG TÔI",
  "WE": "CHÚNG TÔI",
  "OUR": "CHÚNG TÔI",
  "OR": "HOẶC",
  "YOU ARE SIGNIFYING THAT YOU ARE EIGHTEEN (18) YEARS OF AGE OR OLDER AND ARE OTHERWISE CAPABLE OF FORMING LEGALLY BINDING CONTRACTS UNDER APPLICABLE LAW AND THAT YOU HAVE READ, UNDERSTAND AND ACCEPT THESE ADVISOR TERMS AND CONDITIONS (THE": 
  "BẠN ĐỒNG Ý RẰNG BẠN TỪ MƯỜI TÁM (18) TUỔI TRỞ LÊN ĐỂ VÀ CÓ THỂ CHỊU TRÁCH NHIỆM TRƯỚC PHÁP LUẬT. BẠN HIỂU VÀ CHẤP NHẬN CÁC ĐIỀU KHOẢN VÀ ĐIỀU KIỆN CỦA CỐ VẤN TRONG",
  "ADVISOR TERMS": "ĐIỀU KHOẢN CỐ VẤN",
  "AND AGREE TO BE BOUND BY THEM.": "VÀ ĐỒNG Ý ĐƯỢC HỖ TRỢ.",
  "The terms and conditions below are in addition to the terms and condition contained in Our": "Các điều khoản và điều kiện bên dưới bổ sung cho các điều khoản và điều kiện có trong",
  "Terms of Use": "Điều khoản sử dụng",
  "and limit our liability and obligations to You, as an Advisor, and allow Us to change, suspend or terminate your access to and use of the Service at any time in Our sole discretion. It is your responsibility to read and agree to the following terms and conditions, along with any other policies on Our website including, but not limited to, Our": 
  "và quy định trách nhiệm và nghĩa vụ của chúng tôi đối với các cố vấn và chúng tôi có quyền thay đổi, tạm ngừng hoặc chấm dứt quyền truy cập và sử dụng dịch vụ của bạn bất kỳ lúc nào theo chính sách riêng của chúng tôi. Bạn có trách nhiệm đọc và đồng ý với các điều khoản, điều kiện và các chính sách khác bao gồm: ",
  "Privacy Policy": "Chính sách Quyền riêng tư",
  "and Our": "và",
  "BY ACCESSING OR USING THE SERVICE YOU AGREE TO BE BOUND BY THESE ADVISOR TERMS AS WELL AS ANY ADDITIONAL TERMS AND CONDITIONS CONTAINED IN OUR ": 
  "BẰNG CÁCH TRUY CẬP HOẶC SỬ DỤNG DỊCH VỤ BẠN ĐỒNG Ý ĐƯỢC CÁC ĐIỀU KHOẢN CỐ VẤN NÀY CŨNG NHƯ BẤT KỲ ĐIỀU KHOẢN VÀ ĐIỀU KIỆN BỔ SUNG NÀO CÓ TRONG ",
  "TERMS OF USE": "ĐIỀU KHOẢN SỬ DỤNG",
  "AND OUR OTHER POLICIES AND AGREEMENTS THAT MAY APPLY TO YOU, INCLUDING, BUT NOT LIMITED TO INDEMNIFICATION, DISCLAIMERS, AND LIMITATIONS OF LIABILITY.": 
  "VÀ CÁC CHÍNH SÁCH VÀ THỎA THUẬN KHÁC CỦA CHÚNG TÔI CÓ THỂ ÁP DỤNG CHO BẠN, BAO GỒM, NHƯNG KHÔNG GIỚI HẠN VỀ VIỆC CHỈ ĐỊNH, TUYÊN BỐ TỪ CHỐI VÀ GIỚI HẠN TRÁCH NHIỆM PHÁP LÝ.",
  "THESE ADVISOR TERMS APPLY TO ALL ADVISORS WHO ACCESS OR USE THE SERVICE. You understand and agree that your access to and use of the Service is conditioned on your acceptance of and compliance with these Advisor Terms. If You disagree with any part of the Advisor Terms then You must not use or access the Service.": 
  "CÁC ĐIỀU KHOẢN NÀY ÁP DỤNG CHO TẤT CẢ CÁC CỐ VẤN TÂM LINH ĐANG TRUY CẬP VÀ SỬ DỤNG DỊCH VỤ. Bạn cần đồng ý rằng khi chấp nhận và tuân thủ các Điều khoản của Cố vấn này bạn sẽ có thể sử dụng Dịch vụ của của chúng tôi. Nếu Bạn không đồng ý với bất kỳ phần nào của “Điều khoản cho Cố vấn” thì bạn sẽ không truy cập được Dịch vụ. ",
  "Becoming an Advisor": "TRỞ THÀNH CỐ VẤN TÂM LINH",
  "The Service is a psychic advice marketplace that brings advice-seekers and independent, psychic advisors (“Advisor(s)” or “You”) together through video, chat, and messaging. Users will search, engage, submit questions to You, or chat live via text or video with You, via orders that they place. Advisors record their responses or respond in real time for live video or chat sessions, along with any optional text, and deliver it to the clients. If your client desires a live chat-based psychic reading platform, You may invite them to try your services on Fuda, available at": 
  "Dịch vụ này là chuyên đưa ra những tư vấn tâm lịn cho những ai đang đi tìm kiếm lời khuyên từu những cố vấn tâm linh uy tín (như bạn) thông qua tin nhắn, video và gọi trực tiếp. Người dùng sẽ tìm đến bạn, gửi câu hỏi và đặt lịch xem với Bạn khi bạn đang hoạt động. Cố vấn sẽ gửi phần giải đáp trong thời gian cố định theo lịch đặt  theo 2 hình thức chat hoặc video cho khách hàng. Nếu khách hàng đã đặt lịch tư vấn đọc bài qua chat, bạn có thể mời họ dùng thử các dịch vụ khác của bạn trên Fuda tại",
  "To become an Advisor, You must first download the application and create an advisor profile. You will be required to submit your legal name, address, and telephone number so that We may identify You and contact You. You must also provide Us with your PayPal account information in order to receive payment through the Service. You must upload a photograph of yourself (other images, icons, or illustrations are not permitted), create a profile video and provide profile information to describe the types of services that You offer. Once You complete your profile, You will create and submit a sample reading for Us to review. After We receive your information and review your reading, You will be notified whether You have been accepted as an Advisor. We reserve the right to verify your identity and other background information at any time and You agree that You will provide Us with information to do so at Our reasonable request. You understand and agree that it is your responsibility to make sure that your profile is appropriate, and that all information that You provide is true, complete, up to date, and accurately reflects your credentials and is in compliance with all applicable laws and regulations.": 
  "Để trở thành Cố vấn, trước tiên Bạn phải tải ứng dụng và tạo tài khoản cho cố vấn. Bạn sẽ được yêu cầu cung cấp tên, địa chỉ và số điện thoại hợp pháp của mình để chúng tôi xác nhận thông tin và liên hệ với Bạ (nếu cần). Bạn cần cung cấp cho chúng tôi thông tin tài khoản PayPal để nhận tiền thanh toán từ Dịch vụ. Tiếp theo, bạn cần tải ảnh của mình lên (không được phép sử dụng ảnh, biểu tượng mạo danh), tạo và cung cấp thông tin hồ sơ để mô tả loại dịch vụ mà Bạn cung cấp. Khi bạn hoàn thành hồ sơ của mình, bạn cần gửi một trải bài mẫu để chúng tôi xem xét. Sau khi chúng tôi nhận được và xem xét  các thông tin của bạn, bạn sẽ nhận được thông báo duyệt hồ sơ cố vấn. Khi bạn đồng ý rằng bạn sẽ cung cấp thông tin cho chúng tô, chúng tôi có quyền xác minh danh tính và các thông tin cơ bản khác vào bất kỳ lúc nào. Vì thế, bạn cần có trách nhiệm đảm bảo rằng thông tin bạn cung cấp là trung thực, đầy đủ, chính xác với thông tin cá nhân của bạn và tuân thủ tất cả các luật và quy định hiện hành.",
  "By registering as an Advisor, You are not bound to continue to use the Service or to accept orders and provide readings for any length of time. You are free to use the Service when and how you choose, subject to the Advisor Terms. You may stop accepting orders for any period of time or stop using the Service all together for any reason or for no reason at all, provided that You fulfill outstanding orders and existing obligations.": 
  "Bằng cách đăng ký làm Cố vấn, Bạn không bị ràng buộc phải tiếp tục sử dụng Dịch vụ hoặc chấp nhận đơn đặt hàng và cung cấp các bài đọc trong bất kỳ khoảng thời gian nào. Bạn có thể tự do sử dụng Dịch vụ khi nào và như thế nào bạn chọn, tuân theo Điều khoản của Cố vấn. Bạn có thể ngừng chấp nhận đơn đặt hàng trong bất kỳ khoảng thời gian nào hoặc ngừng sử dụng Dịch vụ vì bất kỳ lý do gì hoặc không vì lý do gì, miễn là Bạn hoàn thành các đơn đặt hàng chưa thanh toán và các nghĩa vụ hiện có.",
  "Your Responsibilities as an Advisor": "TRÁCH NHIỆM CỦA MỘT NGƯỜI TƯ VẤN",
  "If a client selects You to be their Advisor, You should receive a push notification and an email message. You must have push notifications enabled in order to receive them. We encourage You to log in often to see if You have any new orders. If You have a pending order, it will be contained in the “My Jobs” section of the app. We recommend that You respond as soon as You can to encourage repeat orders and positive client feedback. If You know that You will be unavailable to fulfill orders You may simply turn your availability status to “off” in the “My Jobs” screen and, provided that no clients were in the process of placing an order, no new orders will be received.": 
  "Nếu khách hàng chọn Bạn làm Cố vấn của họ, Bạn sẽ nhận được thông báo và email. Bạn cần bật thông báo ứng dụng để nhận được chúng. Chúng tôi khuyến khích bạn đăng nhập thường xuyên để xem bạn có đơn đặt hàng mới nào không. Nếu bạn có một đơn đặt hàng đang chờ xử lý, đơn đặt hàng đó sẽ có trong phần “Công việc của tôi” trên ứng dụng. Chúng tôi khuyên bạn nên trả lời ngay khi bạn có thể để khuyến khích các đơn đặt hàng cũ và phản hồi tích cực của khách hàng. Nếu bạn không sắp xếp được lịch tư vấn, bạn chỉ cần chuyển trạng thái từ “bật” sang “tắt” trên màn hình “Công việc của tôi”. Miễn là không có khách hàng nào đang trong quá trình đặt hàng, bạn sẽ không nhận được thêm đơn đặt hàng mới.",
  "As You fulfill orders, keep in mind your clients will be able to provide feedback and reviews that will appear on your profile. Clients may edit their review for up to 2 weeks after it is entered and We do not remove or edit reviews except for extreme circumstances or to comply with Our": 
  "Khi bạn hoàn thành các đơn đặt hàng, hãy nhớ rằng khách hàng của bạn sẽ có thể cung cấp phản hồi và đánh giá sẽ xuất hiện trên hồ sơ của bạn. Khách hàng có thể chỉnh sửa đánh giá của họ trong tối đa 2 tuần sau khi đánh giá và chúng tôi không xóa hoặc chỉnh sửa đánh giá, ngoại trừ các trường hợp đặc biệt hoặc để tuân thủ",
  "We will not be liable for any delays or failures in your receipt of any push notifications or emails as delivery is subject to effective transmission from your network operator and processing by your mobile device. These services are provided on an AS IS, AS AVAILABLE basis.": 
  "Thông báo đẩy hoặc email khi gửi tùy thuộc vào đường truyền mạng và thiết bị di động của bạn đang sử dụng. Các dịch vụ này được cung cấp trên cơ sở NGUYÊN TRẠNG, HIỆN CÓ.",
  "Independent Contractor": "HỢP ĐỒNG",
  "You understand and agree that, as an Advisor and/or user of the Service, You are acting as an independent contractor and not as Our employee, agent, consultant or representative. You have exclusive control of the manner and means of giving your advice, subject to certain prohibited actions referenced below. As an Advisor, You do not have the right to enter into any contract on our behalf. Similarly, by your use of the Service, we are not establishing a partnership, franchise or joint venture between us. Under no circumstances shall You be considered Our employee. By using the Service, You represent and warrant that You have any and all permissions as may be required by federal, state, local, or international law, as applicable, to use the Service as an Advisor.": 
  "Bạn hiểu và đồng ý rằng, với tư cách là Cố vấn và / hoặc người sử dụng Dịch vụ, Bạn đang hoạt động với tư cách là một nhà tư vấn độc lập chứ không phải với tư cách là nhân viên, đại lý, nhà tư vấn hoặc đại diện của Chúng tôi. Bạn có trách nhiệm và quyền đối với những tư vấn, nội dung và cần tuân theo một số quy định bị cấm đề cập dưới đây. Với tư cách là Cố vấn, Bạn không có quyền thay mặt chúng tôi ký kết bất kỳ hợp đồng nào. Tương tự, bằng việc bạn sử dụng Dịch vụ, chúng tôi không thiết lập quan hệ đối tác, nhượng quyền thương mại hoặc liên doanh. Trong mọi trường hợp, Bạn sẽ không được coi là nhân viên của Chúng tôi. Bằng cách sử dụng Dịch vụ, Bạn tuyên bố và đảm bảo rằng Bạn có bất kỳ và tất cả các quyền được yêu cầu bởi luật liên bang, tiểu bang, địa phương hoặc quốc tế,...  nếu có, để sử dụng Dịch vụ với tư cách là Cố vấn.",
  "Advisor Fees": "PHÍ TƯ VẤN",
  "Advisors will be paid each time that they timely fulfill an order for a reading. As an Advisor, You will receive all payments for fulfilling your orders through PayPal or a similar service to be chosen at Our sole discretion. Any revenue that You earn through Our service is held for a period of two (2) weeks from the time that it is generated and there is a minimum withdrawal sum of $35. In the event that We receive a complaint about your service from a client and We find the complaint to have merit, We may choose to reverse the payment to your account and credit the client the amount paid for the order. In addition to reversing the payment to your account, depending on your behavior, You may be subject to disciplinary action in accordance with Our Advisor Terms. To withdraw money from your account, simply tap on the “Withdraw” button in the revenue screen of your account. Payment is usually deposited into your PayPal account within 2 to 4 days of your withdrawal request. If an order is placed, but You do not deliver the reading, the payment goes back to the client, You will receive zero compensation, and your account may be subject to suspension or termination.": 
  "Người tư vấn sẽ được thanh toán sau khi hoàn thành xong dịch vụ tư vấn. Với tư cách là Cố vấn, Bạn sẽ nhận được tất cả các khoản thanh toán cho việc thực hiện các đơn đặt hàng của mình thông qua PayPal hoặc một dịch vụ tương tự khác (được lựa chọn theo quyết định riêng của Chúng tôi). Bất kỳ doanh thu nào mà Bạn kiếm được thông qua dịch vụ của Chúng tôi được giữ trong khoảng thời gian hai (2) tuần kể từ khi nhận được và số tiền rút tối thiểu là 35 đô la. Trong trường hợp Chúng tôi nhận được khiếu nại về dịch vụ của bạn từ khách hàng và Chúng tôi sẽ căn cứ vào đơn khiếu nại để giải quyết. Chúng tôi có thể rút số tiền bạn đã nhận và hoàn lại cho khách hàng đã thanh toán theo đơn trên. Việc hoàn/trả khoản thanh toán vào tài khoản của bạn sẽ tùy thuộc vào mức độ nghiêm trọng và hành động của bạn. Bạn có thể bị kỷ luật theo Điều khoản Cố vấn của Chúng tôi. Để rút tiền từ tài khoản của bạn, chỉ cần nhấn vào nút “Rút tiền” trong màn hình doanh thu của tài khoản của bạn. Thanh toán thường được gửi vào tài khoản PayPal của bạn trong vòng 2 đến 4 ngày kể từ ngày bạn yêu cầu rút tiền. Nếu một đơn đặt hàng được đặt, nhưng Bạn không giao bản đọc, khoản thanh toán sẽ được chuyển lại cho khách hàng. Bạn sẽ không nhận được khoản bồi thường nào và tài khoản của bạn có thể bị tạm khóa hoặc hủy bỏ. ",
  "Notwithstanding the foregoing, Barges reserves sole discretion, for any reason, to either withhold any amount due to You for completed orders or to reverse payments posted to your account.": 
  "Trên hết, Barges có toàn quyền quyết định giữ lại bất kỳ số tiền nào với các đơn đặt hàng đã hoàn thành hoặc hoàn lại các khoản thanh toán được đăng nhập từ tài khoản của bạn.",
  "Advisor Discipline": "NGUYÊN TẮC TƯ VẤN",
  "If You fail to fulfill an order for any reason, the client's order will be cancelled and your Advisor profile will no longer appear on the Service. You will be unable to accept any new orders until your profile is relisted. However, if for any reason You turn your availability off or You fail to fulfill an order and your profile is removed, You are still responsible for on time delivery of any orders that were pending prior to the change in status. In the event that You violate any of these Advisor Terms, the terms and conditions contained in Our other policies, or You fail to fulfill client orders, Barges retains sole discretion to either (i) suspend an Advisor's account; (ii) take away or suspend your ability to enter earn bonus compensation through “Rush Delivery” mode; (iii) outright terminate your account; or (iv) take any other action We deem appropriate.": 
  "Nếu Bạn không thực hiện được đơn hàng vì bất kỳ lý do gì, đơn đặt hàng của khách hàng sẽ bị hủy và hồ sơ Cố vấn của bạn sẽ không còn xuất hiện trên Dịch vụ. Bạn sẽ không thể chấp nhận bất kỳ đơn đặt hàng mới nào cho đến khi hồ sơ của bạn được cung cấp lại. Tuy nhiên, nếu vì bất kỳ lý do gì Bạn tắt tính khả dụng của mình hoặc Bạn không thực hiện được đơn hàng và hồ sơ của bạn bị xóa, Bạn vẫn phải chịu trách nhiệm giao hàng đúng hạn cho bất kỳ đơn hàng nào đang chờ xử lý trước khi thay đổi trạng thái. Trong trường hợp Bạn vi phạm bất kỳ Điều khoản nào trong số các Điều khoản của Cố vấn này, các chính sách khác của Chúng tôi hoặc Bạn không thực hiện các đơn đặt hàng của khách hàng, Barges sẽ có toàn quyền quyết định (i) xóa tài khoản của Cố vấn; (ii) tước bỏ hoặc vô hiệu hóa khả năng tham gia kiếm tiền thưởng của bạn thông qua chế độ “Giao hàng gấp rút”; (iii) chấm dứt thỏa thuận với tài khoản của bạn; hoặc (iv) thực hiện bất kỳ hành động nào khác mà Chúng tôi cho là phù hợp.",
  "If You feel that your profile was wrongly removed, contact the support team and tell them why.": "Nếu Bạn có bất cứ khiếu nại nào, hãy liên hệ với nhóm hỗ trợ và cho họ biết lý do.",
  "Advisor Code of Conduct": "CHUẨN MỰC HÀNH VI",
  "You, the Advisor, will be solely and fully liable for all conduct, services, advice, postings and transmissions that are made in connection with your use of the Service. We reserve the right to monitor all videos, messages, communications, and content to ensure that You are complying with these Advisor Terms, as well as the terms and conditions contained in our other policies. You understand and agree that all of your communications with clients must be exclusively carried out via the Service. If You fail to abide by this code of conduct, in addition to pursuing any and all other legal remedies available to Us, your account may be suspended or terminated at Our sole discretion.": 
  "Bạn sẽ chịu trách nhiệm hoàn toàn đối với tất cả các hành vi, dịch vụ, lời khuyên, bài đăng khi bạn sử dụng Dịch vụ của chúng tôi. Chúng tôi có quyền giám sát tất cả video, tin nhắn, thông tin liên lạc và nội dung để đảm bảo rằng Bạn đang tuân thủ bộ “Điều khoản của Cố vấn”, cũng như các điều khoản và điều kiện có trong các chính sách khác của chúng tôi. Tất cả các giao tiếp của bạn với khách hàng phải được thực hiện độc quyền qua Dịch vụ. Nếu Bạn không tuân thủ quy tắc ứng xử này, tài khoản của Bạn có thể bị tạm ngưng hoặc vô hiệu hóa theo quyết định riêng của Chúng tôi.",
  "When using the Service, the following guidelines are to be followed by all Advisors:": "Khi sử dụng Dịch vụ, tất cả các Cố vấn phải tuân theo các nguyên tắc sau:",
  "You will not knowingly engage in any conduct designed to defraud, mislead, or otherwise deceive any user of the Service.": 
  "Không tham gia, thực hiện vào bất kỳ hành vi nào với mục đích lừa đảo, gian lận với người dùng các tính năng trên ứng dụng.",
  "You will not using vulgar, racist, offensive or obscene language.": "Không sử dụng ngôn ngữ thô tục, phân biệt chủng tộc, xúc phạm hoặc tục tĩu.",
  "You will not harass, threaten, or embarrass a client or other user of the Service.": "Bạn sẽ không quấy rối, đe dọa, làm tổn thương danh dự, uy tín của khách hàng hoặc người dùng khác trên Dịch vụ.",
  "You will not wear revealing clothing.": "Không mặc quần áo hở hang, phản cảm.",
  "You will present a pleasant visual atmosphere for your client free of disruptions, background noise and outside interferences. For example, You will not record your Advisor responses from a loud party, while driving, or while eating.": 
  "Tạo ra một bầu không khí gần gũi, thân thiện cho khách hàng và không để bị gián đoạn bởi tiếng ồn xung quanh và các tác động bên ngoài. Ví dụ: Bạn sẽ không ghi âm lời tư vấn trong một bữa tiệc ồn ào, khi đang lái xe hoặc khi đang ăn.",
  "You will maintain a professional relationship with your clients at all times and treat them with dignity and respect.": 
  "Bạn sẽ luôn duy trì mối quan hệ chuyên nghiệp với khách hàng của mình và đối xử với họ một cách tử tế và tôn trọng.",
  "You will not badger or retaliate against a client who left you a negative rating.": "Bạn sẽ không nói xấu hoặc trả đũa khách hàng đã để lại cho bạn một đánh giá tiêu cực.",
  "You will not post or exchange contact information with any client (or any other information which would allow You to interact with a client outside of the Service), except for your BitWine user name if You so desire. This includes, but is not limited to, your telephone number, postal address, e-mail address, social media accounts, other sites where You offer services, or the name You are operating under on any of these sites.": 
  "Bạn sẽ không đăng hoặc trao đổi thông tin liên hệ của khách hàng (bao gồm các thông tin không nằm trong Dịch vụ), ngoại trừ tên tài khoản BitWine của bạn nếu Bạn muốn. Điều này bao gồm: số điện thoại, địa chỉ bưu điện, địa chỉ email, tài khoản mạng xã hội, các trang web khác mà Bạn cung cấp dịch vụ, hoặc tên Bạn đang điều hành trên bất kỳ trang web nào khác.",
  "You will not interact or communicate, or attempt to interact or communicate, with a client outside of the Service, other than through BitWine.": 
  "Không chủ đích tương tác hoặc cố gắng kết nối với khách hàng bên ngoài Dịch vụ mà không phải thông qua BitWine.",
  "You will not give advice that is of the legal or medical nature. Such advice may only be provided by a properly licensed medical or legal professional, with an established physician-patient relationship, attorney-client, or other relationship. You understand and agree that if a client is suicidal or is indicating potential self-harm, You will tell them that they should seek immediate professional medical care provided by a properly licensed medical professional.":
  "Bạn sẽ không đưa ra lời khuyên có tính chất pháp lý hoặc y dược. Những lời khuyên đó chỉ có thể được cung cấp bởi một chuyên gia y dược hoặc ban pháp lý được cấp phép phù hợp, có mối quan hệ bác sĩ-bệnh nhân, luật sư-khách hàng hoặc các mối quan hệ khác. Nếu khách hàng tự tử hoặc có dấu hiệu tự làm hại bản thân, Bạn cần cho họ biết rằng họ nên tìm kiếm tư vấn tâm lí chuyên nghiệp ngay lập tức.",
  "You will not offer services that guarantee the direct altering of the future, including, but not limited to, spells or spell removal.": 
  "Bạn sẽ không cung cấp các dịch vụ đảm bảo sự thay đổi trực tiếp trong tương lai.",
  "You will advise clients, whom You have been introduced to through the Service, only by, on, or through the Service.": "Bạn sẽ tư vấn cho khách hàng, những người mà Bạn đã được giới thiệu thông qua Dịch vụ và trong quy định của Dịch vụ.",
  "You will comply with any applicable rules, regulations, laws, or statutes in using the Service as an Advisor.": "Bạn sẽ tuân thủ mọi quy tắc, quy định, luật hoặc chế độ hiện hành trong việc sử dụng Dịch vụ với tư cách là Cố vấn.",
  "You will not provide false information or misleading information in connection with your Advisor profile.": "Bạn sẽ không cung cấp thông tin sai lệch hoặc thông tin gây hiểu nhầm, thông tin không liên quan đến lĩnh vực đăng kí theo hồ sơ Cố vấn của bạn.",
  "You will regularly maintain your profile and assure that the information is accurate and up-to-date.": "Bạn sẽ thường xuyên cập nhập thông tin hồ sơ của mình và đảm bảo rằng thông tin là chính xác.",
  "You will assure that at all times You accurately list and represent your skills, qualifications, and background.": "Bạn sẽ đảm bảo rằng tại mọi thời điểm bạn đã đăng kí đúng theo các kỹ năng, trình độ chuyên môn và nền tảng của bạn.",
  "You will not perform services as an Advisor in any jurisdiction where it is unlawful to do so.": "Bạn sẽ không thực hiện các dịch vụ với tư cách là Cố vấn ở bất kỳ hình thức nào nếu làm như vậy là bất hợp pháp.",
  "You will not provide any information or advice or recommendations pertaining to the value, viability, or investment or purchase value of any security, sweepstakes, lottery, games of chance, etc.": 
  "Bạn sẽ không cung cấp bất kỳ thông tin hoặc lời khuyên hoặc khuyến nghị nào liên quan đến giá trị, khả năng tồn tại hoặc giá trị mua/bán của bất kỳ thông tin bảo mật, rút thăm trúng thưởng, xổ số, trò chơi may mắn,...",
  "You will not offer additional or alternative services for additional payment or compensation.": "Bạn sẽ không cung cấp các dịch vụ bổ sung hoặc dịch vụ thay thế để trả thêm hoặc bồi thường dịch vụ.",
  "You will not engage in any conduct or take any actions that may manipulate, undermine, and/or interfere with any ratings of any Advisor on the Service. We reserve the right to exclude without explanation any rating that We think may compromise the integrity of the Advisors feedback system.": 
  "Bạn sẽ không tham gia vào bất kỳ hành vi nào nhằm mục đích thao túng, phá hoại và can thiệp vào bảng xếp hạng của các Cố vấn trên Dịch vụ. Chúng tôi không chấp nhận và lưu bất cứ thông tin xếp hạng và không cần giải thích bất kỳ gì về bảng xếp hạng nếu nó ảnh hưởng đến hệ thống xếp hạng Cố vấn.",
  "You will not engage in conduct that disparages or otherwise negatively affects the Fuda, AppUni brands.": "Bạn sẽ không tham gia vào các hành vi làm mất uy tín hoặc ảnh hưởng tiêu cực đến thương hiệu Fuda, AppUni.",
  "You will not attempt to send any “spam” or “junk” or any other form of unsolicited emails or communications to any user of the Service.": 
  "Bạn sẽ không cố gắng gửi bất kỳ “thư rác”, “mã độc” hoặc bất kỳ hình thức email nào người dùng dịch vụ.",
  "You will not promote, advertise, introduce or refer any other service to any other user.": "Không quảng bá, quảng cáo bất kỳ dịch vụ nào khác cho người dùng trên ứng dụng.",
  "You will take all necessary measures to safeguard any and all confidential information provided to You by any client.": "Bạn sẽ thực hiện tất cả các biện pháp cần thiết để bảo vệ bất kỳ và tất cả thông tin bí mật do bất kỳ khách hàng nào cung cấp cho Bạn.",
  "You will not share or disclose any personal information about a client with anyone, including, but not limited to, your friends, family members, and professional colleagues. If You learn personal information about a client from sources outside of the Service, You will disclose such information to the client as well as the source of your information. You will not conceal such information and otherwise use it to your advantage as an Advisor.": 
  "Bạn sẽ không chia sẻ hoặc tiết lộ bất kỳ thông tin cá nhân nào về khách hàng với bất kỳ ai, bao gồm cả bạn bè, gia đình và đồng nghiệp thân thiết của bạn. Nếu Bạn Sử dụng thông tin cá nhân về khách hàng từ các nguồn bên ngoài Dịch vụ, Bạn sẽ thông báo và cần được khách hàng chấp thuận.·Bạn sẽ không che giấu những thông tin đó và sử dụng nó để làm lợi thế cho bạn với tư cách là Cố vấn.",
  "You will not register multiple accounts as an Advisor and shall at all times have at most one active Advisor account.": "Không đăng ký quá nhiều tài khoản với tư cách là Cố vấn, nếu có ít nhất một trong số đó vẫn đang hoạt động.",
  "You will not, under any circumstances whatsoever, accept, solicit or request any payment, tips, compensation or renumeration of any kind, either directly or indirectly, from a client.": "Trong bất kỳ trường hợp nào, bạn sẽ không gạ gẫm hoặc yêu cầu bất kỳ khoản thanh toán, tiền boa, tiền bồi thường hoặc từ bỏ dưới bất kỳ hình thức nào, dù trực tiếp hay gián tiếp, từ khách hàng.",
  "If We suspect that You have violated the Advisor Code of Conduct or have otherwise breached this Agreement, in addition to all other rights and remedies available to Us herein or by law, Barges reserves the right to withhold or reverse payments, levy fines, and/or suspend or terminate your account in its sole and absolute discretion.": 
  "Nếu Chúng tôi nghi ngờ rằng Bạn đã vi phạm Quy tắc Ứng xử của Cố vấn hoặc đã vi phạm Thỏa thuận này, ngoài tất cả các quyền và đề xuất khắc phục khác theo luật, Barges có quyền giữ lại hoặc xử lý các khoản thanh toán: tiền phạt; vô hiệu hóa hoặc chấm dứt tài khoản của bạn theo quyết định khác. ",
  "Privacy & Confidentiality": "QUYỀN RIÊNG TƯ & BẢO MẬT",
  "All information disclosed to You as an Advisor by a client should be treated as private and confidential information. This information must not be shared, posted, or disclosed to any person (including other Advisors), entity, group, publication, forum, website, or any other place, whatsoever. You understand that your clients are not required to maintain confidentiality with regard to any advice that You provide to a client as an Advisor.": 
  "Mọi thông tin được khách hàng tiết lộ cho Bạn với tư cách là Cố vấn phải được coi là thông tin riêng tư và bí mật. Thông tin này không được chia sẻ, đăng hoặc tiết lộ cho bất kỳ người nào (bao gồm các Cố vấn khác), tổ chức, nhóm, ấn phẩm, diễn đàn, trang web hoặc bất kỳ nơi nào khác. Bạn hiểu rằng khách hàng của bạn không được yêu cầu duy trì tính bảo mật đối với bất kỳ lời khuyên nào mà Bạn cung cấp cho khách hàng với tư cách là Cố vấn.",
  "Any information or content that You post or transmit through the Service will not be considered your confidential information. You grant Barges an unlimited, irrevocable, royalty-free license to use, reproduce, display, edit, copy, transmit, process, control, publicly perform and create derivative works, communicate to the public or any third party any such information and content on a world-wide basis.": 
  "Bất kỳ thông tin hoặc nội dung nào mà Bạn đăng hoặc gửi qua Dịch vụ sẽ không được coi là thông tin bí mật của bạn. Bạn cấp cho Barges giấy phép không giới hạn, không thể thu hồi, miễn phí bản quyền để sử dụng, tái sản xuất, hiển thị, chỉnh sửa, sao chép, truyền tải, xử lý, kiểm soát, thực hiện công khai và tạo ra các tác phẩm phái sinh, truyền thông đại chúng, bất kỳ bên thứ ba về thông tin và nội dung cụ thể.",
  "You also acknowledge that you will maintain compliance with the rules of GDPR (General Data Protection Regulation). This includes the following rights to which Clients are entitled:": 
  "Bạn cũng thừa nhận rằng bạn sẽ tuân thủ các quy tắc của GDPR (Quy định chung về bảo mật dữ liệu). Điều này bao gồm các quyền sau mà Khách hàng được hưởng:",
  "Right of access": "Quyền truy cập",
  "Right to erasure of personal data": "Quyền xóa dữ liệu cá nhân",
  "Data portability": "Khả năng di chuyển dữ liệu",
  "Data protection": "Bảo vệ dữ liệu",
  "This means that You are not to do anything that might infringe upon the Client's privacy rights, including but not limited to:": 
  "Điều này có nghĩa là Bạn không được làm bất cứ điều gì vi phạm quyền riêng tư của Khách hàng, bao gồm:",
  "Keeping notes physically or on your own computer/device containing any of the Client's personal data": "Lưu trữ dữ liệu cá nhân Khách hàng trong ghi chú cá nhân hoặc trên thiết bị",
  "Downloading files or web pages containing any of the Client's personal data": "Tải xuống các tệp hoặc trang web chứa bất kỳ dữ liệu cá nhân nào của Khách hàng",
  "Sharing personal Client data with anyone": "Chia sẻ dữ liệu khách hàng cá nhân với bất kỳ ai",
  "Failure to take reasonable measures to protect personal Client data": "Không trang bị các biện pháp để bảo vệ dữ liệu cá nhân của Khách hàng",
  "The full text of the GDPR can be found": "Toàn bộ văn bản của GDPR có thể được tìm thấy tại",
  "BY BEING AN ADVISOR ON FUDA, YOU ACKNOWLEDGE THAT YOU ARE LIABLE FOR THE COMPLIANCE WITH THE RULES OF GDPR AS OUTLINED ABOVE. FAILURE TO COMPLY WITH THESE RULES MAY EXPOSE YOU TO MONETARY FINES OR OTHER PENALTIES BY EU AUTHORITIES, AS WELL AS TERMINATION OF YOUR FUDA ACCOUNT.": 
  "TRỞ THÀNH NHÀ TƯ VẤN TẠI Fuda, BẠN XÁC NHẬN RẰNG BẠN CÓ TRÁCH NHIỆM PHÁP LÝ VỀ VIỆC TUÂN THỦ CÁC QUY TẮC GDPR NHƯ TRÊN. VIỆC KHÔNG TUÂN THỦ CÁC QUY TẮC NÀY CÓ THỂ KHIẾN BẠN CÓ NGUY CƠ PHẢI TRẢ CÁC KHOẢN TIỀN TỆ HOẶC CÁC HÌNH PHẠT KHÁC BỞI CÁC BÊN CỦA LIÊN MINH CHÂU ÂU, CŨNG NHƯ XÓA TÀI KHOẢN Fuda CỦA BẠN.",
  "Advisor Tax Compliance": "TUÂN THỦ THUẾ",
  "As an Advisor, You may be required to provide Us with completed tax forms as prescribed by law, such as a Form W-9 or Form W-8BEN. If so required, your failure to submit such forms may result in delay of payment to You and/or the termination of your account with Us. As applicable and if required by law, You will be sent a Form 1099 for You to comply with your income tax obligations. Remember, You are solely responsible for reporting, paying, and filing any and all income, taxes, and documents associated with any compensation You receive through your use of the Service.": 
  "Với tư cách là Cố vấn, Bạn có thể được yêu cầu cung cấp cho Chúng tôi các biểu mẫu thuế đã hoàn chỉnh theo quy định của pháp luật, chẳng hạn như Mẫu W-9 hoặc Mẫu W-8BEN. Nếu được yêu cầu như vậy, việc bạn không gửi các biểu mẫu đó có thể dẫn đến việc chậm thanh toán cho Bạn hoặc chấm dứt dịch vụ tài khoản của bạn với Chúng tôi. Nếu có và nếu luật pháp yêu cầu, Bạn sẽ được gửi Biểu mẫu 1099 để Bạn tuân thủ các nghĩa vụ thuế thu nhập của mình. Hãy nhớ rằng, Bạn hoàn toàn chịu trách nhiệm về việc báo cáo, thanh toán và nộp bất kỳ thông tin về thu nhập, thuế và tài liệu liên quan đến bất kỳ khoản bồi thường nào Bạn nhận được thông qua việc sử dụng Dịch vụ.",
  "Waiver & Release": "MIỄN TRỪ TRÁCH NHIỆM",
  "You acknowledge and understand that, as an Advisor, You will be solely responsible and liable for any damages and/or claims suffered by or asserted by a client in connection with services that You provided.": 
  "Với tư cách là Cố vấn, Bạn sẽ tự chịu trách nhiệm đối với bất kỳ thiệt hại hoặc khiếu nại mà khách hàng phản ánh liên quan đến các dịch vụ mà Bạn đã cung cấp.",
  "IN THE EVENT OF YOU ARE INVOLVED IN A DISPUTE REGARDING ANY CLIENT OR TRANSACTION, YOU HEREBY RELEASE BARGES AND ITS AFFILIATES, AND THEIR RESPECTIVE OFFICERS, DIRECTORS, SHAREHOLDERS, EMPLOYEES, REPRESENTATIVES AND AGENTS FROM ALL MANNER OF ACTIONS, CLAIMS OR DEMANDS AND FROM ANY AND ALL LOSSES (DIRECT, INDIRECT, INCIDENTAL OR CONSEQUENTIAL), DAMAGES, COSTS OR EXPENSES, INCLUDING, WITHOUT LIMITATION, COURT COSTS AND ATTORNEY'S FEES.": 
  "TRONG TRƯỜNG HỢP CÓ TRANH CHẤP LIÊN QUAN ĐẾN BẤT KỲ KHÁCH HÀNG HOẶC GIAO DỊCH NÀO, BẠN SẼ KHÔNG LIÊN QUAN ĐẾN BARGES VÀ CÁC CHI NHÁNH, VĂN PHÒNG ĐẠI DIỆN, GIÁM ĐỐC, CÁC BÊN ĐẦU TƯ, NH N VIÊN, ĐẠI DIỆN VÀ CÁC ĐẠI LÝ, BAO GỒM TẤT CẢ CÁC HÀNH ĐỘNG, PHẢN ÁNH HOẶC NHU CẦU VÀ TỪ BẤT KỲ THIỆT HẠI (TRỰC TIẾP, GIÁN TIẾP, VÔ TÌNH, CỐ Ý), GIÁ HOẶC CÁC CHI PHÍ KHÁC.",
  "Indemnification": "BỒI THƯỜNG",
  "You agree to defend, indemnify and hold harmless Barges Technologies, Inc. and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of or in connection with: (i) your use and access of the Service, by You or any person using your account and password, including, but not limited to, any interaction or communications with another user, client, advisor, or third party; (ii) a violation or breach of the Terms of Use, the Advisor Terms, or any other agreement that governs your use of the Service; (iv) a violation of any of your representations or warranties made to Us, (v) a violation of any law or the rights of any third party; or (vi) any user Content, third party content, third party sites and any other content posted on the Service.": 
  "Bạn đồng ý bảo vệ, bồi thường và giữ vô hại Barges Technologies, Inc. và người cấp phép và người cấp phép của nó, và nhân viên, nhà thầu, đại lý, cán bộ và giám đốc của họ, khỏi và chống lại bất kỳ và tất cả các khiếu nại, thiệt hại, nghĩa vụ, tổn thất, trách nhiệm pháp lý, chi phí hoặc nợ và các chi phí (bao gồm nhưng không giới hạn ở phí luật sư), do hoặc phát sinh từ hoặc liên quan đến: (i) việc bạn sử dụng và truy cập Dịch vụ, bởi Bạn hoặc bất kỳ người nào sử dụng tài khoản và mật khẩu của bạn; (ii) vi phạm hoặc vi phạm các Điều khoản này, Điều khoản và Điều kiện của Cố vấn, hoặc bất kỳ thỏa thuận nào khác điều chỉnh việc sử dụng Dịch vụ của bạn; (iv) vi phạm bất kỳ tuyên bố hoặc bảo đảm nào của bạn đối với Chúng tôi, (v) vi phạm bất kỳ luật nào hoặc quyền của bất kỳ bên thứ ba nào; hoặc (vi) bất kỳ Nội dung người dùng nào, nội dung của bên thứ ba.",
  "Limitation of Liability": "TRÁCH NHIỆM PHÁP LÝ",
  "In no event shall Barges, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any user, client, advisor, or third party on the Service; (iii) any Content obtained from or provided to the Service; (iv) reliance by any other person or entity on your recommendations or advice or the actions You take; and (v) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not We have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.": 
  "Trong mọi trường hợp, Barges cũng như giám đốc, nhân viên, đối tác, đại lý, nhà cung cấp hoặc chi nhánh của nó sẽ không phải chịu trách nhiệm pháp lý đối với bất kỳ thiệt hại do hậu quả của bạn gây ra hoặc các khoản bồi thường, bao gồm không giới hạn, mất lợi nhuận, dữ liệu,...hoặc những tổn thất vô hình khác, do  (i) bạn truy cập hoặc sử dụng hoặc không thể truy cập hoặc sử dụng Dịch vụ; (ii) bất kỳ hành vi hoặc nội dung nào của bất kỳ bên thứ ba nào trên Dịch vụ; ( iii) bất kỳ Nội dung nào có được từ hoặc được cung cấp cho Dịch vụ; (iv) theo dõi hoặc dựa trên các khuyến nghị hoặc lời khuyên của Cố vấn hoặc các hành động mà Bạn thực hiện dựa trên những điều tương tự; và (v) truy cập trái phép, sử dụng hoặc thay đổi việc truyền hoặc nội dung của bạn, cho dù dựa trên bảo hành, hợp đồng, sai phạm (bao gồm cả sơ suất) hoặc bất kỳ lý thuyết pháp lý nào khác.",
  "Barges is not responsible for (i) any Content or conduct, whether online or offline, made in connection with the Service, whether caused by visitors, users, Advisors, third parties and others; (ii) any error, omission, interruption, deletion, defect, delay in operation or transmission, communication lines failure, theft or destruction or unauthorized access to, or alteration of user communications or Advisor communications; (iii) any problems or technical malfunction of any telephone network or lines, cellular data provider or network, computer online systems, servers or providers, computer equipment, software, failure of email, traffic congestion or downtime on the Service, including injury or damage to users or to any other person's computer and/or mobile device; or (iv) any loss or damage, including personal injury or death, resulting from anyone's use of the Service, any content posted or transmitted to users, or any interactions between users of the Service, whether online or offline.": 
  "Barges không chịu trách nhiệm về (i) bất kỳ Nội dung hoặc hành vi nào, dù trực tuyến hay ngoại tuyến, được thực hiện liên quan đến Dịch vụ, cho dù do khách truy cập, người dùng, Cố vấn, bên thứ ba và những người khác gây ra; (ii) bất kỳ lỗi nào, thiếu sót, gián đoạn, xóa, khiếm khuyết, chậm trễ trong vận hành hoặc truyền tải, lỗi đường truyền liên lạc, trộm cắp hoặc phá hủy hoặc truy cập trái phép, hoặc thay đổi thông tin liên lạc của người dùng hoặc thông tin liên lạc của Cố vấn; (iii) bất kỳ sự cố hoặc trục trặc kỹ thuật nào của bất kỳ đường dây mạng, nhà cung cấp dữ liệu di động, hệ thống máy tính trực tuyến, máy chủ hoặc nhà cung cấp, thiết bị máy tính, phần mềm, lỗi email, tắc nghẽn giao thông hoặc thời gian ngừng hoạt động trên Dịch vụ, bao gồm cả thương tích hoặc thiệt hại cho người dùng hoặc cho bất kỳ máy tính và / hoặc thiết bị di động nào của người khác; hoặc (iv) bất kỳ tổn thất hoặc thiệt hại nào, bao gồm thương tật cá nhân hoặc tử vong do bất kỳ ai sử dụng Dịch vụ, bất kỳ nội dung nào được đăng tải đến người dùng hoặc bất kỳ tương tác nào giữa những người dùng Dịch vụ, cho dù trực tuyến hay ngoại tuyến.",
  "Governing Law": "LUẬT QUẢN TRỊ",
  "You expressly agree that these Advisor Terms and any dispute arising out of these Advisor Terms or use of the Service shall be governed, construed, and enforced in accordance with the laws of New York, United States, without regard to its conflict of law provisions and excluding the United Nations Convention on Contracts for the International Sale of Goods (CISG). You further agree and consent to the exclusive personal and subject matter jurisdiction and venue of the state and federal courts located in the Bergen County, New Jersey, for the resolution of any such dispute and You agree and submit to personal jurisdiction in such courts. In addition, you forever waive any argument or defense based on personal jurisdiction, venue, or forum non conveniens.": 
  "Bạn hoàn toàn đồng ý rằng các Điều khoản của Advisor này và bất kỳ tranh chấp nào phát sinh từ các Điều khoản của Advisor  hoặc việc sử dụng Dịch vụ sẽ được điều chỉnh, giải thích và thực thi theo luật của New York, Hoa Kỳ, bất kể xung đột với các điều khoản luật của nó, ngoại trừ Công ước của Liên hợp quốc về Hợp đồng Mua bán Hàng hóa Quốc tế (CISG). Bạn cũng đồng ý với quyền tài phán và địa điểm xét xử cá nhân và chủ đề độc quyền của các tòa án liên bang và tiểu bang đặt tại Quận Bergen, New Jersey, để giải quyết bất kỳ tranh chấp nào như vậy. Bạn đồng ý và tuân theo quyền tài phán cá nhân tại các tòa án đó. Ngoài ra, bạn chấp nhận từ bỏ mọi tranh luận hoặc biện hộ dựa trên quyền tài phán cá nhân, địa điểm hoặc diễn đàn không tiện lợi.",
  "Changes": "THAY ĐỔI",
  "We reserve the right, at Our sole discretion, to modify or replace these Advisor Terms at any time, without notification. The latest Advisor Terms will be posted on Our website. It is your responsibility to review the latest Advisor Terms before You use the Service. By continuing to access or use Our Service after new Advisor Terms become effective, You agree to be bound by the new Advisor Terms. If You do not agree to the new Advisor Terms, please stop using the Service.": 
  "Chúng tôi bảo lưu quyền, theo quyết định riêng của Chúng tôi, sửa đổi hoặc thay thế các Điều khoản Cố vấn này bất kỳ lúc nào mà không cần thông báo. Các Điều khoản mới nhất của Cố vấn sẽ được đăng trên trang web của Chúng tôi. Bạn có trách nhiệm xem lại các Điều khoản mới nhất của Cố vấn trước khi Bạn sử dụng Dịch vụ. Bằng cách tiếp tục truy cập hoặc sử dụng Dịch vụ của Chúng tôi sau khi Điều khoản Cố vấn mới có hiệu lực, Bạn đồng ý bị ràng buộc bởi Điều khoản Cố vấn mới. Nếu Bạn không đồng ý với Điều khoản mới của Cố vấn, vui lòng ngừng sử dụng Dịch vụ.",
  "THIS IS A LEGAL AGREEMENT BETWEEN YOU AND BARGES TECHNOLOGIES, INC. BY CLICKING ON THE “CONTINUE” OR “SAVE” BUTTON ON THE ADVISOR SIGNUP PAGE YOU ARE AFFIRMATIVELY STATING THAT YOU HAVE READ AND UNDERSTAND THE ADVISOR TERMS SET FORTH HEREIN AND ARE AFFIRMATIVELY INDICATING YOUR ACCEPTANCE OF THIS AGREEMENT AND YOU AGREE TO BE BOUND BY THE ADVISOR TERMS HEREOF.": 
  "ĐÂY LÀ THỎA THUẬN PHÁP LÝ GIỮA BẠN VÀ CÔNG NGHỆ BARGES, INC. BẰNG CÁCH NHẤP VÀO NÚT “TIẾP TỤC” HOẶC “LƯU” TRÊN TRANG ĐĂNG KÝ TƯ VẤN. BẠN XÁC NHẬN RẰNG BẠN ĐÃ ĐỌC VÀ HIỂU CÁC ĐIỀU KHOẢN QUẢNG CÁO ĐƯỢC THIẾT LẬP TẠI ĐÂY VÀ CÓ LIÊN QUAN BẠN CHẤP NHẬN THỎA THUẬN NÀY VÀ BẠN ĐỒNG Ý VỚI NHỮNG THAY ĐỔI TRONG ĐIỀU KHOẢN CỦA CỐ VẤN.",

  // Page AdvisorComponent
  "No reviews" : "Chưa có đánh giá",
  "Total order:" : "Tổng số đơn hàng:",
  "About" : "Giới thiệu",
  "Years of Experience:" : "Năm kinh nghiệm: ",
  "years" : "năm",
  "Experience" : "Số năm kinh nghiệm",
  "Certificate" : "Chứng chỉ",
  "Category" : "Thể loại",
  "Order instruction" : "Yêu cầu đối với khách hàng",
  "Language" : "Ngôn ngữ",
  "English" : "Tiếng anh",
  "Review" : "Đánh giá",
  "Not have Review" : "Chưa có đánh giá",

  // Page AdvisorCategoryComponent
  "Advisor list is empty!": "Danh sách chuyên gia trống!",

  // Page DashboardComponent
  "Username": "Tên đăng nhập",
  "Full name": "Họ và tên",
  "Address": "Địa chỉ",
  "Phone number": "Số điện thoại",
  "Birthday": "Ngày sinh",
  "Choose Month": "Chọn tháng",
  "Choose Day": "Chọn ngày",
  "Job": "Công việc",
  "Department": "Phòng ban",
  "About you": "Thông tin",
  "Country": "Quốc gia",
  "Top Advisors": "Chuyên gia hàng đầu",
  "My Services": "Dịch vụ của tôi",

  // Page article/_slug
  "Home": "Trang chủ",
  "Article": "Bài viết",

  // Page article/index
  "Article is empty!": "Không có bài viết nào!",

  // Page NeedHelpComponent
  "Frequently Asked Questions": "Câu hỏi thường gặp",
  "Need Help is empty!": "Câu hỏi thường gặp trống!"
};
