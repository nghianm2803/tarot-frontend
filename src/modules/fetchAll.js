export default async function (fetches) {
	try{
        let ajax_func = [];
        for (let j = 0; j < fetches.length; j++) {
            ajax_func.push(fetches[j].function);
        }
        let result = await Promise.all(ajax_func);
        for (let i = 0; i < fetches.length; i++) {
            if (fetches[i].success) {
                fetches[i].success(result[i]);
            }
        }
        return result;
    } catch(error) {
        for (let i = 0; i < fetches.length; i++) {
            if (fetches[i].error) {
                fetches[i].error(error);
            }
        }
    }
}