let dataText = [
  {
    id: "60",
    name: "Công việc",
    value: "Công việc",
  },
  {
    id: "61",
    name: "Tình cảm",
    value: "Tình cảm",
  },
  {
    id: "62",
    name: "Gia đình",
    value: "Gia đình",
  },
  {
    id: "63",
    name: "Học tập",
    value: "Học tập",
  },
  {
    id: "64",
    name: "Tài chính",
    value: "Tài chính",
  },
  {
    id: "65",
    name: "Chia tay và ly hôn",
    value: "Chia tay và ly hôn",
  },
  {
    id: "66",
    name: "Tiên tri tương lai",
    value: "Tiên tri tương lai",
  },
  {
    id: "67",
    name: "Giải mã giấc mơ",
    value: "Giải mã giấc mơ",
  },
  {
    id: "68",
    name: "Phát triển bản thân",
    value: "Phát triển bản thân",
  },
  {
    id: "69",
    name: "Phần âm",
    value: "Phần âm",
  },
  {
    id: "70",
    name: "Sức khoẻ",
    value: "Sức khoẻ",
  },
  {
    id: "238",
    name: "Thế giới vô hình",
    value: "Thế giới vô hình",
  },
];

class TopicClass {
  getTopic = () => {
    return dataText;
  };
}

export const TopicModule = new TopicClass();

