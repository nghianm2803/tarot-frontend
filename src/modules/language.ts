export default function (text: string, lang: string) {
	if (lang == 'en') {
        return text;
    } else {
    	try {
    		var language = require('~/modules/language/'+lang+'.js');
            if (language[text]) {
		        return language[text].toString();
            } else {
                return text;
            }
    	} catch (error) {
    		return text;
    	}
    }
}
