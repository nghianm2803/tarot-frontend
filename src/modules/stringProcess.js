class stringProcessClass {
  addCommas = (n) => {
    var rx = /(\d+)(\d{3})/;
    return String(n).replace(/^\d+/, function (w) {
      while (rx.test(w)) {
        w = w.replace(rx, '$1,$2');
      }
      return w;
    });
  }
  getMonths = () => {
    return {
      '1': '01',
      '2': '02',
      '3': '03',
      '4': '04',
      '5': '05',
      '6': '06',
      '7': '07',
      '8': '08',
      '9': '09',
      '10': '10',
      '11': '11',
      '12': '12',
    };
  }
  getGender = () => {
    return {
      'female': 'Nữ',
      'male': 'Nam',
      'none': 'Không xác định'
    }
  }
  getDays = () => {
    return {
      '1': '01',
      '2': '02',
      '3': '03',
      '4': '04',
      '5': '05',
      '6': '06',
      '7': '07',
      '8': '08',
      '9': '09',
      '10': '10',
      '11': '11',
      '12': '12',
      '13': '13',
      '14': '14',
      '15': '15',
      '16': '16',
      '17': '17',
      '18': '18',
      '19': '19',
      '20': '20',
      '21': '21',
      '22': '22',
      '23': '23',
      '24': '24',
      '25': '25',
      '26': '26',
      '27': '27',
      '28': '28',
      '29': '29',
      '30': '30',
      '31': '31'
    };
  }
  getTimeString = (arrayTime, lang) => {
    var number = arrayTime.number;
    var string = arrayTime.text;
    if (number == 0) {
      return this.language("A few seconds", lang);
    } else {
      if (number == 1) {
        return number + ' ' + this.language(string, lang);
      } else {
        return number + ' ' + this.language(string + 's', lang);
      }
    }
  }
  language = (text, lang) => {
    if (lang == 'en') {
      return text;
    } else {
      try {
        var language = require('~/modules/language/' + lang + '.js');
        if (language[text]) {
          return language[text];
        } else {
          return text;
        }
      } catch (error) {
        return text;
      }
    }
  }
  truncateString = (string, length, ending) => {
    if (length == null) {
      length = 100;
    }
    if (ending == null) {
      ending = '...';
    }
    if (string.length > length) {
      return string.substring(0, length - ending.length) + ending;
    } else {
      return string;
    }
  }

  getSecondString = (remainingSec, lang) => {
    if (remainingSec < 5) {
      remaining = this.language("Just now", lang);
    } else if (remainingSec < 60) {
      remaining = remainingSec;
      if (remaining == 1) {
        return remaining + ' ' + this.language('second', lang);
      } else {
        return remaining + ' ' + this.language('seconds', lang);
      }
    } else if (remainingSec < 3600) {
      remaining = Math.floor(remainingSec / 60);
      if (remaining == 1) {
        return remaining + ' ' + this.language('minute', lang);
      } else {
        return remaining + ' ' + this.language('minutes', lang);
      }
    } else if (remainingSec < 86400) {
      remaining = Math.floor(remainingSec / 3600);
      if (remaining == 1) {
        return remaining + ' ' + this.language('hour', lang);
      } else {
        return remaining + ' ' + this.language('hours', lang);
      }
    } else if (remainingSec < 2592000) {
      remaining = Math.floor(remainingSec / 86400);
      if (remaining == 1) {
        return remaining + ' ' + this.language('day', lang);
      } else {
        return remaining + ' ' + this.language('days', lang);
      }
    } else if (remainingSec < 31556926) {
      remaining = Math.floor(remainingSec / 2592000);
      if (remaining == 1) {
        return remaining + ' ' + this.language('month', lang);
      } else {
        return remaining + ' ' + this.language('months', lang);
      }
    } else {
      remaining = Math.floor(remainingSec / 31556926);
      if (remaining == 1) {
        return remaining + ' ' + this.language('year', lang);
      } else {
        return remaining + ' ' + this.language('years', lang);
      }
    }
    return '';
  }
}
export const stringProcess =  new stringProcessClass();
