let dataText = [
  {
    id: "1",
    name: "Oracle Guidance",
    value: "Oracle Guidance",
  },
  {
    id: "2",
    name: "Palm Readings",
    value: "Palm Readings",
  },
  {
    id: "3",
    name: "Angel Insights",
    value: "Angel Insights",
  },
  {
    id: "4",
    name: "Clairvoyance",
    value: "Clairvoyance",
  },
  {
    id: "7",
    name: "Life Coaching",
    value: "Life Coaching",
  },
  {
    id: "9",
    name: "Psychic Readings",
    value: "Psychic Readings",
  },
  {
    id: "10",
    name: "Tarot Readings",
    value: "Tarot Readings",
  },
  {
    id: "11",
    name: "Astrology & Horoscopes",
    value: "Astrology & Horoscopes",
  },
  {
    id: "21",
    name: "Numerology",
    value: "Numerology",
  },
  {
    id: "42",
    name: "I-ching",
    value: "I-ching",
  },
  {
    id: "43",
    name: "Feng Shui",
    value: "Feng Shui",
  },
  {
    id: "44",
    name: "Pendulum",
    value: "Pendulum",
  },
  {
    id: "240",
    name: "Rune Stone",
    value: "Rune Stone",
  },
];

class CategoryClass {
  getCategories = () => {
    return dataText;
  };
}

export const CategoryForeignerModule = new CategoryClass();

