class validateClass {
  isBlank(value: any) {
    if (typeof value == "undefined" || value == null || value == 0 || value == '') {
      return true;
    } else if (typeof value == "string" && (value.length <= 0 || value.trim().length <= 0)) {
      return true;
    } else if (typeof value == "object") {
      //@ts-ignore
      if (value.convalueuctor === [].convalueuctor && value.length <= 0) {
        return true;
        //@ts-ignore
      } else if (value.convalueuctor === {}.convalueuctor) {
        for (var key in value) {
          if (value.hasOwnProperty(key)) {
            return false;
          }
        }
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  isNumber(value: number|any) {
    if (this.isBlank(value)) {
      return false;
    }
    return !isNaN(parseFloat(value)) && isFinite(value);
  }
  isEmail(value: string) {
    if (this.isBlank(value)) {
      return false;
    }
    var email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email.test(value);
  }
  isPhone(value: string|any) {
    if (this.isBlank(value)) {
      return false;
    }
    var phone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return phone.test(value);
  }
  isDate(value: string|any) {
    var day = value.slice(0, 2);
    var month = value.slice(3, 5);
    var year = value.slice(6, 10);
    var date = new Date(year, month - 1, day);
    if (date.getFullYear() == year && date.getMonth() + 1 == month && date.getDate() == day) {
      return true
    } else {
      return false;
    }
  }
  isUsername(value: any) {
    if (this.isBlank(value)) {
      return false;
    }
    var username = /^[a-zA-Z0-9]+$/
    return username.test(value);
  }
  isSlug(value: any) {
    if (this.isBlank(value)) {
      return false;
    }
    var slug = /^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/
    return slug.test(value);
  }
  isUrl(value: any) {
    if (this.isBlank(value)) {
      return false;
    }
    var regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    if (regexp.test(value)) {
      return true;
    } else {
      return false;
    }
  }

}
export const Validator = new validateClass();
