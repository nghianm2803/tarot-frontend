export default function (string) {
    if (string == undefined || string == null || string == '') {
        return string;
    }
    string = string.toString().trim();
    if (string == '') {
        return string;
    }
    string = string.replace(/\\/g, '\\\\'); // escape backslashes
    string = string.replace(/"/g, '\\"');   // escape quotes
    string = string.replace(/'/g, "\\'");   // escape quotes
    
    return string;
}