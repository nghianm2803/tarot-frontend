let dataText = [
  {
    id: "23",
    name: "Kinh dịch",
    value: "Kinh dịch",
  },
  {
    id: "24",
    name: "Tử vi",
    value: "Tử vi",
  },
  {
    id: "25",
    name: "Tarot",
    value: "Tarot",
  },
  {
    id: "26",
    name: "Oracle",
    value: "Oracle",
  },
  {
    id: "27",
    name: "Lenormand",
    value: "Lenormand",
  },
  {
    id: "28",
    name: "Thần số học",
    value: "Thần số học",
  },
  {
    id: "29",
    name: "Xem chỉ tay",
    value: "Xem chỉ tay",
  },
  {
    id: "30",
    name: "Bản đồ sao",
    value: "Bản đồ sao",
  },
  {
    id: "31",
    name: "Tứ trụ bát tự",
    value: "Tứ trụ bát tự",
  },
  {
    id: "40",
    name: "Sinh trắc vân tay",
    value: "Sinh trắc vân tay",
  },
  {
    id: "41",
    name: "Phong thuỷ",
    value: "Phong thuỷ",
  },
  {
    id: "45",
    name: "Lắc cảm xạ",
    value: "Lắc cảm xạ",
  },
  {
    id: "46",
    name: "Thấu thị",
    value: "Thấu thị",
  },
  {
    id: "47",
    name: "Khai vấn",
    value: "Khai vấn",
  },
  {
    id: "71",
    name: "Kỳ môn độn giáp",
    value: "Kỳ môn độn giáp",
  },
  {
    id: "72",
    name: "Bài tây",
    value: "Bài tây",
  },
  {
    id: "239",
    name: "Rune",
    value: "Rune",
  },
];

class CategoryClass {
  getCategories = () => {
    return dataText;
  };
}

export const CategoryModule = new CategoryClass();
