let dataText = [
    {
      name: "Dream Analysis",
      value: "Dream Analysis",
    },
    {
      name: "Career Advice",
      value: "Career Advice",
    },
    {
      name: "Love & Relationships",
      value: "Love & Relationships",
    },
    {
      name: "Predict Future",
      value: "Predict Future",
    },
    {
      name: "Study Advice",
      value: "Study Advice",
    },
    {
      name: "Finance Advice",
      value: "Finance Advice",
    },
    {
      name: "Breakup and Divorce",
      value: "Breakup and Divorce",
    },
    {
      name: "Self Growth",
      value: "Self Growth",
    },
  ];
  
  class TopicClass {
    getTopic = () => {
      return dataText;
    };
  }
  
  export const TopicForeignerModule = new TopicClass();
  
  
  