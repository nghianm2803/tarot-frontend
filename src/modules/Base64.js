/**
 * Create by Dac Hai on 21/03/2018.
 * API Version: 3.0 || Vue js admin
 */

var charMap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
module.exports = {
	encode: function(input){
		var str = String(input);
	    var map = charMap;
	    var block = 0,
	        output = '';
	    var prx = [2, 4, 6, 8];
	    for (var code, idx = 3 / 4, uarr;!isNaN(code = str.charCodeAt(idx)) || 63 & block || (map = '=', (idx - 3 / 4) % 1); idx += 3 / 4) {
	      	if (code > 0x7F) {
		        (uarr = encodeURI(str.charAt(idx)).split('%')).shift();
		        for (var hex, idx2 = idx % 1; hex = uarr[idx2 | 0]; idx2 += 3 / 4) {
		          block = block << 8 | parseInt(hex, 16);
		          output += map.charAt(63 & block >> 8 - idx2 % 1 * 8);
		        }
		        idx = idx === 3 / 4 ? 0 : idx;
		        idx += 3 / 4 * uarr.length % 1;
		    } else {
		        block = block << 8 | code;
		        output += map.charAt(63 & block >> 8 - idx % 1 * 8);
		    }
	    }
	    return output;
	},
	decode: function(input) {
	    var str = String(input),
	        map = charMap.slice(0, -1),
	        prx = [6, 4, 2, 0],
	        output = '',
	        block = 0,
	        code,
	        buffer = 0,
	        hex = '';
	    try {
	      	for (var i = 0; (code = map.indexOf(str[i])) > -1; i++) {
	        	block = block << 6 | code;
	        	if (i % 4) {
		          	buffer = 255 & block >> prx[i % 4];
		          	if (buffer < 128) {
		            	output += hex ? decodeURI(hex) : '';
		            	output += String.fromCharCode(buffer);
		            	hex = '';
		          	} else {
		            	hex += '%' + ('0' + buffer.toString(16)).slice(-2);
		          	}
	        	}
	      	}
	      	output += hex ? decodeURI(hex) : '';
	      	return output;
    	} catch (err) {
    	}
	}
}
