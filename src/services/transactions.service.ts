import { axiosGetInstanceBackend } from "../helpers/axios-config";

class TransactionsClass {
  getTransactions = async (page: number, limit: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataGet = `/transactions?page=${page}&limit=${limit}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + dataGet, config);
  }
}

export const transactionsService =  new TransactionsClass();
