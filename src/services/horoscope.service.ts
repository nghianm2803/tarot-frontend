import { axiosGetInstanceBackend, axiosPostInstanceBackend } from "../helpers/axios-config";

interface DataFilterDto extends URLSearchParams {
  page: number;
  limit: number;
  horoscope_date?: string;
  horoscope_week?: string;
  horoscope_month?: string;
  horoscope_zodiac?: string;
  orderBy?: "ASC"|"DESC";
  language?: string
}

class horoscopeClass {
  getHoroscope = async (dataFilter: DataFilterDto) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    let dataObject = new URLSearchParams(dataFilter).toString();
    return await axiosGetInstanceBackend(process.env.chatBackendUrl + "/horoscope/list?" + dataObject, config);
  };
}

export const horoscopeService = new horoscopeClass();
