import { axiosGetInstanceBackend, axiosPostInstanceBackend } from "../helpers/axios-config";


interface ContactFormDto extends URLSearchParams {
  contactform_name: string;
  contactform_email: string;
  contactform_content: string;
}

class ContactFormClass {
  createContactForm = async (dataContactForm: ContactFormDto, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataContactForm);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + '/contactform', params, config);
  }
  getContactFormByName = async (authCode: string, name: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + `/contactform?contactform_name=${name}`, config);
  }
}

export const contactFormService = new ContactFormClass();
