import { axiosGetInstanceBackend, axiosPostInstanceBackend } from "../helpers/axios-config";

class userServiceClass {
  getUserService = async (userId: number) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    let urlGet = `/service?createBy=${userId}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + urlGet, config);
  };
  getOnlineUser = async () => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    let urlGet = `/chat-user/list?limit=100&page=1&userRole=advisor&userActive=1`;
    return await axiosGetInstanceBackend(process.env.chatBackendUrl + urlGet, config);
  }
}

export const userServiceService = new userServiceClass();
