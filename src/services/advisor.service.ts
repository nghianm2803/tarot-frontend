import { searchAdvisor } from "../helpers/advisor.helper";
import { axiosGetInstanceBackend, axiosPatchInstanceBackend } from "../helpers/axios-config";

interface UpdateUserDto extends URLSearchParams {
  user_avatar?: string
  display_name?: string
  user_address?: string
  user_nation?: string
  user_gender?: string
  user_numberphone?: string
  user_login?: string
  user_job?: string
  user_department?: string
  user_birthday?: string
  bio?: string
}
interface GetAllAdvisorDto extends URLSearchParams {
  page?: number
  limit?: number
  user_role?: string
  display_name?: string
  ids?: string
  user_nation?: string
}
class advisorClass {
  getNewAdvisor = async (page: number, limit: number) => {
    return await searchAdvisor.getNewAdvisor(page, limit).then((response: any) => {
      if (response && response.data) {
        return response.data;
      } else {
        return [];
      }
    }).catch((error: any) => {
      return error;
    });
  }

  getAllAdvisor = async (dataFilter: GetAllAdvisorDto) => {
    let dataObject = new URLSearchParams(dataFilter).toString();
    let dataUrl = process.env.mainBackendUrl + `/users?${dataObject}`;
    return await axiosGetInstanceBackend(dataUrl, {});
  }

  getAdvisorDetail = async (slug: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + '/user/'+  slug, config);
  }
  updateUser = async(dataUpdate: UpdateUserDto, userToken: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": userToken,
      },
    };
    const params = new URLSearchParams(dataUpdate);
    return await axiosPatchInstanceBackend(process.env.mainBackendUrl + "/user", params, config);
  }
}

export const advisorService =  new advisorClass();
