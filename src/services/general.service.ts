import { axiosGetInstanceBackend, axiosPatchInstanceBackend, axiosPostInstanceBackend } from "../helpers/axios-config";

interface DataServiceDto extends URLSearchParams {
  service_id: number,
  service_active: Number,
  service_name: string,
  service_description: string,
  service_price: Number,
  service_duration: Number,
  service_unit: string,
  service_category: string,
  service_thumbnail: string,
  createBy: number,
  service_group: string,

}
class ServiceClass {
  getGeneralServiceByUserId = async (authCode: string, createBy: number) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let dataGet = `/service?createBy=${createBy}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + dataGet, config);
  }
  updateGeneralService = async (authCode: string, dataUpdate: DataServiceDto) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataUpdate);
    return await axiosPatchInstanceBackend(process.env.mainBackendUrl + `/service/${dataUpdate.service_id}`, params, config);
  }
  // Create premium service
  createPremiumService = async (authCode: string, dataUpdate: DataServiceDto) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataUpdate);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + '/service/', params, config);
  }
}

export const generalService = new ServiceClass();
