import { axiosGetInstanceBackend, axiosPostInstanceBackend } from "../helpers/axios-config";

interface DataFilterDto extends URLSearchParams {
  page: number;
  limit: number;
  orderBy?: "ASC"|"DESC";
  language?: string
}

class needHelpClass {
  getDataNeedHelp = async (dataFilter: DataFilterDto) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    let dataObject = new URLSearchParams(dataFilter).toString();
    return await axiosGetInstanceBackend(process.env.chatBackendUrl + "/need-help/list?" + dataObject, config);
  };
}

export const needHelpService = new needHelpClass();
