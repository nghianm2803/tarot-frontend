import { axiosGetInstanceBackend, axiosPostInstanceBackend, axiosPatchInstanceBackend, axiosPutInstanceBackend } from "../helpers/axios-config";

class s3ServiceClass {
  createUserMeta = async (urlPut: string, contentType: string, dataPut: any) => {
    const config = {
      headers: {
        "Content-Type": contentType
      },
    };
    return await axiosPutInstanceBackend(urlPut, dataPut, config);
  }
}

export const s3Service = new s3ServiceClass();
