import {
  axiosGetInstanceBackend,
  axiosPostInstanceBackend,
  axiosPatchInstanceBackend,
  axiosDeleteInstanceBackend,
} from "../helpers/axios-config";
interface CreateUser extends URLSearchParams {
  meta_key: string;
  meta_value: string;
  is_private: number;
}

class userMetaClass {
  /**
   * @param userId
   * @param authCode
   * @returns
   */
  getUserMeta = async (userId: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let urlGet = `/user_meta/${userId}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + urlGet, config);
  };
  /**
   *
   * @param userId
   * @param authCode
   * @param metaKey
   * @returns
   */
  getUserMetaByMetaKey = async (userId: number, authCode: string, metaKey: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let urlGet = `/user_meta/${userId}?meta_key=${metaKey}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + urlGet, config);
  };
  updateUserMeta = async (userId: number, metaId: number, authCode: string, dataUpdate: CreateUser) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataUpdate);
    return await axiosPatchInstanceBackend(
      process.env.mainBackendUrl + `/user_meta/${userId}/${metaId}`,
      params,
      config
    );
  };

  // Delete certificate
  deleteUserMeta = async (metaId: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let urlRemove = `/user_meta/${metaId}`;
    console.log(authCode, "authCode");
    return await axiosDeleteInstanceBackend(process.env.mainBackendUrl + `/user_meta/${metaId}`, urlRemove, config);
  };

  createUserMeta = async (userId: number, authCode: string, dataCreate: CreateUser) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataCreate);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + `/user_meta/${userId}`, params, config);
  };
}

export const userMetaService = new userMetaClass();
