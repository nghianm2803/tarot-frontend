import { topicHelper } from "../helpers/topic.helper";

class topicClass {
  getTopics = async (page: number, limit: number) => {
    return await topicHelper.getTopics(page, limit).then((response: any) => {
      if (response && response.data) {
        return response.data;
      } else {
        return [];
      }
    }).catch((error: any) => {
      return [];
    });
  }
}

export const topicService =  new topicClass();
