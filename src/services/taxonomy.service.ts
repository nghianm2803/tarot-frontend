import { axiosGetInstanceBackend, axiosPostInstanceBackend, axiosPatchInstanceBackend, axiosDeleteInstanceBackend } from "../helpers/axios-config";

interface TaxonomyRelationShipDto extends URLSearchParams {
  taxonomy_id: number
  object_id: number
}

/**
 * @class taxonomyServiceClass
 */
class taxonomyServiceClass {
  /**
   * @param type
   * @param authCode
   * @returns
   */
  getTaxonomyByType = async (type: string, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let urlGet = `/taxonomy?taxonomy_type=${type}&orderBy=taxonomy_id,desc`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + urlGet, config);
  };

  /**
   *
   * @param taxonomyId
   * @returns
   */
  getAdvisorByTaxonomy = async (taxonomyId: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": '',
      },
    };
    let urlGet = `/taxonomy_relationship/advisor_category?taxonomy_id=${taxonomyId}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + urlGet, config);

  }

  /**
   * @param dataAdd
   * @param type
   * @param authCode
   * @returns
   */
  addTaxonomyRelationship = async (dataAdd: TaxonomyRelationShipDto, type: string, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let urlAdd = `/taxonomy_relationship/${type}`;
    const params = new URLSearchParams(dataAdd);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + urlAdd, params, config);
  };

  /**
   * @param relationshipId
   * @param authCode
   * @returns
   */
  removeTaxonomyRelationship = async (relationshipId: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let urlRemove = `/taxonomy_relationship/${relationshipId}`;
    return await axiosDeleteInstanceBackend(process.env.mainBackendUrl + urlRemove, {}, config);
  }

  /**
   * @param userId
   * @param type
   * @param authCode
   * @returns
   */
  getTaxonomyRelationship = async (userId: number, type: string, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    let urlGet = `/taxonomy_relationship/${type}/${userId}`;
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + urlGet, config);
  };
}

export const taxonomyService = new taxonomyServiceClass();
