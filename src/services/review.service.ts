import { axiosGetInstanceBackend, axiosPostInstanceBackend } from "../helpers/axios-config";

interface ReviewDto extends URLSearchParams {
  page: number;
  limit: number;
  seller_id: number;
}


interface DataToUpdateDto extends URLSearchParams {
  order_id: number,
  review_content: string,
  review_point: number
}
class reviewClass {
  getReviewByUserId = async (dataFilter: ReviewDto) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    let dataObject = new URLSearchParams(dataFilter).toString();
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + "/review?" + dataObject, config);
  };
  getReviewByOrderId = async (orderId: number, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    return await axiosGetInstanceBackend(process.env.mainBackendUrl + "/review?order_id=" + orderId, config);
  }
  createOrderReview = async (dataCreate: DataToUpdateDto, authCode: string) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": authCode,
      },
    };
    const params = new URLSearchParams(dataCreate);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + `/review`, params, config);
  }
}

export const reviewService = new reviewClass();
