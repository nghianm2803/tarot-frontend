import { axiosGetInstanceBackend, axiosPostInstanceBackend } from "../helpers/axios-config";
interface LoginDto extends URLSearchParams {
  user_email: string
  password: string
  device_uuid: string
  device_signature: string
  remember: string
  device_type: string
}
class loginClass {
  login = async (dataLogin: any) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };

    return await axiosPostInstanceBackend(process.env.mainBackendUrl + "/login", dataLogin, config);
  };

  loginNew = async (dataLogin: LoginDto) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };

    const params = new URLSearchParams(dataLogin);
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + "/login", params, config);
  };

  register = async (dataLogin: any) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };

    return await axiosPostInstanceBackend(process.env.mainBackendUrl + "/register", dataLogin, config);
  };
  loginWithGoogle = async (dataLogin: any) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + "/login/google", dataLogin, config);
  };

  loginWithApple = async (dataLogin: any) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };
    return await axiosPostInstanceBackend(process.env.mainBackendUrl + "/login/apple", dataLogin, config);
  };

  logout = async (userToken: string) => {
    if (userToken) {
      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "X-Authorization": userToken,
        },
      };
      return await axiosGetInstanceBackend(process.env.mainBackendUrl + "/logout", config);
    }
  }

  loginWithFacebook = async (dataLogin: any) => {
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Authorization": "",
      },
    };

    return await axiosPostInstanceBackend(process.env.mainBackendUrl + "/login/facebook", dataLogin, config);
  };
}

export const loginService = new loginClass();
