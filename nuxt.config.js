var path = require('path');
const axios = require('axios');
var fs = require('fs');
const newYear = new Date().getFullYear();
module.exports = {
  srcDir: 'src/',
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: 'vi-VN',
    },
    title: `Fuda`,
    meta: [{
      charset: 'utf-8'
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    },
    {
      hid: 'description',
      name: 'description',
      content: `Fudaly World`
    },
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    },
    {
      href: 'https://fonts.googleapis.com/css?family=Noto+Serif+JP:400,700|Roboto+Slab:400,700|Roboto:100,300,400,700,900&amp;subset=vietnamese',
      rel: 'stylesheet'
    }
    ],
  },
  css: [
    {
      src: '~/assets/scss/global.scss',
      lang: 'scss'
    },
  ],
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#5E0D8D'
  },
  serverMiddleware: [
    { path: '/login', handler: '~/middleware/postRequestHandler.js' },
    { path: '/register-advisor', handler: '~/middleware/postRequestHandler.js' },
    { path: '/register-advisor-thank-you', handler: '~/middleware/postRequestHandler.js' },
    { path: '/signup', handler: '~/middleware/postRequestHandler.js' },
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, {
      isDev,
      isClient
    }) {
      if (isDev && isClient) {
      }
      vendor: [
        'axios',
      ]
    },
    loaders: {
      sass: {
        implementation: require('sass'),
      },
      scss: {
        implementation: require('sass'),
      }
    },
    uglify: {
      uglifyOptions: {
        compress: false
      },
      cache: '/var/www/tarot-frontend/cache'
    }
  },
  modules: [
    // With options
    ['@nuxtjs/axios', {
      credentials: false,
      proxyHeaders: false
    }],
    '@nuxtjs/firebase',
    '@nuxtjs/gtm',
  ],
  gtm: {
    id: 'G-7WD13RYZDC'
  },
  firebase: {
    config: {
      apiKey: "AIzaSyBij1Rhk-yZfPawCUxMtgseuCYHMPsmgtI",
      authDomain: "taki-tarot-mobile-app.firebaseapp.com",
      projectId: "taki-tarot-mobile-app",
      storageBucket: "taki-tarot-mobile-app.appspot.com",
      messagingSenderId: "521286800",
      appId: "1:521286800:web:8375a1a34daa396098d415",
      measurementId: "G-TNTZEP071N"
    },
    services: {
      messaging: {
        createServiceWorker: true,
        fcmPublicVapidKey: 'BAwIfJX616aBgjmQv3L9fHdvSgNW437KS0hsmQSDvlZFsLWed3IOJIvNjXXeANDBmJiV58PYY0Xa-GI2kHY24lw',
        inject: fs.readFileSync('./serviceWorker.js'),
      }
    },
  },

  plugins: [
    '~/plugins/boostrap.js',
    '~/plugins/axios.js',
    '~/plugins/nuxt-client-init.client.js',
    '~/plugins/vue-lightbox.js',
    {
      src: '~/plugins/ga.js',
      ssr: false
    },
    {
      src:  '~/plugins/socket.io.js',
      ssr: false
    },
  ],

  env: {
    gaCode: '',
    mainFrontendUrl: process.env.MAIN_FRONTEND_URL || 'https://fuda.appuni.io',
    mainBackendUrl: process.env.MAIN_BACKEND_URL || 'https://tarotapi.appuni.io/api',
    chatBackendUrl: process.env.CHAT_BACKEND_URL || 'https://chatdev.appuni.io/api',
    socketUrl: process.env.SOCKET_URL || 'https://chatdev.appuni.io/socket',
    PORT: process.env.MAIN_PORT || 80
  },
  buildModules: ['@nuxt/typescript-build'],
}
