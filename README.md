## This source use Nuxtjs Framework
## Website and documentation of [Nuxt](https://nuxtjs.org)

We are using [Docus](https://nuxtlabs.com/docus) to write our documentation (*we plan to open source it in the following weeks once the documentation is ready*). It is similar to Nuxt Content [Docs theme](https://content.nuxtjs.org/themes/docs) but with an advanced syntax to use Vue components without having to write HTML (support props and slots).

## Setup

```bash
git@gitlab.com:t5514/taki-tarot-front-end.git tarot-frontend
cd tarot-frontend/
yarn
```
**Note: Use yarn only while setup this source**

Then the frontend source path look like: /var/www/tarot-frontend

If you change source path, change in file nuxt.config.js
## Development

Start the development server on [http://localhost:3000](http://localhost:3000)

```bash
yarn dev
```

## Build

```bash
yarn build
```
## Setup with PM2

```bash
yarn start
```

**Note: we using ecosystem.config.js to run with PM2**
